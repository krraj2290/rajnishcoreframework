<?php

// vim:set ts=4 sw=4 et:

namespace ElasticSearch\Transport;

/**
 * This file is part of the ElasticSearch PHP client
 *
 * (c) Raymond Julin <raymond.julin@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
if (!defined('CURLE_OPERATION_TIMEDOUT'))
    define('CURLE_OPERATION_TIMEDOUT', 200);

class HTTP extends Base {

    /**
     * How long before timing out CURL call
     */
    const TIMEOUT = 200;

    /**
     * curl handler which is needed for reusing existing http connection to the server
     * @var resource
     */
    protected $ch;

    public function __construct($host = 'localhost', $port = 9200) {
        parent::__construct($host, $port);
        $this->ch = curl_init();
    }

    /**
     * Index a new document or update it if existing
     *
     * @return array
     * @param array $document
     * @param mixed $id Optional
     */
    public function index($document, $id = false, array $options = array()) {
        $url = $this->buildUrl(array($this->type, $id), $options);
        $method = ($id == false) ? "POST" : "PUT";
        return $this->call($url, $method, $document);
    }

    /**
     * Search
     *
     * @return array
     * @param mixed $id Optional
     */
    public function search($query, array $options = array()) {
        if (is_array($query)) {
            /**
             * Array implies using the JSON query DSL
             */
            $arg = "_search";
            if (isset($options['routing'])) {
                if (isset($options['routing_disable'])) {
                    $arg = "_search?&q=_id:" . $options['routing'];
                } elseif (isset($options['routing_key']) && !empty($options['routing_key'])) {
                    $arg = "_search?q=" . $options['routing_key'];
                } else {
                    $arg = "_search?routing=" . $options['routing'] . "&q=_id:" . $options['userId'];
                }
            }
            if (isset($options['scroll']) && !empty($options['scroll'])) {
                $arg = "_search?&scroll=1m&search_type=scan&size=" . $options['size'];
            }
//            echo $arg.PHP_EOL;

            $url = $this->buildUrl(array(
                $this->type, $arg
            ));
            $result = $this->call($url, "GET", $query);
        } elseif (is_string($query)) {
            /**
             * String based search means http query string search
             */
            $url = $this->buildUrl(array(
                $this->type, "_search?q=" . $query
            ));
            $result = $this->call($url, "POST", $options);
        }
//        print_r($result);
        return $result;
    }

    public function count($query, array $options = array()) {
        if (is_array($query)) {
            /**
             * Array implies using the JSON query DSL
             */
            $url = $this->buildUrl(array(
                $this->type, "_count"
            ));
            echo $url;
            $result = $this->call($url, "GET", $query);
        } elseif (is_string($query)) {
            /**
             * String based search means http query string search
             */
            $url = $this->buildUrl(array(
                $this->type, "_count?q=" . $query
            ));
            $result = $this->call($url, "POST", $options);
        }
        return $result;
    }
    
    public function source(array $options = array()) {
       
            /**
             * String based search means http query string search
             */
            $url = $this->buildUrl(array(
                $this->type,$options['userId']."/_source?routing=" . $options['routing']
            ));
            $result = $this->call($url, "GET");
           
        return $result;
    }

    /**
     * Search
     *
     * @return array
     * @param mixed $id Optional
     * @param array $options Parameters to pass to delete action
     */
    public function deleteByQuery($query, array $options = array()) {
        $options += array(
            'refresh' => true
        );
        if (is_array($query)) {
            /**
             * Array implies using the JSON query DSL
             */
            $url = $this->buildUrl(array($this->type, "_query"));
            $result = $this->call($url, "DELETE", $query);
        } elseif (is_string($query)) {
            /**
             * String based search means http query string search
             */
            $url = $this->buildUrl(array($this->type, "_query"), array('q' => $query));
            $result = $this->call($url, "DELETE");
        }
        if ($options['refresh']) {
            $this->request('_refresh', "POST");
        }
//        print_r($result);
        return !isset($result['error']) && $result['ok'];
    }

    /**
     * Perform a request against the given path/method/payload combination
     * Example:
     * $es->request('/_status');
     *
     * @param string|array $path
     * @param string $method
     * @param array|false $payload
     * @return array
     */
    public function request($path, $method = "GET", $payload = false) {
        return $this->call($this->buildUrl($path), $method, $payload);
    }

    /**
     * Flush this index/type combination
     *
     * @return array
     * @param mixed $id Id of document to delete
     * @param array $options Parameters to pass to delete action
     */
    public function delete($id = false, array $options = array()) {
        if (!empty($id)) {
            return $this->request(array($this->type, $id), "DELETE");
        } else {
            throw new Exception('Id should not be empty.');
//            return array("message" => "Id should not be empty");
        }
    }

    /**
     * Perform a http call against an url with an optional payload
     *
     * @return array
     * @param string $url
     * @param string $method (GET/POST/PUT/DELETE)
     * @param array $payload The document/instructions to pass along
     */
    protected function call($url, $method = "GET", $payload = false) {



        $conn = $this->ch;
        $protocol = "http";
        $requestURL = $protocol . "://" . $this->host . $url;
        curl_setopt($conn, CURLOPT_URL, $requestURL);
        curl_setopt($conn, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($conn, CURLOPT_PORT, $this->port);
        curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($conn, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($conn, CURLOPT_FORBID_REUSE, 0);
//        echo 'payload';
//       print_r($payload);
        if (is_array($payload) && count($payload) > 0)
            curl_setopt($conn, CURLOPT_POSTFIELDS, json_encode($payload));
        else
            curl_setopt($conn, CURLOPT_POSTFIELDS, null);

        $response = curl_exec($conn);


        if ($response !== false) {
            $data = json_decode($response, true);
            if (!$data) {
                $data = array('error' => $response, "code" => curl_getinfo($conn, CURLINFO_HTTP_CODE));
            }
        } else {
            /**
             * cUrl error code reference can be found here:
             * http://curl.haxx.se/libcurl/c/libcurl-errors.html
             */
            $errno = curl_errno($conn);
            switch ($errno) {
                case CURLE_UNSUPPORTED_PROTOCOL:
                    $error = "Unsupported protocol [$protocol]";
                    break;
                case CURLE_FAILED_INIT:
                    $error = "Internal cUrl error?";
                    break;
                case CURLE_URL_MALFORMAT:
                    $error = "Malformed URL [$requestURL] -d " . json_encode($payload);
                    break;
                case CURLE_COULDNT_RESOLVE_PROXY:
                    $error = "Couldnt resolve proxy";
                    break;
                case CURLE_COULDNT_RESOLVE_HOST:
                    $error = "Couldnt resolve host";
                    break;
                case CURLE_COULDNT_CONNECT:
                    $error = "Couldnt connect to host [{$this->host}], ElasticSearch down?";
                    echo "\n" . 'HTTP response';
                    print_r($response);
                    break;
                case CURLE_OPERATION_TIMEDOUT:
                    $error = "Operation timed out on [$requestURL]";
                    break;
                default:
                    $error = "Unknown error";
                    if ($errno == 0)
                        $error .= ". Non-cUrl error";
                    break;
            }

            $exception = new HTTPException($error);
            $exception->payload = $payload;
            $exception->port = $this->port;
            $exception->protocol = $protocol;
            $exception->host = $this->host;
            $exception->method = $method;
            throw $exception;
        }
//            curl_close($conn);
        return $data;
    }

    public function _close() {

//        curl_close($this->ch);
    }

}
