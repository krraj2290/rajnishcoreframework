<?php

/**
 * Contains helper functions
 * @author Rajnish 
 *
 */
class Rajnish_Helper {
    /*
     * Remove duplicate data from array
     */

    public static function getWordCount($text) {
        $y = $text;
        $r = 0;
        $a = explode(' ', preg_replace('/\s/', ' ', $y));
        for ($z = 0; $z < count($a); $z++) {
            if (strlen($a[$z]) > 0) {
                $r++;
            }
        }
        return $r;
    }

    public static function exportAsCsv($array) {
        $csv = array(mysql_real_escape_string(implode(",", array_keys($array[0]))));
        foreach ($array as $a) {
            $csv[] = mysql_real_escape_string(implode(",", $a));
        }
        return implode("\n", $csv);
    }

    public static function getYearMonthBoundaries($yearMonth) {
        $year = substr($yearMonth, 0, 4);
        $month = substr($yearMonth, 4);

        $firstDay = strtotime("$year-$month-01");
        $lastDay = strtotime('-1 second', strtotime('+1 month', $firstDay));

        return date('jS M Y', $firstDay) . ' - ' . date('jS M Y', $lastDay);
    }

    public static function getYearWeekBoundaries($yearWeek) {
        $year = substr($yearWeek, 0, 4);
        $week = substr($yearWeek, 4);
        $Jan1 = mktime(1, 1, 1, 1, 1, $year);
        $iYearFirstWeekNum = (int) strftime("%W", mktime(1, 1, 1, 1, 1, $year));
        if ($iYearFirstWeekNum == 1) {
            $week = $week - 1;
        }

        $weekdayJan1 = date('w', $Jan1);
        $FirstMonday = strtotime(( ( 4 - $weekdayJan1 ) % 7 - 3 ) . ' days', $Jan1);
        $CurrentMondayTS = strtotime(( $week ) . ' weeks', $FirstMonday);
        return date('jS M Y', $CurrentMondayTS) . ' - ' . date('jS M Y', ( $CurrentMondayTS + ( 6 * 24 * 60 * 60 )));

        /*
          return array(
          'start' => $CurrentMondayTS,
          'end' => $CurrentMondayTS + ( 6 * 24 * 60 * 60 )
          );
         */
    }

    public static function super_unique($array) {
        $result = array_map("unserialize", array_unique(array_map("serialize", $array)));
        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $result[$key] = self::super_unique($value);
            }
        }
        return $result;
    }

    /*
     * Sort multi-dimention array
     */

    public static function array_sort($array, $on, $order=SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }
        return $new_array;
    }

    /**
     * Create a slug from title
     * @param $title stores text to be slugged
     * @return Slug, or Null if not found
     */
    public static function sanitizeWithDashes($title) {
        $title = strip_tags($title);
        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = preg_replace('/[^A-Za-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');
        return $title;
    }

    /**
     * Determines whether a given is a part of another array.
     * @param $bigArray
     * @param $smallArray
     * @return true on success, first key mismatch on failure.
     */
    public static function arrayContainsArray($bigArray, $smallArray) {
        foreach ($smallArray as $value) {
            if (array_search($value, $bigArray) === false) {
                return $value;
            }
        }
        return true;
    }

    /**
     * Calculates time differences
     * @param $temp_date stores date to be operated upon
     * @return result in minutes, or 0
     */
    public static function timeDiff($temp_date) {
        $today = time();
        $difference = $today - strtotime($temp_date);
        if ($difference > 0)
            $min = floor($difference / 60);
        else
            $min=0;
        return $min;
    }

    /**
     * Calculates the time differences in minutes,days,months,years ago
     */
    public static function timeAgo($temp_date) {
        $md = self::timeDiff($temp_date);
        if ($md < 60) {
            $val = (int) ($md);
            if ($val <= 1)
                return $val . " min ago";
            else
                return $val . " mins ago";
        }
        elseif ($md >= 60 && $md < 24 * 60) {
            $val = (int) ($md / (60));
            if ($val == 1)
                return $val . " hr ago";
            else
                return $val . " hrs ago";
        }
        elseif ($md >= 24 * 60 && $md < 24 * 60 * 7) {
            $val = (int) ($md / (60 * 24));
            if ($val == 1)
                return $val . " day ago";
            else
                return $val . " days ago";
        }
        elseif ($md >= 24 * 60 * 7 && $md < 24 * 60 * 31) {
            $val = (int) ($md / (24 * 60 * 7));
            if ($val == 1)
                return $val . " week ago";
            else
                return $val . " weeks ago";
        }
        elseif ($md >= 24 * 60 * 31 && $md < 24 * 60 * 365) {
            $val = (int) ($md / (24 * 60 * 31));
            if ($val == 1)
                return $val . " month ago";
            else
                return $val . " months ago";
        }
        else {
            return date("M j Y", strtotime($temp_date));

            $val = (int) ($md / (60 * 24 * 365));
            if ($val == 1)
                return $val . " year ago";
            else
                return $val . " years ago";
        }
    }

    //time zone conversion function
    public function getLocalTime($tm, $offset) {

        return ($tm - date("Z", $tm)) + (3600 * $offset);
    }

    /**
     * Calculates difference between $start_date and $end_date
     * @param $start_date
     * @param $end_date
     * @return unknown_type
     */
    public static function theTimeTaken($start_date, $end_date = null) {
        if ($end_date === null) {
            $end_date = date('Y-m-d h:i:s');
        }
        $post_duration = self::TimeDiff2($start_date, $end_date);
        if ($post_duration < 1) {
            $show_duration = "few seconds";
        } elseif ($post_duration < 60) {
            $show_duration = $post_duration . " min";
        } elseif ($post_duration < 1440) {
            $post_duration1 = floor($post_duration / 60);
            $show_duration = $post_duration1 . " hours";
        } else {
            $formatted_post_date = date("M j Y", strtotime($post_duration));
            $show_duration = $formatted_post_date;
        }
        return $show_duration;
    }

    /**
     * Calculates time differences
     * @param $start_date
     * @param $end_date
     * @return unknown_type
     */
    public static function timeDiff2($start_date, $end_date) {
        $end = strtotime($end_date);
        $start = strtotime($start_date);

        $difference = $end - $start;
        if ($difference > 0)
            $min = floor($difference / 60);
        else
            $min=0;
        return $min;
    }

    /**
     * Calculate difference between the days from the $endDate and current date
     * @param $endDate stores the date till which days are to be caluculated from now
     * @return integer days
     */
    public static function daysDifference($endDate) {
        $beginDate = date("Y-m-d");
        //explode the date by "-" and storing to array
        $date_parts1 = explode("-", $beginDate);
        $date_parts2 = explode("-", $endDate);
        //gregoriantojd() Converts a Gregorian date to Julian Day Count
        $start_date = gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
        $end_date = gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
        return $end_date - $start_date;
    }

    /**
     * Calculate difference between the days from the $endDate and $startDate
     * @param $endDate stores the date till which days are to be caluculated from $startDate
     * @return integer days
     */
    public static function daysDifference2($beginDate, $endDate) {
        //explode the date by "-" and storing to array
        $date_parts1 = explode("-", $beginDate);
        $date_parts2 = explode("-", $endDate);
        //gregoriantojd() Converts a Gregorian date to Julian Day Count
        $start_date = gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
        $end_date = gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
        return $end_date - $start_date;
    }

    /**
     * Returns the IP Address of the user even from shared or proxy network
     * @return string IP Address
     */
    public static function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    public static function escape($string) {
        if (( get_magic_quotes_runtime() == 1 ) || ( get_magic_quotes_gpc() == 1 )) {
            $string = stripslashes($string);
        }
        return mysql_real_escape_string($string);
    }

    public static function randomStringGenerator($length, $small=false) {
        $i = 0;
        $random_str = '';
        $a_to_z = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ123456789';
        $max = strlen($a_to_z) - 1;
        while ($i != $length) {
            $rand = mt_rand(0, $max);
            $random_str .= $a_to_z[$rand];
            $i++;
        }
        return $small ? strtolower($random_str) : $random_str;
    }

    public static function isValidEmailId($emailId) {
        if (preg_match("/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i", $emailId) > 0) {
            return true;
        }
        return false;
    }

    public static function isValidUrl($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url) > 0;
    }

    public static function getUrlSourceDomain($imageUrl) {
        $parsedUrl = parse_url($imageUrl);
        $imageHost = isset($parsedUrl["host"]) ? $parsedUrl["host"] : 'unknown';
        $imageHost = str_replace('www.', '', $imageHost);
        return $imageHost;
    }

    public static function xml2array($contents, $get_attributes=1, $priority = 'tag') {
        if (!$contents)
            return array();
        if (!function_exists('xml_parser_create')) {
            return array();
        }
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values)
            return;

        //Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array; //Refference
        //Go through the tags.
        $repeated_tag_index = array(); //Multiple tags with same name will be turned into an array
        foreach ($xml_values as $data) {
            unset($attributes, $value); //Remove existing values, or there will be trouble
            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data); //We could use the array by itself, but this cooler.
            $result = array();
            $attributes_data = array();

            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode

            }
            //Set the attributes too.
            if (isset($attributes) and $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'

                }
            }
            //See tag status and do the needed.
            if ($type == "open") {//The starting of the tag '<tag>'
                $parent[$level - 1] = &$current;
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = &$current[$tag];
                }
                else {
                    if (isset($current[$tag][0])) {//If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else {//This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array($current[$tag], $result); //This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag . '_' . $level] = 2;

                        if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }
            } elseif ($type == "complete") { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if (!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                }
                else { //If taken, put all things inside a list(array)
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else { //If it is not an array...
                        $current[$tag] = array($current[$tag], $result); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) { //The attribute of the last(0th) tag must be moved as well
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }
                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            } elseif ($type == 'close') { //End of tag '</tag>'
                $current = &$parent[$level - 1];
            }
        }
        return($xml_array);
    }

    /**
     * A helping function for php2js function of this class.
     * @param $arr
     * @return unknown_type
     */
    private static function arr2json($arr) {
        $json = array();
        foreach ($arr as $k => &$val) {
            $json[] = $k . ':' . self::php2js($val);
        }
        if (count($json) > 0) {
            return '{' . implode(',', $json) . '}';
        } else {
            return '{}';
        }
    }

    /**
     * Returns the locations of bits which are set in a decimal number
     * @param int $number
     * @return array
     */
    public static function getSetBits($number) {
        $number = intval($number);
        $i = 0;
        $setBits = array();
        while (pow(2, $i) <= $number) {
            if ($number & pow(2, $i)) {
                $setBits[] = $i;
            }
            $i++;
        }
        return $setBits;
    }

    /**
     * Converts a PHP variable into javascript json format.
     * @param variant $val
     * @return string
     */
    public static function php2js($val) {
        if (is_array($val)) {
            return self::arr2json($val);
        }
        if (is_string($val)) {
            return '"' . str_replace("\r", "\\r", str_replace("\n", "\\n", addslashes($val))) . '"'; // str_replace( '"', '\\"', str_replace("\n", " \\\n", addslashes( $val ) ) ) . '"';
        }
        if (is_bool($val)) {
            return 'Boolean(' . (int) $val . ')';
        }
        if (is_null($val)) {
            return '""';
        }
        return $val;
    }

    /**
     * Right shifts the elements of an array.
     * @param array $thisArray
     * @return array
     */
    public static function rShift($thisArray) {
        if (!is_array($thisArray) || !count($thisArray)) {
            return $thisArray;
        }
        $first = $thisArray[0];
        for ($i = 0; $i < count($thisArray) - 1; $i++) {
            $thisArray[$i] = $thisArray[$i + 1];
        }
        $thisArray[count($thisArray) - 1] = $first;
        return $thisArray;
    }

    public static function timeRemaining($endDateTime) {
        $md = (int) ( ( strtotime($endDateTime) - time() ) / 60 );
        if ($md < 0) {
            return 'expired';
        }
        $hr = (int) ( $md / 60 );
        $mins = $md % 60;

        if ($hr > 0) {
            return "$hr hrs. $mins mins. remaining";
        } else {
            return "$mins mins. remaining";
        }
    }

    public static function HourDiff($startDate, $endDate = null, $descriptive = true) {
        if ($endDate === null) {
            $endDate = time();
        } else {
            $endDate = strtotime($endDate);
        }
        $startArry = date_parse($startDate);

        $start_date = strtotime($startDate);
        $end_date = $endDate;

        $seconds = $end_date - $start_date;
        $hours = ( $seconds ) / 3600;

        if ($descriptive !== true) {
            return floor($hours);
        }

        $quant = 0;
        $unit = '';

        if ($seconds < 60) {
            $quant = $seconds;
            $unit = ' Sec';
        } else if ($seconds < 3600) {
            $quant = floor($seconds / 60);
            $unit = ' Min';
        } else if ($seconds < ( 24 * 3600 )) {
            $quant = round(($seconds / 3600), 2);
            $unit = ' Hour';
        } else {
            $days = floor($seconds / ( 3600 * 24 ));
            if ($days < 7) {
                $quant = $days;
                $unit = ' Day';
            } else {
                $weeks = floor($days / 7);
                if ($weeks < 4) {
                    $quant = $weeks;
                    $unit = ' Week';
                } else {
                    $quant = floor($weeks / 4);
                    $unit = ' Month';
                }
            }
        }
        $unit = $quant > 1 ? $unit . 's' : $unit;
        return $quant . $unit;
    }

    public static function getShortDuration($fullDuration) {
        $searchArray = array(' Hour', ' Day', ' Week', ' Month');
        $replaceArray = array('hr', 'dy', 'wk', 'mn');
        foreach ($searchArray as $key => $value) {
            if (strpos($fullDuration, $value) !== false) {
                return str_replace($value, $replaceArray[$key], $fullDuration);
            }
        }
    }

    /**
     * Add/subtract Date from given date.
     * @param int  $givendate
     * @param int  $day
     * @param int  $mth
     * @param int  $yr
     * @return New Date
     */
    public static function add_date($givendate, $day=0, $mth=0, $yr=0) {
        $cd = strtotime($givendate);

        $newdate = date('Y-m-d', mktime(date('h', $cd), date('i', $cd), date('s', $cd), date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr));

        return $newdate;
    }

    /**
     * Get Current Week Date
     * @return Current Week Date
     */
    public static function getCurrentWeek() {
        $currWeekday = date('w');
        $currentDay = date("d", mktime());
        if (!($currWeekday == 0)) {
            $currentDay = $currentDay - $currWeekday;
        }
        return $currentDay;
    }

    /**
     * Get Next Week Date.
     * @param int $nyear
     * @param int $nmonth
     * @param int $nday
     * @return Next Week Date
     */
    public static function getNextWeek($nyear, $nmonth, $nday) {
        $nextWeek = '';
        for ($i = 0; $i < 7; $i++) {
            $givendate = $nyear . '-' . $nmonth . '-' . $nday;
            $completeDate = explode("-", self::add_date($givendate, 1, 0, 0));
            $nday = $completeDate[2];
            $nmonth = $completeDate[1];
            $nyear = $completeDate[0];
        }
        $nextWeek = $nyear . '-' . $nmonth . '-' . $nday;
        return $completeDate;
    }

    /**
     * Get Previous Week Date
     * @param int $pyear
     * @param int $pmonth
     * @param int $pday
     * @return Previous Week Date
     */
    public static function getPrevWeek($pyear, $pmonth, $pday) {
        $givendate = $pyear . '-' . $pmonth . '-' . $pday;
        return explode("-", self::add_date($givendate, -7, 0, 0));
    }

    /**
     * Get Next Day Date.
     * @param int $nyear
     * @param int $nmonth
     * @param int $nday
     * @return Next Day Date
     */
    public static function getNextDay($nyear, $nmonth, $nday) {
        $nextDay = '';
        $givendate = $nyear . '-' . $nmonth . '-' . $nday;
        $completeDate = explode("-", self::add_date($givendate, 1, 0, 0));
        $nday = $completeDate[2];
        $nmonth = $completeDate[1];
        $nyear = $completeDate[0];
        $nextDay = $nyear . '-' . $nmonth . '-' . $nday;
        return $completeDate;
    }

    /**
     * Get Previous Day Date
     * @param int $pyear
     * @param int $pmonth
     * @param int $pday
     * @return Previous Day Date
     */
    public static function getPrevDay($pyear, $pmonth, $pday) {
        $givendate = $pyear . '-' . $pmonth . '-' . $pday;
        return explode("-", self::add_date($givendate, -1, 0, 0));
    }

    /**
     * Get Last Day of Month.
     * @param int $mon
     * @param int $year
     * @return Last Day of Month.
     */
    public static function mk_getLastDayofMonth($mon, $year) {
        for ($tday = 28; $tday <= 31; $tday++) {
            $tdate = getdate(mktime(0, 0, 0, $mon, $tday, $year));
            if ($tdate["mon"] != $mon) {
                break;
            }
        }
        $tday--;
        return $tday;
    }

    /**
     * Defines rule for sorting an array of strings in ascending order.
     * Usage : usort( <array-variable-name>, 'Goquii_Helper::sortTags' );
     * @param string $a
     * @param string $b
     */
    public static function sortTags($a, $b) {
        return trim(strtolower($a)) > trim(strtolower($b));
    }

    /*
     * Compare two dates with Y-m-d format.
     */

    public static function compareTwoDate($firstDate, $secondDate) {
        $today = strtotime($secondDate);
        $expiration_date = strtotime($firstDate);
        if ($expiration_date > $today) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * get text between tags
     */

    public static function getTextBetweenTags($string, $tagname) {
        $pattern = "/<$tagname>(.*?)<\/$tagname>/";
        preg_match($pattern, $string, $matches);
        return $matches[1];
    }

    /**
     * Get youtube video embedd code from its ID.
     * @param $videoId
     * @param $width
     * @param $height
     * @return unknown_type
     */
    public static function getYoutubeVideo($videoId, $width = 480, $height = 385) {
        return '<object width="' . $width . '" height="' . $height . '">
						<param name="movie" value="http://www.youtube.com/v/' . $videoId . '&hl=en_US&fs=1&"></param>
						<param name="allowFullScreen" value="true"></param>
						<param name="allowscriptaccess" value="always"></param>
						<embed src="http://www.youtube.com/v/' . $videoId . '&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed>
					</object>';
    }

    /**
     * Returns all the known file extentions and their mime types.
     * @param string $mimePath
     * @return array
     */
    public static function getMimeTypes($mimeFilePath = '/etc/') {
        $regex = '/([\w\+\-\.\/]+)\t+([\w\s]+)/i';
        $lines = file("$mimeFilePath/mime.types", FILE_IGNORE_NEW_LINES);
        $mimeArray = array();
        foreach ($lines as $line) {
            if (substr($line, 0, 1) == '#') {
                continue; // skip comments
            }
            if (!preg_match($regex, $line, $matches)) {
                continue; // skip mime types w/o any extensions
            }
            $mime = $matches[1];
            $extensions = explode(' ', $matches[2]);
            foreach ($extensions as $ext) {
                $mimeArray[trim($ext)] = $mime;
            }
        }
        return $mimeArray;
    }

    public static function calculatePreModifiedDate($postDate) {
        $today = time();
        $difference = $today - strtotime($postDate);
        if ($difference > 0)
            $postDuration = floor($difference / 60);
        else
            return 0;

        if ($postDuration < 1) {
            $showDuration = "few secs ago";
        } elseif ($postDuration < 2) {
            $showDuration = $postDuration . " min ago";
        } elseif ($postDuration < 60) {
            $showDuration = $postDuration . " mins ago";
        } elseif ($postDuration < 120) {
            $postDuration1 = floor($postDuration / 60);
            $showDuration = $postDuration1 . " hr ago";
        } elseif ($postDuration < 1440) {
            $postDuration1 = floor($postDuration / 60);
            $showDuration = $postDuration1 . " hrs ago";
        } else {
            $formatted_post_date = date("M j Y", strtotime($postDate));
            $showDuration = $formatted_post_date;
        }
        return $showDuration;
    }

    public static function calculatePostModifiedDate($postDate) {
        $today = time();
        $difference = strtotime($postDate) - $today;
        if ($difference > 0)
            $postDuration = floor($difference / 60);
        else
            return 0;

        if ($postDuration < 1) {
            $showDuration = "few secs ";
        } elseif ($postDuration < 2) {
            $showDuration = $postDuration . " min ";
        } elseif ($postDuration < 60) {
            $showDuration = $postDuration . " mins ";
        } elseif ($postDuration < 120) {
            $postDuration1 = floor($postDuration / 60);
            $showDuration = $postDuration1 . " hr ";
        } elseif ($postDuration < 1440) {
            $postDuration1 = floor($postDuration / 60);
            $showDuration = $postDuration1 . " hrs ";
        } else {
            $formatted_post_date = date("M j Y", strtotime($postDate));
            $showDuration = $formatted_post_date;
        }
        return $showDuration;
    }

    public static function getMonthInterval($dbDate) {
        $dbDate = strtotime($dbDate);
        $currentDate = date('Y-m-d H:i:s');
        $diff = strtotime($currentDate) - $dbDate;
        $months = round($diff / ( 108000 * 24 ));
        if ($months < 12) {
            return "$months Month(s)";
        } else {
            return round($months / 12) . ' Year(s)';
        }
    }

    /**
     * Returns the size in Bytes of a remote file(URL).
     * @param $url string, URL of the file.
     * @param $descriptive boolean, If true the size of URL is converted to descriptive form like 3.45KB, 1.2MB etc.
     * @return integer/boolean, False if URL is not accessible, size of the file otherwise.
     */
    public static function getRemoteFileSize($url, $descriptive = false) {
        $parsed = parse_url($url);

        if (!isset($parsed['scheme']) or !isset($parsed['host']) or !isset($parsed['path'])) {
            throw new Exception('The URL "' . $url . '" seems to be invalid!');
        }
        $host = $parsed["host"];
        $portNumber = 80;
        if ($parsed['scheme'] == 'https') {
            $portNumber = 443;
        }

        $fp = @fsockopen($host, $portNumber, $errno, $errstr, 20);

        if (!$fp) {
            return false;
        } else {
            @fputs($fp, "HEAD $url HTTP/1.1\r\n");
            @fputs($fp, "HOST: $host\r\n");
            @fputs($fp, "Connection: close\r\n\r\n");
            $headers = "";
            while (!@feof($fp)) {
                $headers .= @ fgets($fp, 128);
            }
        }

        @fclose($fp);

        $return = false;
        $arr_headers = explode("\n", $headers);
        foreach ($arr_headers as $header) {
            // follow redirect
            $s = 'Location: ';
            if (substr(strtolower($header), 0, strlen($s)) == strtolower($s)) {
                $url = trim(substr($header, strlen($s)));
                return $this->getRemoteFileSize($url, $descriptive);
            }
            // parse for content length
            $s = "Content-Length: ";
            if (substr(strtolower($header), 0, strlen($s)) == strtolower($s)) {
                $return = trim(substr($header, strlen($s)));
                break;
            }
        }
        if ($descriptive) {
            $size = round($return / 1024, 2);
            $sz = "KB"; // Size In KB
            if ($size > 1024) {
                $size = round($size / 1024, 2);
                $sz = "MB"; // Size in MB
            }
            $return = "$size $sz";
        }
        return $return;
    }
    
    public static function encrypt( $text, $key='zse' ) {
    	return base64_encode( mcrypt_encrypt( MCRYPT_3DES, $key, base64_encode( $text ), MCRYPT_MODE_ECB ) );
    }

    public static function decrypt( $text, $key='zse' ) {    	
    	return base64_decode( mcrypt_decrypt( MCRYPT_3DES, $key, base64_decode( $text ), MCRYPT_MODE_ECB ) );
    }

    public static function auto_link_text($text) {
        $pattern = '#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#';
        $callback = create_function('$matches', '
	       $url       = array_shift($matches);
	       $url_parts = parse_url($url);
	
	       $text = parse_url($url, PHP_URL_HOST) . parse_url($url, PHP_URL_PATH);
	       $text = preg_replace("/^www./", "", $text);
	
	       $last = -(strlen(strrchr($text, "/"))) + 1;
	       if ($last < 0) {
	           $text = substr($text, 0, $last) . "&hellip;";
	       }
	
	       return sprintf(\'<a rel="nofollow" href="%s">%s</a>\', $url, $text);
	   ');

        return preg_replace_callback($pattern, $callback, $text);
    }

    public static function auto_link_linebreak_text($text) {
        $text = preg_replace("/(https?|ftps?|mailto):\/\/([-\w\p{L}\.]+)+(:\d+)?(\/([\'\!\,\:\(\)\x\$\;\#\+\%\=\-\w\p{L}\/_\.]*(\?\S+)?)?)?/u", '<a style="display: block;" target="_blank" href="$0">$0</a>', $text);
        return $text;
    }

    public static function recursiveArraySearch($array, $needle, $index = null) {
        $aIt = new RecursiveArrayIterator($array);
        $it = new RecursiveIteratorIterator($aIt);

        while ($it->valid()) {
            if (((isset($index) AND ($it->key() == $index)) OR (!isset($index))) AND ($it->current() == $needle)) {
                return $aIt->key();
            }

            $it->next();
        }

        return false;
    }

    public static function sortmulti($array, $index, $order, $natsort=FALSE, $case_sensitive=FALSE) {
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key)
                $temp[$key] = $array[$key][$index];
            if (!$natsort) {
                if ($order == 'asc')
                    asort($temp);
                else
                    arsort($temp);
            }
            else {
                if ($case_sensitive === true)
                    natsort($temp);
                else
                    natcasesort($temp);
                if ($order != 'asc')
                    $temp = array_reverse($temp, TRUE);
            }
            foreach (array_keys($temp) as $key)
                if (is_numeric($key))
                    $sorted[] = $array[$key];
                else
                    $sorted[$key] = $array[$key];
            return $sorted;
        }
        return $sorted;
    }

    function describe($thisVariable = null, $exit = false, $verbose = true, $level = 0) {
        echo '<pre>';
        $spaces = '';
        for ($i = 0; $i < $level * 4; $i++) {
            $spaces .= ' ';
        }
        if (is_string($thisVariable) || is_numeric($thisVariable)) {
            echo $spaces . htmlspecialchars($thisVariable);
        } else if (is_array($thisVariable)) {
            echo "<pre>{$spaces}[Array ( " . count($thisVariable) . " ) ]</pre>";
            foreach ($thisVariable as $key => $value) {
                describe("[" . $key . '] =>', false, true, $level + 1);
                describe($value, false, true, $level + 1);
            }
            echo "{$spaces}[/Array]";
        } else {
            var_dump($thisVariable);
        }
        echo '</pre>';
        if ($thisVariable === null || $exit !== false) {
            if ($verbose) {
                describe('Exit Call Handled . . .');
            }
            exit;
        }
        return $thisVariable;
    }

}
