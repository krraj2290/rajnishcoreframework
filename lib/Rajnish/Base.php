<?php

abstract class Rajnish_Base {

    private $_reservedParameters = array("order", "count", "sortOrder", "sortColumn", "pageNumber", "quantity", "sum", "column", "avg", /*         * Rajnish Kr Rajesh */ 'noPagination', 'groupBy');
    //reserve request arguments

    /**
     * @author Jitendra Singh Bhadouria
     * @desc Inserted paramter for noPagination
     */
    protected $_noPagination = 'N';
    private $_sortOrder = "DESC";
    protected $_sortOrderType = "";
    protected $_sortColumn = "createdTime";
    private $_pageNumber = 1;
    protected $_quantity = 10;
    private $_count = 'N';
    protected $_order = 'Y';
    private $_sum = array();
    private $_column = array();
    private $_avg = array();
    protected $_enableForcedCondition = false;
    protected $_betweenClause = '';
    //basic db and table info
    protected $_databaseConnection = null;
    protected $_mainTable = null;
    protected $_groupByClause = false;
    protected $_clauseColumnNames = null;
    protected $_sortColumnNames = null;
    protected $_requiredAddColumns = array();
    protected $_runtimeAddColumns = array();
    protected $_foreignKey = null;
    protected $_expandableTables = null;
    //permissible values variables
    private $_mainTablePermissibleValues = '';
    private $_expandableTablesPermissibleValues = '';
    private $_arrayExpandablePermissibleValues = array();
    //array of all column conditions, where clauses
    private $_arrayClauses = array();
    //add or update variables
    protected $_lastInsertedId = 0;
    protected $_arrayUpdatedData = array();
    protected $_task = 'show';
    //resultset information variables
    private $_dataResultArray = array();
    private $_dataResultCount = 0;
    private $_dataTotalCount = 0;
    private $_dataTotalPages = 0;

    public function setForcedCondition($betweenClause) {
        $this->_betweenClause = $betweenClause;
        if (!empty($betweenClause)) {
            $this->_enableForcedCondition = true;
        }
    }

//	public function __construct( $task = 'show' ) {
//		$this->_task = strtolower($task);
//		$this->_databaseConnection = LiveData_Connection::GetInstance();
//	}

    public function set() {
        try {
            $argumentsList = func_get_args();
           
            
            global $contentCloudClientId;
            global $contentCloudUserId;
            if ($contentCloudClientId) {
                $stringArgumentList = implode("~", $argumentsList);
                if (stristr($stringArgumentList, 'clientId||') === FALSE) {
                    if (array_search('clientId', $this->_updateColumnNames) !== FALSE) {
                        $argumentsList[] = "clientId||$contentCloudClientId";
                    }
                }
            }
            $this->_sortColumn = is_array($this->_sortColumnNames) && (count($this->_sortColumnNames) == 1) ? $this->_sortColumnNames[0] : $this->_sortColumn;
            $this->_sortOrder = $this->_sortOrderType == '' ? $this->_sortOrder : $this->_sortOrderType;
            $totalArguments = count($argumentsList);

            switch ($this->_task) {
                case "add":
                    
                    if ($contentCloudUserId) {
                        if (array_search('createdUserId', $this->_updateColumnNames) !== FALSE) {
                            $argumentsList[] = "createdUserId||$contentCloudUserId";
                        }
                    }

                    $totalArguments = count($argumentsList);
                   

                    for ($i = 0; $i < $totalArguments; $i++) {
                        //process single argument and break into array
                        $tempArgumentArray = explode("||", $argumentsList[$i]);
                        $argumentsArrayCount = count($tempArgumentArray);
                        //if someone is not following the keyname||keyvalue rule, throw error
                        if ($argumentsArrayCount < 2)
                        //throw new Exception("Argument '$argumentsList[$i]' is invalid");
                            throw new Exception(gettext("Base : ") . gettext("Argument ") . $argumentsList[$i] . gettext(" is invalid"));
                        else if ($argumentsArrayCount > 2) {
                            for ($ii = 2; $ii < $argumentsArrayCount; $ii++) {
                                $tempArgumentArray[1] .= '||' . $tempArgumentArray[$ii];
                            }
                        }
                        //check for keyname present in allowed columns list
                        if (array_search($tempArgumentArray[0], $this->_updateColumnNames) === FALSE) {
                            //throw new Exception("Argument '$tempArgumentArray[0]' is invalid");
                            throw new Exception(gettext("Base : ") . gettext("Argument ") . $tempArgumentArray[0] . gettext(" is invalid"));
                        }

                        if ($tempArgumentArray[0] != 'xElementId' && ( substr($tempArgumentArray[0], strlen($tempArgumentArray[0]) - 2) == 'Id' and !is_numeric($tempArgumentArray[1]) )) {
                            //throw new Exception("$tempArgumentArray[0] should be a natural number.");
                            throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("should be a natural number."));
                        }

                        $this->_arrayUpdatedData[$tempArgumentArray[0]] = $tempArgumentArray[1];
                    }

                    $this->add();
                    break;

                case "edit":
                    for ($i = 0; $i < $totalArguments; $i++) {
                        //process single argument and break into array
                        $tempArgumentArray = explode("||", $argumentsList[$i]);

                        //if someone is not following the keyname||keyvalue rule, throw error
                        if (count($tempArgumentArray) != 2)
                        //throw new Exception("Argument '$argumentsList[$i]' is invalid");
                            throw new Exception(gettext("Base : ") . gettext("Argument") . $argumentsList[$i] . gettext("is invalid"));

                        //check for keyname present in allowed columns list
                        if (array_search($tempArgumentArray[0], $this->_updateColumnNames) === FALSE) {
                            if ($tempArgumentArray[0] != $this->_foreignKey)
                            //throw new Exception("Argument '$tempArgumentArray[0]' is invalid");
                                throw new Exception(gettext("Base : ") . gettext("Argument") . $tempArgumentArray[0] . gettext("is invalid"));
                        }

                        if ($tempArgumentArray[0] != 'xElementId' && ( substr($tempArgumentArray[0], strlen($tempArgumentArray[0]) - 2) == 'Id' and !is_numeric($tempArgumentArray[1]) )) {
                            //throw new Exception("$tempArgumentArray[0] should be a natural number.");
                            throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext(" should be a natural number."));
                        }

                        $this->_arrayUpdatedData[$tempArgumentArray[0]] = $tempArgumentArray[1];
                    }

                    $this->edit();
                    break;

                case "delete":
                    for ($i = 0; $i < $totalArguments; $i++) {
                        //process single argument and break into array
                        $tempArgumentArray = explode("||", $argumentsList[$i]);

                        //if someone is not following the keyname||keyvalue rule, throw error
                        if (count($tempArgumentArray) != 2)
                        //throw new Exception("Argument '$argumentsList[$i]' is invalid");
                            throw new Exception(gettext("Base : ") . gettext("Argument") . $argumentsList[$i] . gettext("is invalid"));

                        //check for keyname present in allowed columns list
                        if (array_search($tempArgumentArray[0], $this->_updateColumnNames) === FALSE) {
                            if ($tempArgumentArray[0] != $this->_foreignKey)
                            //throw new Exception("Argument '$tempArgumentArray[0]' is invalid");
                                throw new Exception(gettext("Base : ") . gettext("Argument") . $tempArgumentArray[0] . gettext("is invalid"));
                        }

                        if (substr($tempArgumentArray[0], strlen($tempArgumentArray[0]) - 2) == 'Id' and !is_numeric($tempArgumentArray[1])) {
                            //throw new Exception("$tempArgumentArray[0] should be a natural number.");
                            throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("should be a natural number."));
                        }

                        $this->_arrayUpdatedData[$tempArgumentArray[0]] = Rajnish_Helper :: escape($tempArgumentArray[1]);
                    }
                    $this->delete();
                    break;

                default:
                    //process each argument, set reserve parameters and clauses
                    for ($i = 0; $i < $totalArguments; $i++) {
                        //process single argument and break into array
                        $tempArgumentArray = explode("||", $argumentsList[$i]);

                        //if someone is not following the keyname||keyvalue rule, throw error
                        if (count($tempArgumentArray) != 2)
                        //throw new Exception("Argument '$argumentsList[$i]' is invalid");
                            throw new Exception(gettext("Base : ") . gettext("Argument ") . $argumentsList[$i] . gettext(" is invalid!"));

                        if ($tempArgumentArray[0] != 'xElementId' && ( strstr($tempArgumentArray[0], 'Id') !== FALSE and !is_numeric(trim($tempArgumentArray[1], '><')) )) {
                            //throw new Exception("$tempArgumentArray[0] should be a natural number.");
                            throw new Exception(gettext("Base : ") . gettext("Argument ") . $tempArgumentArray[0] . gettext(" is invalid!!"));
                        }

                        //check for keyname present in reserve parameter list
                        if (array_search($tempArgumentArray[0], $this->_reservedParameters) === FALSE) {
                            //check for keyname present in allowed columns list
                            if (array_search($tempArgumentArray[0], $this->_clauseColumnNames) === FALSE) {
                                //throw new Exception("Argument '$tempArgumentArray[0]' is invalid");
                                throw new Exception(gettext("Base : ") . gettext("Argument") . $tempArgumentArray[0] . gettext("is invalid!!!"));
                            } else {
                                //set column clauses
                                $firstCharacter = substr($tempArgumentArray[1], 0, 1);
                                if ('=' == $firstCharacter || '<' == $firstCharacter || '>' == $firstCharacter) {
                                    $tempArgumentArray[1] = substr($tempArgumentArray[1], 1);
                                    $clauseValue = Rajnish_Helper :: escape($tempArgumentArray[1]);
                                    if (is_numeric($clauseValue))
                                        $tempClause = "$tempArgumentArray[0] $firstCharacter $clauseValue";
                                    else
                                        $tempClause = "$tempArgumentArray[0] $firstCharacter '$clauseValue'";
                                }
                                else {
                                    $clauseValue = Rajnish_Helper:: escape($tempArgumentArray[1]);
                                    $tempClause = "$tempArgumentArray[0] = '$clauseValue'";
                                }
                                array_push($this->_arrayClauses, $tempClause);
                            }
                        } else {
                            //process keyname to populate reserve parameter variables
                            $firstCharacter = substr($tempArgumentArray[1], 0, 1);
                            if ('=' == $firstCharacter)
                                $tempArgumentArray[1] = substr($tempArgumentArray[1], 1);

                            if ('<' == $firstCharacter || '>' == $firstCharacter)
                            //throw new Exception("'<','>' are not allowed with $tempArgumentArray[0]");
                                throw new Exception(gettext("Base : ") . gettext("'<','>' are not allowed with ") . $tempArgumentArray[0]);
                            switch ($tempArgumentArray[0]) {
                                case 'sortOrder':
                                    if ('asc' == strtolower($tempArgumentArray[1]) || 'desc' == strtolower($tempArgumentArray[1]))
                                        $this->_sortOrder = strtoupper($tempArgumentArray[1]);
                                    else
                                    //throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. Valid values : ASC, DESC");
                                        throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("'s value '") . $tempArgumentArray[1] . gettext("'s value  is invalid. Valid values : ASC, DESC"));
                                    break;

                                case 'quantity':
                                    if (is_numeric($tempArgumentArray[1])) {
                                        $this->_quantity = $tempArgumentArray[1];
                                        if ($this->_quantity < 1) {
                                            $this->_quantity = 1;
                                        }
                                    } else
                                    //throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. It should be Number.");
                                        throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("'s value '") . $tempArgumentArray[1] . gettext("'s value  is invalid. It should be Number."));
                                    break;

                                case 'pageNumber':
                                    if (is_numeric($tempArgumentArray[1]))
                                        $this->_pageNumber = $tempArgumentArray[1];
                                    else
                                    //throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. It should be Number.");
                                        throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("'s value '") . $tempArgumentArray[1] . gettext("'s value  is invalid. It should be Number."));
                                    break;

                                case 'sortColumn':
                                    if (array_search($tempArgumentArray[1], $this->_sortColumnNames) === FALSE) {
                                        $validSortColumns = implode(', ', $this->_sortColumnNames);
                                        //throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. Valid values : $validSortColumns");
                                        throw new Exception(gettext("Base : ") . $tempArgumentArray[0] . gettext("'s value '") . $tempArgumentArray[1] . gettext("'s value  is invalid. Valid values : ") . $validSortColumns);
                                    }

                                    $this->_sortColumn = $tempArgumentArray[1];
                                    break;

                                case 'count':
                                    if ('y' == strtolower($tempArgumentArray[1]) || 'n' == strtolower($tempArgumentArray[1]))
                                        $this->_count = strtoupper($tempArgumentArray[1]);
                                    else
                                        throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. Valid values : Y, N");
                                    break;

                                case 'order':
                                    if ('y' == strtolower($tempArgumentArray[1]) || 'n' == strtolower($tempArgumentArray[1]))
                                        $this->_order = strtoupper($tempArgumentArray[1]);
                                    else
                                        throw new Exception("$tempArgumentArray[0]'s value '$tempArgumentArray[1]' is invalid. Valid values : Y, N");
                                    break;
                                case 'sum':
                                    $var = 'sum(' . $tempArgumentArray[1] . ') as ' . $tempArgumentArray[1] . ' ';
                                    //echo $var."<br/>";print_r($this->_sum);
                                    $this->_sum[] = $var;
                                    break;

                                case 'column':
                                    $this->_column[] = $tempArgumentArray[1];
                                    //array_push($this->_column, $tempArgumentArray[1]);
                                    break;

                                case 'avg':
                                    $var = 'avg(' . $tempArgumentArray[1] . ') as ' . $tempArgumentArray[1] . ' ';
                                    $this->_avg[] = $var;
                                    break;
                                case 'noPagination':
                                    $this->_noPagination = $tempArgumentArray[1];
                                    break;
                                case 'groupBy' :
                                    $this->_groupByClause = $tempArgumentArray[1];
                                    break;
                            }
                        }
                    }

                    $this->show();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    function show() {
        $start = ($this->_pageNumber - 1) * $this->_quantity;
        $limitClause = " LIMIT $start, $this->_quantity";
        $orderClause = " ORDER BY $this->_sortColumn $this->_sortOrder";

        $clauseList = "";
        if (count($this->_arrayClauses) > 0) {
            $clauseList = implode(" AND ", $this->_arrayClauses);
            $clauseList = " WHERE " . $clauseList;
            if ($this->_enableForcedCondition && $this->_betweenClause) {
                $clauseList .= ' and ' . $this->_betweenClause;
            }
            if ($this->_groupByClause != false) {
                $clauseList .= ' GROUP BY ' . $this->_groupByClause;
            }
        } else {
            if ($this->_enableForcedCondition && $this->_betweenClause) {
                $clauseList = " WHERE ";
                $clauseList .= $this->_betweenClause;
            }
            if ($this->_groupByClause != false) {
                $clauseList .= ' GROUP BY ' . $this->_groupByClause;
            }
        }

        $sumCheck = (sizeof($this->_sum) > 0) ? true : false;
        $columnCheck = (sizeof($this->_column) > 0) ? true : false;
        $avgCheck = (sizeof($this->_avg) > 0) ? true : false;
        /* print_r($this->_sum);
          print_r($this->_column);
          print_r($this->_avg); */

        if ($sumCheck || $columnCheck || $avgCheck) {
            $dataQueryString = 'SELECT ';

            if ($columnCheck) {
                $dataQueryString .= implode(',', $this->_column);
            }
            if ($sumCheck) {
                if ($columnCheck)
                    $dataQueryString .= ',';

                $dataQueryString .= implode(',', $this->_sum);
            }
            if ($avgCheck) {
                if ($avgCheck)
                    $dataQueryString .= ',';

                $dataQueryString .= implode(',', $this->_avg);
            }

            $dataQueryString .= " FROM $this->_mainTable " . $clauseList;

            // echo $dataQueryString;
            // echo "<br>";

            $queryResult = $this->_databaseConnection->FetchAllArray($dataQueryString);
            $this->_dataResultCount = count($queryResult);
            $this->_dataResultArray = $queryResult;
            //print_r($queryResult);
            if ($this->_dataResultCount > 0)
                $this->_mainTablePermissibleValues = implode(", ", array_keys($this->_dataResultArray[0]));
        }

        /*         * *
         * @author Jitendra Singh Bhadouria
         * @desc removes the unnecessary count query when not required
         */
        elseif ($this->_noPagination == 'Y') {
//            $countQueryString = "SELECT COUNT(*) as dataTotalCount FROM $this->_mainTable " . $clauseList;
            // echo $countQueryString;
            // echo "<br>";
//            $queryResult = $this->_databaseConnection->FetchAllArray($countQueryString);
//            $this->_dataTotalCount = $queryResult[0]['dataTotalCount'];
            $this->_dataTotalPages = 1;

            if ($this->_count == 'N') {

                $dataQueryString = "SELECT * FROM $this->_mainTable " . $clauseList . $orderClause . $limitClause;

                if ($this->_order == 'N') {
                    $dataQueryString = "SELECT * FROM $this->_mainTable " . $clauseList . $limitClause;
                }
                // echo $dataQueryString;
                // echo "<br>";
                $queryResult = $this->_databaseConnection->FetchAllArray($dataQueryString);
                $this->_dataResultCount = count($queryResult);
                $this->_dataResultArray = $queryResult;
                //print_r(array_keys($queryResult[0]));
                if ($this->_dataResultCount > 0)
                    $this->_mainTablePermissibleValues = implode(", ", array_keys($this->_dataResultArray[0]));
            }
        }
        else {
            $countQueryString = "SELECT COUNT(*) as dataTotalCount FROM $this->_mainTable " . $clauseList;
            // echo $countQueryString;
            // echo "<br>";

            $queryResult = $this->_databaseConnection->FetchAllArray($countQueryString);
            if ($this->_groupByClause != false) {
                $this->_dataTotalCount = count($queryResult);
            } else {
                $this->_dataTotalCount = $queryResult[0]['dataTotalCount'];
            }
			// echo "<br/>Qty=".$this->_quantity;
			// echo "<br/>total=".$this->_dataTotalCount;
            //echo "<br/>pages=";
			$this->_dataTotalPages = ceil($this->_dataTotalCount / $this->_quantity);

            if ($this->_count == 'N') {

                $dataQueryString = "SELECT * FROM $this->_mainTable " . $clauseList . $orderClause . $limitClause;

                if ($this->_order == 'N') {
                    $dataQueryString = "SELECT * FROM $this->_mainTable " . $clauseList . $limitClause;
                }
                // echo $dataQueryString;
                // echo "<br>";

                $queryResult = $this->_databaseConnection->FetchAllArray($dataQueryString);
                $this->_dataResultCount = count($queryResult);
                $this->_dataResultArray = $queryResult;
                //print_r(array_keys($queryResult[0]));
                if ($this->_dataResultCount > 0)
                    $this->_mainTablePermissibleValues = implode(", ", array_keys($this->_dataResultArray[0]));
            }
        }
        
        //   echo $countQueryString;
    }

    function get($keyName, $index = 0, $xindex = 1) {
        $range = $this->_dataResultCount > 0 ? $this->_dataResultCount - 1 : $this->_dataResultCount;

        if ($index >= $this->_dataResultCount)
        //throw new Exception("Your requested index '$index' is invalid. Range for get should be 0 - $range");
            throw new Exception(gettext("Base : ") . gettext("Your requested index ") . $index . gettext(" is invalid. Range for get should be 0 - ") . $range);

        if (!array_key_exists($keyName, $this->_dataResultArray[$index])) {
            $countExpandableTables = count($this->_expandableTables);
            if ($countExpandableTables == 0) {
                $validArguments = $this->_mainTablePermissibleValues;
                //throw new Exception("Your requested argument '$keyName' is invalid. Valid arguments : $validArguments");
                throw new Exception(gettext("Base : ") . gettext("Your requested argument ") . $keyName . gettext(" is invalid. Valid arguments : ") . $validArguments);
            }

            if (isset($this->_dataResultArray[$index][$xindex])) {
                if (array_key_exists($keyName, $this->_dataResultArray[$index][$xindex])) {
                    return $this->_dataResultArray[$index][$xindex][$keyName];
                }
            }

            $assetTypeId = '';

            if ($this->_mainTable == 'cms_asset')
                $assetTypeId = $this->_dataResultArray[$index]['assetTypeId'];
            else if ($this->_mainTable == 'cms_image') {
                $objAssetType = new Cms_DO_AssetType();
                $cId = $this->_dataResultArray[$index]['clientId'];
                $objAssetType->set("clientId||$cId", "assetTypeSlug||image");
                $assetTypeId = $objAssetType->get('assetTypeId', 0);
            } else if ($this->_mainTable == 'cms_video') {
                $objAssetType = new Cms_DO_AssetType();
                $cId = $this->_dataResultArray[$index]['clientId'];
                $objAssetType->set("clientId||$cId", "assetTypeSlug||video");
                $assetTypeId = $objAssetType->get('assetTypeId', 0);
            }

            if (!array_key_exists($assetTypeId, $this->_arrayExpandablePermissibleValues)) {
                $tempClientId = $this->_dataResultArray[$index]['clientId'];
                $this->setExpandablePermissibleValues($tempClientId, $assetTypeId);
            }

            // describe( $keyName, true );
            $_xElementId = array_search($keyName, $this->_arrayExpandablePermissibleValues[$assetTypeId]);

            if ($_xElementId === FALSE) {
                $validArguments = $this->_mainTablePermissibleValues . ", " . $this->_expandableTablesPermissibleValues;
                //throw new Exception("Your requested argument '$keyName' is invalid. Valid arguments : $validArguments");
                throw new Exception(gettext("Base : ") . gettext("Your requested argument ") . $keyName . gettext(" is invalid. Valid arguments : ") . $validArguments);
            } else {
                $tempJoinColumnId = $this->_dataResultArray[$index][$this->_foreignKey];
                $condition = "$this->_foreignKey = $tempJoinColumnId AND xGroupId = $xindex";

                $queryCount = 0;
                foreach ($this->_expandableTables as $value) {
                    $queryString = "SELECT xElementId, xElementValue FROM $value WHERE $condition";
                    $queryResult = $this->_databaseConnection->FetchAllArray($queryString);
                    $queryCount = count($queryResult);

                    for ($_qr = 0; $_qr < $queryCount; $_qr++) {
                        $this->_dataResultArray[$index][$xindex][$this->_arrayExpandablePermissibleValues[$assetTypeId][$queryResult[$_qr]['xElementId']]] = $queryResult[$_qr]['xElementValue']; // Wow !!!
                    }
                }

                if (isset($this->_dataResultArray[$index][$xindex][$keyName]))
                    return $this->_dataResultArray[$index][$xindex][$keyName];
                else
                    return false;
            }
        }
        else {
            if ('assetElementsOrder' == $keyName) {
                if ('' == $this->_dataResultArray[$index][$keyName])
                    return array();
                else
                    return explode(",", $this->_dataResultArray[$index][$keyName]);
            }
            return $this->_dataResultArray[$index][$keyName];
        }
    }

    function getResultCount() {
        return $this->_dataResultCount;
    }

    function getTotalCount() {
        return $this->_dataTotalCount;
    }

    function getLastInsertedId() {
        return $this->_lastInsertedId;
    }

    function getQuantity() {
        return $this->_quantity;
    }

    public function add() {

        foreach ($this->_requiredAddColumns as $column => $validationType) {
            if (!isset($this->_arrayUpdatedData[$column])) {
                throw new Exception(__CLASS__ . " : $column is a required field!");
            } else {
                $value = trim($this->_arrayUpdatedData[$column]);
                if ($validationType == 'string') {
                    if (!$value) {
                        throw new Exception(__CLASS__ . " : $column should be a non-empty string!");
                    }
                } else if ($validationType == 'number') {
                    if (!$value || !is_numeric($value)) {
                        throw new Exception(__CLASS__ . " : $column should be a natural number!");
                    }
                } else if (substr($validationType, 0, 4) == 'enum') {
                    $values = explode(',', trim(str_replace('enum(', '', $validationType), ')'));
                    if (!$value || array_search($value, $values) === false) {
                        throw new Exception(__CLASS__ . " : $column should be on of these strings: " . implode(', ', $values) . ' !');
                    }
                }
            }
        }

        $queryData = array();
        foreach ($this->_updateColumnNames as $column) {
            if (isset($this->_arrayUpdatedData[$column])) {
                $queryData[$column] = trim($this->_arrayUpdatedData[$column]);
            }
        }

        foreach ($this->_runtimeAddColumns as $column => $value) {
            $queryData[$column] = $value;
        }
       
        $queryData = $this->beforeAdd($queryData);
        if ($queryData !== false) {
            try {
               
                $this->_lastInsertedId = $this->_databaseConnection->QueryInsert($this->_mainTable, $queryData);
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        
    }

    function edit() {
        //required and numeric
        $pkValue = isset($this->_arrayUpdatedData[$this->_foreignKey]) ? trim($this->_arrayUpdatedData[$this->_foreignKey]) : false;
        if (!$pkValue || !is_numeric($pkValue)) {
            throw new Exception(__CLASS__ . " : $this->foreignKey should be a natural number!.");
        }

        $queryData = array();
        foreach ($this->_updateColumnNames as $column) {
            if (isset($this->_arrayUpdatedData[$column])) {
                $queryData[$column] = trim($this->_arrayUpdatedData[$column]);
            }
        }

        try {
            $this->_databaseConnection->QueryUpdate($this->_mainTable, $queryData, " $this->_foreignKey = '$pkValue'");
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function delete() {
        //required and numeric
        $pkValue = isset($this->_arrayUpdatedData[$this->_foreignKey]) ? trim($this->_arrayUpdatedData[$this->_foreignKey]) : false;
        if (!$pkValue || !is_numeric($pkValue)) {
            throw new Exception(__CLASS__ . " : $this->foreignKey should be a natural number!.");
        }

        try {
            $this->_databaseConnection->QueryDelete($this->_mainTable, " $this->_foreignKey = '$pkValue'");
        } catch (Exception $ex) {
            throw new Exception('Problem in record deletion' . $ex->getMessage());
        }
    }

    private function setExpandablePermissibleValues($clientId, $assetTypeId = 0) {
        if ('cms_asset' == $this->_mainTable || 'cms_image' == $this->_mainTable || 'cms_video' == $this->_mainTable) {
            $this->_arrayExpandablePermissibleValues[$assetTypeId] = $this->getAssetPermissibleValuesByTypeId($clientId, $assetTypeId);
            $this->_expandableTablesPermissibleValues = implode(", ", $this->_arrayExpandablePermissibleValues[$assetTypeId]);
        }
    }

    protected function getUserPermissibleValues($clientId) {
        $userElements = new Cms_DO_UserElement();
        $userElements->set("clientId||$clientId");
        $totalResultCount = $userElements->getResultCount();

        $userPermissibleValues = Array();
        if ($totalResultCount > 0) {

            for ($_pv = 0; $_pv < $totalResultCount; $_pv++) {
                $userPermissibleValues[$userElements->get("userElementId", $_pv)] = $userElements->get("elementName", $_pv);
            }
        }
        return $userPermissibleValues;
    }

    protected function getAssetPermissibleValuesByTypeId($clientId, $assetTypeId) {
        $assetTypesObj = new Cms_AssetTypeElements("assetTypeId||$assetTypeId");

        if ($assetTypesObj->getResultCount() > 0) {
            $assetPermissibleValues = Array();
            foreach ($assetTypesObj() as $assetTypeObj) {
                $assetPermissibleValues[$assetTypeObj->getAssetTypeElementId()] = $assetTypeObj->getElementName();
            }
            return $assetPermissibleValues;
        } else {
            throw new Exception(gettext("Base : ") . $assetTypeId . gettext(" is invalid"));
        }
    }

    function ipmlIf($keyName, $index = 0) {
        $range = $this->_dataResultCount > 0 ? $this->_dataResultCount - 1 : $this->_dataResultCount;

        if ($index >= $this->_dataResultCount)
        //throw new Exception("Your requested index '$index' is invalid. Range for get should be 0 - $range");
            throw new Exception(gettext("Base : ") . gettext("Your requested index ") . $index . gettext(" is invalid. Range for get should be 0 - ") . $range);

        if (!array_key_exists($keyName, $this->_dataResultArray[$index])) {
            $validArguments = $this->_mainTablePermissibleValues;
            //throw new Exception("Your requested argument '$keyName' is invalid. Valid arguments : $validArguments");
            throw new Exception(gettext("Base : ") . gettext("Your requested argument ") . $keyName . gettext(" is invalid. Valid arguments : ") . $validArguments);
        } else {
            if ('' == $this->_dataResultArray[$index][$keyName])
                return false;
        }

        return true;
    }

    public function getTotalPages() {
        return $this->_dataTotalPages;
    }

    function __call($functionName, $argumentsArray) {
        if (substr($functionName, 0, 3) == 'get') {
            $columnName = lcfirst(substr($functionName, 3));
            return $this->get($columnName, count($argumentsArray) > 0 ? $argumentsArray[0] : 0, count($argumentsArray) > 1 ? $argumentsArray[1] : 0);
        } else {
            throw new Exception("Call to undefinded function '$functionName'!");
        }
    }

    function getRecord($index = false) {
        if ($index === false) {
            return $this->_dataResultArray;
        } else {
            $range = $this->_dataResultCount > 0 ? $this->_dataResultCount - 1 : $this->_dataResultCount;
            if ($index >= $this->_dataResultCount) {
                throw new Exception(gettext("Base : ") . gettext("Your requested index ") . $index . gettext(" is invalid. Range for get should be 0 - ") . $range);
            }
            return $this->_dataResultArray[$index];
        }
    }

    private function getClassName() {
        $table = explode('_', $this->_mainTable);
        $prefix = ucfirst($table[0]);
        $className = str_replace(' ', '_', ucwords(implode(' ', $table)));
        return "{$prefix}_DO_" . str_replace("{$prefix}_", '', $className);
    }

    protected function beforeAdd($queryData) {
        return $queryData;
    }

    public function fillData($record) {
        $className = $this->getClassName();
        $caseIsFor = 'add';
        if (!is_array($this->_foreignKey)) {
            if (!isset($record[$this->_foreignKey])) {
                throw new Exception('Unable to process data!');
            }
            $primaryKeyValue = $record[$this->_foreignKey];
            $selfObj = new $className();
            $selfObj->set($this->_foreignKey . "||$primaryKeyValue", 'count||Y');
            if ($selfObj->getTotalCount() > 0) {
                $caseIsFor = 'edit';
            }
        } else {
            $selfObj = new $className();
            $args = array();
            foreach ($this->_foreignKey as $column) {
                if (!isset($record[$column])) {
                    throw new Exception('Unable to process data!');
                } else {
                    $args[] = "\"$column||" . addslashes($record[$column]) . '"';
                }
            }
            $args = implode(', ', $args);
            eval('$selfObj->set( ' . $args . ' );');
            if ($selfObj->getTotalCount() > 0) {
                $caseIsFor = 'edit';
            }
        }

        $this->_arrayUpdatedData = $record;
        $this->_task = $caseIsFor;
        $this->$caseIsFor();
        return $caseIsFor;
    }

}
