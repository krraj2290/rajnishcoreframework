<?php

/**
 * This Class is a singleton class which provides CRUD functions related to mysql database
 *
 */
class Rajnish_DbConnection {

// holds single instance
    protected static $hinst = null;
    protected static $hinstm = null;
    protected static $hinsts = null;

    /**
     * Constructor
     * @return Database Class
     */
//    private function __construct() {
//        $this->hinstm = Goquii_MasterDbConnection::GetInstance();
//        $this->hinsts = Goquii_SlaveDbConnection::GetInstance();
//    }

    function __destruct() {
        // ParentClass has a close() method
        if (self::$hinstm !== null) {
            self::$hinstm->Close();
        }
        if (self::$hinsts !== null) {
            self::$hinsts->Close();
        }
    }

    /**
     * Return instance
     * @return instance
     */
//    public static function GetInstance() {
//
//        if (!self::$hinst) {
//            //echo "Network";
//            //echo "<br/>";
//            self::$hinst = new self();
//        }
//
//        return self::$hinst;
//    }

    /**
     * 
     * @param $sql contains sql 'select' query
     * @return array
     */
    function FetchAllArray($sql) {
        //fetch all array from slave
        $sql = trim($sql);
        $temp_sql = $sql;
        $arr_temp_sql = explode(" ", $temp_sql);
        $check_sql_term = strtolower(trim($arr_temp_sql[0]));
        if ('select' == $check_sql_term) {
            return $this->hinsts->FetchAllArray($sql);
        } else
            throw new Exception("Hey, you are not using select statement.");
    }

    /**
     * Updates an table values based on $data and $where
     * @param $table, string, tablename
     * @param $data, array, [name][value] pair
     * @param $where, string, condition
     * @return string, just check for true or false
     */
    function QueryUpdate($table, $data, $where = '1') {
        return $this->hinstm->QueryUpdate($table, $data, $where);
    }

    /**
     * Delete an table values based on $where
     * @param $table, string, tablename
     * @param $where, string, condition
     * @return string, just check for true or false
     */
    function QueryDelete($table, $where = '1') {
        return $this->hinstm->QueryDelete($table, $where);
    }

    /**
     * Insert data into table 
     * @param $table, string, table name
     * @param $data, array, key => value pair
     * @return int, primary key of the inserted record
     */
    function QueryInsert($table, $data, $autoIncrement = true) {
        return $this->hinstm->QueryInsert($table, $data, $autoIncrement);
    }

    /**
     * 
     * @param $msg, string, error message
     * @return void
     */
    function oops($msg = '') {
        if ($this->link_id > 0) {
            $this->error = mysql_error($this->link_id);
            $this->errno = mysql_errno($this->link_id);
        } else {
            $this->error = mysql_error();
            $this->errno = mysql_errno();
        }
        throw new Exception($this->error);
    }

    /**
     * Hack to run any query on master - specially put in for running bulk inserts & joins
     * @param type $query - sql query string
     * @param type $escape - true if query needs to be escaped
     */
    function RunMyQuery($query, $escape = true) {
        if ($escape)
            $query = $this->hinstm->Escape($query);

        return $this->hinstm->Query($query);
    }

    /**
     * @ Javed Ali
     * 
     * Hack to run any query on slave - specially put in for running bulk inserts & joins
     * @param type $query - sql query string
     * @param type $escape - true if query needs to be escaped
     */
    function RunMyQuerySlave($query, $escape = true) {
        $rds = SPECIAL_QUERY_RDS_SLAVE;
        $queryLower = strtolower($query);
        $condition = strpos($queryLower, 'insert ');
        if ($condition !== false) {
            $rds = SPECIAL_QUERY_RDS_MASTER;
        }
        $condition = strpos($queryLower, 'update ');
        if ($condition !== false) {
            $rds = SPECIAL_QUERY_RDS_MASTER;
        }
        $condition = strpos($queryLower, 'delete from ');
        if ($condition !== false) {
            $rds = SPECIAL_QUERY_RDS_MASTER;
        }
        if ($rds == SPECIAL_QUERY_RDS_SLAVE) {
            if ($escape)
                $query = $this->hinsts->Escape($query);

            return $this->hinsts->Query($query);
        }else {
            if ($escape)
                $query = $this->hinstm->Escape($query);

            return $this->hinstm->Query($query);
        }
    }

}

?>