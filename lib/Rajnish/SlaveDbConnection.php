<?php

/**
 * This Class is a singleton class which provides CRUD functions related to mysql database
 *
 */
class Rajnish_SlaveDbConnection {

    protected $server = ""; //database server
    protected $user = ""; //database login name
    protected $pass = ""; //database login password
    protected $database = ""; //database name
    protected $pre = ""; //table prefix
    protected $autoincrement_pre = "";
//internal info
    private $error = "";
    private $errno = 0;
//number of rows affected by SQL query
    private $affected_rows = 0;
    private $link_id = 0;
    private $query_id = 0;
// holds single instance
    protected static $hinst = null;

    /**
     * Constructor
     * @return Database Class
     */
//    private function __construct() {
//        $this->Connect(true);
//    }
//
//    /**
//     * Return instance
//     * @return instance
//     */
//    public static function GetInstance() {
//        if (!self::$hinst) {
//            self::$hinst = new self();
//        }
//
//        return self::$hinst;
//    }

    /**
     * Connects to database
     * @param $new_link false, if u need a single connection thread
     * @return void
     */
    protected function Connect($new_link = false) {
        $this->link_id = @mysql_connect($this->server, $this->user, $this->pass, $new_link);

        if (!$this->link_id) {//open failed
            echo "<h2>Come back soon..</h2>";
            die;
//            $this->oops("Could not connect to server: <b>$this->server</b>.");
        }

        if (!@mysql_select_db($this->database, $this->link_id)) {//no database
            echo "<h2>Come back soon..</h2>";
            die;
//            $this->oops("Could not open database: <b>$this->database</b>.");
        }

        // unset the data so it can't be dumped
        $this->server = '';
        $this->user = '';
        $this->pass = '';
        $this->database = '';
    }

    /**
     * Close the mysql connection
     * @return void
     */
    private function Close() {
        if (!@mysql_close($this->link_id)) {
            $this->oops("Connection close failed.");
        }
    }

    /**
     * 
     * @param $string string value, to be cleaned from malicious characters
     * @return string
     */
    private function Escape($string) {
        if (( get_magic_quotes_runtime() == 1 ) || ( get_magic_quotes_gpc() == 1 )) {
            $string = stripslashes($string);
        }
        return mysql_real_escape_string($string);
    }

    /**
     * 
     * @param $sql string, real sql queries
     * @return true, false based on success or failure
     */
    function Query($sql) {
        global $_slave_query;
        global $_total_slave_query;
        /**
         * @author Jitendra Singh Bhadouria
         * @desc This constant can be set true for a particular page to print query for that page only.
         */
//        if (defined('PRINT_SLAVE_QUERY')) {
//            echo "/* \n" . $sql . "*/";
////            echo $sql . "<br/>";
//        }
//        if (defined('TOTAL_SLAVE_QUERY')) {
//            $_slave_query[] = $sql;
//        }
        $starttime = microtime(true);
        $this->query_id = @mysql_query($sql, $this->link_id);
        $endtime = microtime(true);
//        if (defined('SHOW_QUERY_TIME')) {
//            if (($endtime - $starttime) > 0.09) {
//                echo "/* \n<div style='color:red' ><b>" . $sql . "</b></div>*/";
//                echo "/* \n <div style='color:red' >Slave Query time:  " . ($endtime - $starttime) . " sec </div>*/ \n";
//            } else {
//                echo "/* \n <div style='color:green' ><b>" . $sql . "</b></div>*/";
//                echo "/* \n <div style='color:green' >Slave Query time:  " . ($endtime - $starttime) . " sec  </div>*/ \n";
//            }
//            $_total_slave_query = ($_total_slave_query + ($endtime - $starttime));
//        }

//        if (ELASTIC_SEARCH_DB_CONNECTION) {
//            $commentArr['@query'] = $sql;
//            $commentArr['@queryTime'] = ($endtime - $starttime);
//            $commentArr['@time'] = date("d M Y h:i A");
//            $commentArr['@server'] = "slave";
//            $timestamp = date("Y-m-d H:i:s");
//            $timestamp = str_replace(" ", "T", $timestamp);
//            $commentArr['@timestamp'] = $timestamp . ".325z";
//            if (($endtime - $starttime) > 0.09) {
//                $commentArr['@color'] = "red";
//            } else {
//                $commentArr['@color'] = "green";
//            }
////            $dbConncetionObjects = new RabbitMQ_Dbconnection();
////            $dbConncetionObjects->_produce($commentArr);
//        }
        if (defined('DEBUG_BACK_TRACE')) {
            echo "<br/><b>debug back trace: </b>";
            $debug = debug_backtrace();
            print_r($debug[5]['file']);
            print_r($debug[5]['line']);
            echo " <br/>";
            print_r($debug[6]['file']);
            print_r($debug[6]['line']);
            echo " <br/>";
            print_r($debug[7]['file']);
            print_r($debug[7]['line']);
            echo " <br/>";
            echo " <br/>";
            print_r($debug[8]['file']);
            print_r($debug[8]['line']);
            echo " <br/>";
            echo " <br/>";
            print_r($debug[9]['file']);
            print_r($debug[9]['line']);
            echo " <br/>";
            echo " <br/>";
            print_r($debug[10]['file']);
            print_r($debug[10]['line']);
            echo " <br/>";
            echo " <br/>";
            print_r($debug[11]['file']);
            print_r($debug[11]['line']);
            echo " <br/>";
            echo " <br/>" . PHP_EOL;
        }
        if (!$this->query_id) {
            $this->oopUserDefines("<b>oops!MySQL Query fail</b>");
            return 0;
        }

        $this->affected_rows = @mysql_affected_rows($this->link_id);

        return $this->query_id;
    }

    /**
     * 
     * @param $query_id
     * @return array
     */
    private function FetchArray($query_id = -1) {
        // retrieve row
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }

        if (isset($this->query_id)) {
            $record = @mysql_fetch_assoc($this->query_id);
        } else {
            $this->oops("Invalid query_id: <b>$this->query_id</b>. Records could not be fetched.");
        }

        return $record;
    }

    /**
     * 
     * @param $sql contains sql 'select' query
     * @return array
     */
    function FetchAllArray($sql) {
        $query_id = $this->Query($sql);
        $out = array();

        while ($row = $this->FetchArray($query_id, $sql)) {
            $out[] = $row;
        }

        $this->FreeResult($query_id);
        return $out;
    }

    /**
     * 
     * @param $query_id
     * @return void
     */
    private function FreeResult($query_id = -1) {
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }
        if ($this->query_id != 0 && !@mysql_free_result($this->query_id)) {
            $this->oops("Result ID: <b>$this->query_id</b> could not be freed.");
        }
    }

    /**
     * 
     * @param $msg, string, error message
     * @return void
     */
    function oopUserDefines($msg = '') {
        echo $msg;
    }

    /**
     * 
     * @param $msg, string, error message
     * @return void
     */
    function oops($msg = '') {
        if ($this->link_id > 0) {
            $this->error = mysql_error($this->link_id);
            $this->errno = mysql_errno($this->link_id);
        } else {
            $this->error = mysql_error();
            $this->errno = mysql_errno();
        }
        throw new Exception($this->error);
    }

}

?>
