<?php
	abstract class Rajnish_ModelBase {
		protected $_dbClassPrefix = '';
		protected $_foreignKey = '';
		protected $_recordArray = array();
		protected $_dbClass = '';
		protected $_relatedClasses = array();
		private $_relatedInfoLoaded = 0;
		protected $_errorMessage = '';
		protected $_varDumped = false;
		protected $_expandableElements = array();

		/**
		 * 
		 * @param $primaryKeyValue
		 * @access public
		 * @return Object
		 */
		public function __construct( $primaryKeyValue ) {
			if (empty($this->_dbClassPrefix)) {
				throw new Exception("Db class prefix cannot be empty!");
			}
			if (is_array($primaryKeyValue)) {
				if (isset($primaryKeyValue[$this->_foreignKey])) {
					$this->_init($primaryKeyValue);
				} else {
					throw new Exception(gettext($this->_dbClass . ' : ') . gettext('Object initialization not possible with provided data!'));
				}
			} else if ($primaryKeyValue > 0) {
				$dbClass = $this->_dbClassPrefix . $this->_dbClass;
				$selfDbObj = new $dbClass();
				$selfDbObj->set("$this->_foreignKey||$primaryKeyValue","order||N","noPagination||Y","quantity||1");

				if ($selfDbObj->getResultCount() > 0) {
					$this->_init($selfDbObj->getRecord(0));
				}
			} else {
				throw new Exception(gettext($this->_dbClass . ' : ') . gettext($this->_foreignKey . ' should be natural number!'));
			}
		}

		public function refresh() {
			$dbClass = $this->_dbClassPrefix . $this->_dbClass;
			$selfDbObj = new $dbClass();
			$runFunction = 'get' . ucfirst( $this->_foreignKey );
			$primaryKeyValue = $this->$runFunction();
			$selfDbObj->set( "$this->_foreignKey||$primaryKeyValue" );

			if ($selfDbObj->getResultCount() > 0) {
				$this->_init($selfDbObj->getRecord(0));
			}
		}

		public function getMessage() {
			return $this->_errorMessage;
		}

		private function _init($recordArray) {
			$this->_recordArray = $recordArray;
		}

		public function getResultCount() {
			return count( $this->_recordArray ) > 0 ? 1 : 0;
		}

		public function getTotalCount() {
			return $this->getResultCount();
		}

		/**
		 * 
		 * @param string $functionName
		 * @param array $arguments
		 * @return mixed
		 */
		public function __call( $functionName, $arguments ) {
			if( substr( $functionName, 0, 10 ) == 'getElement' ) {
				$order = count( $arguments ) > 0 ? $arguments[ 0 ] : 1;
				$this->varDump();
				$varName = lcfirst( substr( $functionName, 10 ) );
				return isset( $this->_expandableElements[ $order ][ $varName ] ) ? $this->_expandableElements[ $order ][ $varName ] : false;
			} else if( substr( $functionName, 0, 3 ) == 'get' ) {
				$varName = lcfirst( substr( $functionName, 3 ) );
				if( isset( $this->{ '_' . $varName } ) ) {
					return $this->{ '_' . $varName };
				}
				if( isset( $this->_recordArray[ $varName ] ) ) {
					return $this->_recordArray[ $varName ];
				} else {
					$this->varDump();
					return isset( $this->_recordArray[ $varName ] ) ? $this->_recordArray[ $varName ] : false;
				}
			} else {
				throw new Exception( "Call to undefinded function '$functionName'!" );
			}
		}

		public function varDump() {
			if( !$this->_varDumped ) {
				while( $this->_relatedInfoLoaded != count( $this->_relatedClasses ) ) {
					$this->_loadRelatedInfo();
				}
				try {
					$expClass = str_replace( '_', '_DO_', get_called_class() . 'Expandable' );
					$expObj = new $expClass();
					$runFunction = 'get' . ucfirst( $this->_foreignKey );
					$expObj->set( $this->_foreignKey . '||' . $this->$runFunction(), 'quantity||10000' );
					if( $expObj->getResultCount() > 0 ) {
						$possibleOrderColumns = array( 'elementGroup', 'storyGroupId' );
						$records = $expObj->getRecord();
						$orderColumn = current( array_intersect( array_keys( $records[ 0 ] ), $possibleOrderColumns ) );
						if( $orderColumn !== false ) {
							foreach( $records as $record ) {
								$this->_expandableElements[ $record[ $orderColumn ] ][ $record[ 'fieldName' ] ] = $record;
							}
						}
					}
				} catch( Exception $ex ) {}
				$this->_varDumped = true;
			}
			return $this->_recordArray;
		}

		/**
		 * Loads additional information from related classes.
		 * @return void
		 */
		private function _loadRelatedInfo() {
			$nextClass = $this->_relatedClasses[$this->_relatedInfoLoaded++];
			$runFunction = 'get' . ucfirst($this->_foreignKey);
			$nextClassObj = new $nextClass();
			$nextClassObj->set( $this->_foreignKey . '||' . $this->$runFunction());

			if ($nextClassObj->getResultCount() > 0) {
				$this->_recordArray = array_merge($this->_recordArray, $nextClassObj->getRecord(0));
			}
		}

		/**
		 * 
		 * @return array
		 */
		public function getColumnNames() {
			return array_keys( $this->_recordArray );
		}

		/**
		 * Updates information of a user to database.
		 * @return boolean
		 * $userObj->update( "userFirstName||Rashid", "userLastName||Mohamad" );
		 */
		public function update($firstArgument) {
			if( func_num_args() > 0 ) {
				$functionArguments = is_array( $firstArgument ) ? $firstArgument : func_get_args();
				$dbClass = $this->_dbClassPrefix . $this->_dbClass;
				$runFunction = 'get' . ucfirst( $this->_foreignKey );
				$argumentList = '"' . str_replace('<--- BO --->', '", "', str_replace('"', '\"', implode('<--- BO --->', $functionArguments))) . '", "' . $this->_foreignKey . '||' . $this->$runFunction() . '"';
				$selfDbObj = new $dbClass( 'edit' );
				try {
					eval( '$selfDbObj->set( ' . $argumentList . ' );' );
//					$this->refresh();
					return true;
				} catch( Exception $ex ) {
                                    /**
                                     * eliminated by Jitendra Singh Bhadouria because it doesn't defined anywhere
                                     */
//					describe( $ex->getMessage(), $ex->getTraceAsString(), true );
					$this->_errorMessage = $ex->getMessage();
					return false;
				}
			} else {
				throw new Exception($this->_dbClass . ' : No data found for updation!');
			}
		}

		protected function beforeDelete() {
			return true;
		}

		public function delete() {
			if( $this->beforeDelete() ) {
				$dbClass = $this->_dbClassPrefix . $this->_dbClass;
				$selfDbObj = new $dbClass( 'delete' );
				$runFunction = 'get' . ucfirst( $this->_foreignKey );
				$selfDbObj->set( $this->_foreignKey . '||' . $this->$runFunction() );
				$this->_recordArray = array();
			}
		}
	}
