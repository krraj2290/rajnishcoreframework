<?php

/**
 * This Class is a singleton class which provides CRUD functions related to mysql database
 *
 */
class Rajnish_MasterDbConnection {

    // Server Variables
    protected $server = ""; //database server
    protected $user = ""; //database login name
    protected $pass = ""; //database login password
    protected $database = ""; //database name
    protected $pre = ""; //table prefix
    protected $autoincrement_pre = "";
    //internal info
    private $error = "";
    private $errno = 0;
    //number of rows affected by SQL query
    private $affected_rows = 0;
    private $link_id = 0;
    private $query_id = 0;
    // holds single instance
    protected static $hinst = null;

    /**
     * Constructor
     * @return Database Class
     */
//    private function __construct() {
//        $this->Connect(true);
//    }
//
//    /**
//     * Return instance
//     * @return instance
//     */
//    public static function GetInstance() {
//        if (!self::$hinst) {
//            self::$hinst = new self();
//        }
//
//        return self::$hinst;
//    }

    /**
     * Connects to database
     * @param $new_link false, if u need a single connection thread
     * @return void
     */
    function Connect($new_link = false) {
       $this->link_id = @mysql_connect($this->server, $this->user, $this->pass, $new_link);

        if (!$this->link_id) {//open failed
            echo "<h2>Come back soon..</h2>";
            die;
//            $this->oops("Could not connect to server: <b>$this->server</b>.");
        }

        if (!@mysql_select_db($this->database, $this->link_id)) {//no database
            echo "<h2>Come back soon..</h2>";
            die;
//            $this->oops("Could not open database: <b>$this->database</b>.");
        }

        // unset the data so it can't be dumped
        $this->server = '';
        $this->user = '';
        $this->pass = '';
        $this->database = '';
    }

    /**
     * Close the mysql connection
     * @return void
     */
    function Close() {
        if (!@mysql_close($this->link_id)) {
            $this->oops("Connection close failed.");
        }
    }

    /**
     *
     * @param $string string value, to be cleaned from malicious characters
     * @return string
     */
    function Escape($string) {
        if (( get_magic_quotes_runtime() == 1 ) || ( get_magic_quotes_gpc() == 1 )) {
            $string = stripslashes($string);
        }
        return mysql_real_escape_string($string);
    }

    /**
     *
     * @param $sql string, real sql queries
     * @return true, false based on success or failure
     */
    function Query($sql) {
        global $_master_query;
        /**
         * @author Jitendra Singh Bhadouria
         * @desc This constant can be set true for a particular page to print query for that page only.
         */
//        if (@defined('PRINT_MASTER_QUERY')) {
//            echo "/* \n" . $sql . "*/";
////            echo $sql . "<br/>";
//        }
//        if (defined('TOTAL_MASTER_QUERY')) {
//            $_master_query[] = $sql;
//        }
        $starttime = microtime(true);
        $this->query_id = @mysql_query($sql, $this->link_id);
        $endtime = microtime(true);
//        if (@defined('SHOW_MASTER_QUERY_TIME')) {
//            if (($endtime - $starttime) > 0.09) {
//                echo "/* \n<div style='color:red' ><b>" . $sql . "</b></div>*/";
//                echo "/* \n <div style='color:red' >Slave Query time:  " . ($endtime - $starttime) . " sec </div>*/ \n";
//            } else {
//                echo "/* \n <div style='color:green' ><b>" . $sql . "</b></div>*/";
//                echo "/* \n <div style='color:green' >Slave Query time:  " . ($endtime - $starttime) . " sec  </div>*/ \n";
//            }
//        }

//        if (ELASTIC_SEARCH_DB_CONNECTION) {
//            $commentArr['@query'] = $sql;
//            $commentArr['@queryTime'] = ($endtime - $starttime);
//            $commentArr['@time'] = date("d M Y h:i A");
//            $commentArr['@server'] = "master";
//            $timestamp = date("Y-m-d H:i:s");
//            $timestamp = str_replace(" ", "T", $timestamp);
//            $commentArr['@timestamp'] = $timestamp . ".325z";
//            if (($endtime - $starttime) > 0.09) {
//                $commentArr['@color'] = "red";
//            } else {
//                $commentArr['@color'] = "green";
//            }
////            $dbConncetionObjects = new RabbitMQ_Dbconnection();
////            $dbConncetionObjects->_produce($commentArr);
////            ElasticSearch_DBConnection::addSqlQueryStatus($commentArr);
//        }
        if (!$this->query_id) {
            $this->oopUserDefines("<b>oops!MySQL Query fail</b>");
            return 0;
        }

        $this->affected_rows = @mysql_affected_rows($this->link_id);

        return $this->query_id;
    }

    /**
     *
     * @param $query_id
     * @return array
     */
    private function FetchArray($query_id = -1) {
        // retrieve row
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }

        if (isset($this->query_id)) {
            $record = @mysql_fetch_assoc($this->query_id);
        } else {
            $this->oops("Invalid query_id: <b>$this->query_id</b>. Records could not be fetched.");
        }

        return $record;
    }

    /**
     *
     * @param $sql contains sql 'select' query
     * @return array
     */
    function FetchAllArray($sql) {
        $query_id = $this->Query($sql);
        $out = array();

        while ($row = $this->FetchArray($query_id, $sql)) {
            $out[] = $row;
        }

        $this->FreeResult($query_id);
        return $out;
    }

    /**
     *
     * @param $query_id
     * @return void
     */
    private function FreeResult($query_id = -1) {
        if ($query_id != -1) {
            $this->query_id = $query_id;
        }
        if ($this->query_id != 0 && !@mysql_free_result($this->query_id)) {
            $this->oops("Result ID: <b>$this->query_id</b> could not be freed.");
        }
    }

    /**
     *
     * @param $query_string, contains select query
     * @return array, return only the first row
     */
    function QueryFirst($query_string) {
        $query_id = $this->query($query_string);
        $out = $this->fetch_array($query_id);
        $this->free_result($query_id);
        return $out;
    }

    /**
     * Updates an table values based on $data and $where
     * @param $table, string, tablename
     * @param $data, array, [name][value] pair
     * @param $where, string, condition
     * @return string, just check for true or false
     */
    function QueryUpdate($table, $data, $where = '1') {
        $q = "UPDATE `" . $this->pre . $table . "` SET ";

        foreach ($data as $key => $val) {
            if (strtolower($val) == 'null')
                $q.= "`$key` = NULL, ";
            elseif (strtolower($val) == 'now()')
                $q.= "`$key` = NOW(), ";
            elseif (strtolower($val) == 'unix_timestamp()')
                $q.= "`$key` = UNIX_TIMESTAMP(), ";
            else
                $q.= "`$key`='" . $this->escape($val) . "', ";
        }

        $q = rtrim($q, ', ') . ' WHERE ' . $where . ';';

        //echoLine( $q );

        return $this->query($q);
    }

    /**
     * Delete an table values based on $where
     * @param $table, string, tablename
     * @param $where, string, condition
     * @return string, just check for true or false
     */
    function QueryDelete($table, $where = '1') {
        $q = 'DELETE FROM `' . $this->pre . $table . '` WHERE ' . $where . ';';
        return $this->query($q);
    }

    /**
     * Insert data into table
     * @param $table, string, table name
     * @param $data, array, key => value pair
     * @return int, primary key of the inserted record
     */
    function QueryInsert($table, $data) {
        $q = "INSERT INTO `" . $this->pre . $table . "` ";
        $v = '';
        $n = '';

        foreach ($data as $key => $val) {
            $n.="`$key`, ";
            if (strtolower($val) == 'null')
                $v.="NULL, ";
            elseif (strtolower($val) == 'now()')
                $v.="NOW(), ";
            elseif (strtolower($val) == 'unix_timestamp()')
                $v.="UNIX_TIMESTAMP(), ";
            else
                $v.= "'" . $this->escape($val) . "', ";
        }

        $q .= "(" . rtrim($n, ', ') . ") VALUES (" . rtrim($v, ', ') . ");";

        //echoLine( $q );

        if ($this->query($q)) {
            //$this->free_result();
            return mysql_insert_id($this->link_id); //this can't be done on non autoincremented tables;
        } else
            return false;
    }

    /**
     *
     * @param $msg, string, error message
     * @return void
     */
    function oopUserDefines($msg = '') {
        echo $msg;
    }

    /**
     *
     * @param $msg, string, error message
     * @return void
     */
    function oops($msg = '') {
        if ($this->link_id > 0) {
            $this->error = mysql_error($this->link_id);
            $this->errno = mysql_errno($this->link_id);
        } else {
            $this->error = mysql_error();
            $this->errno = mysql_errno();
        }
        throw new Exception($this->error);
    }

}

?>
