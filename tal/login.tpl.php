<!DOCTYPE html>
<html >
    <head>
        <!--<tal:block metal:use-macro="${APP_TPL}/header.tpl.php/head"/>-->
        <meta charset="UTF-8" />
        <title>User Login Or Register</title>


        <!--<link rel="stylesheet" href="/css/reset.css" />-->
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900' />
        <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' />
        <link rel="stylesheet" href="/css/style.css" />
    </head>
    <body>
        <div class="pen-title">
            <h1>User Login Or Register</h1>
        </div>
        <!-- Form Module-->
        <div class="module form-module" >
          <div style="padding: 40px;">
                <h2>Login to your account</h2>
                <form>
                    <input type="email" class="loginUserEmail input" placeholder="Useremail"/>
                    <input type="password" class="loginUserPassword input" placeholder="Password"/>
                    <input type="button" class="loginUser button" value="Login" />
                </form>
            </div>
            
            <div class="cta"><a href="/forget-password">Forgot your password?</a></div>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="/js/index.js"></script>
    </body>
</html>