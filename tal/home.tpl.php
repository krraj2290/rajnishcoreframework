<!doctype html>
<html>
    <head>
        <title>studentbook</title>
        <script src="/js/home.js"></script>
        <script src="/js/jquery-1.7.1.min.js"></script>
        <link rel="stylesheet" href="/css/home.css" />
        <style>
            .group-sidebar 
            {
                border: 1px solid rgba(29, 49, 91, 0.3);
                height: 100%;
                left: 0;
                padding-bottom: 10px;
                padding-top: 10px;
                position: fixed;
                top: 0;
                width: 15%;
            }
        </style>
    </head>
    <body>
        <input type="hidden" id="hiddenLoginAdminId" value="${adminId}"/>
        <input type="hidden" id="xmppopenfireip" value="52.33.140.191" />
        <div class="group-sidebar">
            <div class="sidebar-name" tal:repeat="user userArr">
                <a href="javascript:register_popup('${user/fName}_${user/userId}', '${user/fName} ${user/lName}');">
                    <img width="30" height="30" src="${user/image}" />
                    <span>${user/fName} ${user/lName}</span>
                    
                </a>
            </div>

        </div>

        <div class="chat-sidebar">
            <div class="sidebar-name" tal:repeat="user userArr">
                <!-- Pass username and display name to register popup -->
                <a href="javascript:register_popup('${user/fName}_${user/userId}', '${user/fName}');">
                    <img width="30" height="30" src="${user/image}" />
                    <span>${user/fName} ${user/lName}</span>
                    <img src="http://52.33.140.191:9090/plugins/presence/status?jid=${user/userId}_user@52.33.140.191" style="height:20px;width:20px" />
                </a>
            </div>

        </div>

    <link rel="stylesheet" href="/xmpp/css/chat.css"/>
    <script src="/xmpp/js/moment.js"></script>
    <script src="/xmpp/js/chat.js"></script>
    <script src="/xmpp/js/strophe.js"></script>
    </body>
</html>