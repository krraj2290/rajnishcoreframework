<tal:block metal:define-macro="head">
    <link rel="stylesheet" href="/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>        
    <!--<link rel="stylesheet" href="/css/alertify.css" type="text/css"/>-->        
    <link rel="stylesheet" href="/css/font-awesome.css" type="text/css"/>
    <link rel="stylesheet" href="/css/iThing.css" type="text/css"/>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'/>
    <script src="/js/jquery-1.7.1.min.js"></script>
 <!--    <script type="text/javascript" src="/js/bootstrap.js"></script>-->

    <script src="/js/bootstrap-modal.js"></script>
    <script src="/js/modernizr.js"></script>
    <script src="/js/jquery.form.js"></script>
    <!--<script src="/js/alertify.js"></script>-->
    <script src="/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script src="/js/jQDateRangeSlider-withRuler-min.js"></script>
    <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
    <script src="/js/alertify.js"></script>
    <link rel="stylesheet" href="/css/alertify.css" type="text/css"/>

    <style>
        .user-primary-nav > ul > li > a {
            padding: 10px 15px;
        }

        .dropdowns-menu {
            background-clip: padding-box;
            background-color: #ffffff;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 6px;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            float: left;
            left: 0;
            list-style: outside none none;
            margin: 2px 0 0;
            min-width: 50px;
            padding: 5px 0;
            position: absolute;
            top: 100%;
            z-index: 1000;
        }
    </style>
<!--    <script>
        $('.dropdown-toggle').dropdown()
    </script>-->
</tal:block>