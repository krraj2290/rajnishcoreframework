
//// Toggle Function
//$('.toggle').click(function () {
//    // Switches the Icon
//    $(this).children('i').toggleClass('fa-pencil');
//    // Switches the forms  
//    $('.form').animate({
//        height: "toggle",
//        'padding-top': 'toggle',
//        'padding-bottom': 'toggle',
//        opacity: "toggle"
//    }, "slow");
//});
$(document).on('click', '.chatSignUpUser', function () {
    var signUpUserFirstName = $(".chatSignUpUserFirstName").val();
    if (signUpUserFirstName == '') {
        alert("First Name Should Not be Blank");
        return false;
    }
    var signUpUserLastName = $(".chatSignUpUserLastName").val();
    var signUpUserPassword = $(".chatSignUpUserPassword").val();
    if (signUpUserPassword == '') {
        alert("Password Should Not be Blank");
        return false;
    }
    var signUpUserEmail = $(".chatSignUpUserEmail").val();
    if (signUpUserEmail == '') {
        alert("Email Should Not be Blank");
        return false;
    }
    var signUpUserMobile = $(".chatSignUpUserMobile").val();

//    popup.showSimpleAjaxLoader();
    $.ajax({
        type: "POST",
        url: "/user-ajax",
        data: {
            action: "signUpNewUserForRegister",
            signUpUserFirstName: signUpUserFirstName,
            signUpUserLastName: signUpUserLastName,
            signUpUserPassword: signUpUserPassword,
            signUpUserEmail: signUpUserEmail,
            signUpUserMobile: signUpUserMobile
        },
        success: function (response) {
            alert(response);
            window.location = "login";
        },
        error: function (e, msg) {
            
        }
    });
})
$(document).on('click', '.loginUser', function () {
    var loginUserEmail = $(".loginUserEmail").val();
    if (loginUserEmail == '') {
        alert("Email Should Not be Blank");
        return false;
    }
    var loginUserPassword = $(".loginUserPassword").val();
    if (loginUserPassword == '') {
        alert("Password Should Not be Blank");
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/user-ajax",
        data: {
            action: "loginUserProfile",
            loginUserEmail: loginUserEmail,
            loginUserPassword: loginUserPassword
            
        },
        success: function (response) {
//            popup.hideSimpleAjaxLoader();
            alert(response);
            window.location = "/home";
        },
        error: function (e, msg) {
            
        }
    });
})
