popup = new Object()
website_url = "http://ec2-52-33-140-191.us-west-2.compute.amazonaws.com/";

popup = {
    open: function () {
        $('body').append('<div class="modal-backdrop"></div>');
        $('.' + arguments[0]).removeClass('hide');
        $(".errorMessage").addClass('hide');
        $(".successMessage").addClass('hide');
        $(".resetInputBox").val('');
        $("#showingImageClassName").removeClass();
        $('.resetSelectField').each(function () {
            $(this).get(0).selectedIndex = 0;
        });

    },
    close: function () {
        $(".modal").addClass('hide');
        $(".modal-backdrop").remove();
        return false;
    },
    openSimplePopUp: function () {
        $('body').append('<div class="modal-backdrop"></div>');
        $('.' + arguments[0]).removeClass('hide');
    },
    closeSimplePopUp: function () {
        //        alert(arguments[0]);
        $('.' + arguments[0]).addClass('hide');
        $(".modal-backdrop").remove();
    },
    showSimpleAjaxLoader: function () {
        $('body').prepend('<div id="ajax_loaders" style="position: fixed; top: 30%; width: 100%; text-align: center; z-index: 9999;"><img src="/webApp/css-img/ajax_loader.gif" style="position: relative; margin: 0px auto; width: 200px; text-align: center;"></div>');
    },
    hideSimpleAjaxLoader: function () {
        $("#ajax_loaders").remove();
    }, openSimplePopUpWithoutBackdrop: function () {
        $('.' + arguments[0]).removeClass('hide');
    },
    closeSimplePopUpWithoutBackdrop: function () {
        $('.' + arguments[0]).addClass('hide');
    }
}

$(".dropdown-toggle").live('click', function (e) {
    e.stopPropagation();
    if ($(this).next().hasClass('hide')) {
        $(this).next().removeClass('hide');
    } else {
        $(this).next().addClass('hide');
    }
})