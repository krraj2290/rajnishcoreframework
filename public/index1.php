<!DOCTYPE html>
<html >
    <head>
        <tal:block metal:use-macro="${APP_TPL}/header.tpl.php/head"/>
        <meta charset="UTF-8" />
        <title>User Login Or Register</title>


        <!--<link rel="stylesheet" href="/css/reset.css" />-->
        <link rel='stylesheet prefetch' href='http://fonts.goo/gleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900' />
        <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' />
        <link rel="stylesheet" href="/css/style.css" />
    </head>
    <body>
        <div class="pen-title">
            <h1>User Login Or Register</h1>
        </div>
        <!-- Form Module-->
        <div class="module form-module" >
            <div style="padding: 40px;">
                <h2>Create an account</h2>
                <form>
                    <input type="text" class="chatSignUpUserFirstName input" placeholder="firstName" autocomplete="off"/>
                    <input type="text" class="chatSignUpUserLastName input" placeholder="lastName" autocomplete="off"/>
                    <input type="password" class="chatSignUpUserPassword input" placeholder="Password" autocomplete="off"/>
                    <input type="email" class="chatSignUpUserEmail input" placeholder="Email Address" autocomplete="off"/>
                    <input type="tel" class="chatSignUpUserMobile input" placeholder="Phone Number" autocomplete="off"/>
                    <input type="button" class="chatSignUpUser button" name="register" value="Register" />
                </form>
            </div>
            <div class="cta"><a href="/login">Already Member</a></div>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <!--<script src='http://codepen.io/andytran/pen/vLmRofferIdVp.js'></script>-->

        <script src="/js/index.js"></script>




    </body>
</html>