<?php

/**
 * @author Rajnish Kr Rajesh <krraj2290@gmail.com>
 * @desc localConfig, defines specific settings,
 * localConfig will get preference over globalConfig.
 */



// Application Path Block Start
defined('APP_PWD') || define('APP_PWD', ( realpath(dirname(__FILE__) . '/../') . '/'));


defined('APP_PATH') || define('APP_PATH', APP_PWD . '');

defined('LIB_PATH') || define('LIB_PATH', APP_PATH . '../lib/');

defined('CONFIG_PATH') || define('CONFIG_PATH', ( APP_PATH . 'config/'));

defined('COMPONENT_PATH') || define('COMPONENT_PATH', ( APP_PATH . '../components/'));

defined('APP_LIB_PATH') || define('APP_LIB_PATH', ( APP_PATH . '../lib/'));

defined('APP_PHP') || define('APP_PHP', ( APP_PATH . 'php/'));

defined('APP_PUBLIC') || define('APP_PUBLIC', ( APP_PATH . 'public/'));

defined('APP_TPL') || define('APP_TPL', ( APP_PATH . 'tal/'));

defined('LURACAST_PATH') || define('LURACAST_PATH', ( APP_PATH . '../luracast/'));
// Application Path Block End


defined('DB_MASTER_SERVER') || define('DB_MASTER_SERVER', '127.0.0.1');
defined('DB_MASTER_USER') || define('DB_MASTER_USER', 'root');
defined('DB_MASTER_PSWD') || define('DB_MASTER_PSWD', 'root');
//if (!isset($_COOKIE['dubug_cookie'])) {
defined('DB_MASTER_DB') || define('DB_MASTER_DB', 'llps_llps');

defined('DB_MASTER_PREFIX') || define('DB_MASTER_PREFIX', '');

defined('DB_SLAVE_SERVER') || define('DB_SLAVE_SERVER', '127.0.0.1');
defined('DB_SLAVE_USER') || define('DB_SLAVE_USER', 'root');
//defined('DB_SLAVE_PSWD') || define('DB_SLAVE_PSWD', 'go#qiiiiq#og');
defined('DB_SLAVE_PSWD') || define('DB_SLAVE_PSWD', 'root');
//if (!isset($_COOKIE['dubug_cookie'])) {
defined('DB_SLAVE_DB') || define('DB_SLAVE_DB', 'llps_llps');
defined('DB_SLAVE_PREFIX') || define('DB_SLAVE_PREFIX', '');


defined('ENCRYPTION_KEY') || define('ENCRYPTION_KEY', 'CH@#AT123');
defined('ENCRYPTION_KEY_PRIVATE') || define('ENCRYPTION_KEY_PRIVATE', 'CH@#AT123INDIACOM');

//defined('URL_PERMISSION') || define('URL_PERMISSION', '2');


defined('BO_DEBUG') || define('BO_DEBUG', false);
//defined('BO_ERROR_CONSOLE') || define('BO_ERROR_CONSOLE', false);
//defined('BO_LOG') || define('BO_LOG', true);


defined('URL_SHORTENER') || define('URL_SHORTENER', 1);





