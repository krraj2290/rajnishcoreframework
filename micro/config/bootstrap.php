<?php

/* get api json data
 * 
 */
$json = file_get_contents('php://input');
$api_data = json_decode($json, true);
ini_set("display_errors",1);
if (isset($_COOKIE['dubug_cookie'])) {
    ini_set('display_errors', 1);
    define('PRINT_MASTER_QUERY', true);
    define('PRINT_SLAVE_QUERY', true);
    define('SHOW_QUERY_TIME', true);
}


$pageStartLoadingTime = microtime(true);



require_once( LIB_PATH . "Rajnish/Loader.php" );
spl_autoload_register(null, false);
spl_autoload_extensions('.php');

spl_autoload_register(Array('Rajnish_Loader', 'Lib'));
require_once (LURACAST_PATH . "restler.php");
use Luracast\Restler\Restler;
use Luracast\Restler\Defaults;
//require_once COMPONENT_PATH . "Api/AccessInternalControl.php";


try {

    $Urls = DB_Urls::getInstance();
 
    $Urls::processAPIUrl();
   
    $requestUri = explode("/", $_SERVER['REQUEST_URI']);
    $_SERVER['REQUEST_URI'] = "/$requestUri[1]/$requestUri[2]";
    
    $responseFormats = isset($_REQUEST['responseFormats']) ? $_REQUEST['responseFormats'] : 'jsonFormat';
    if (!empty($requestUri[1]) && file_exists(APP_PHP . $requestUri[1] . ".php")) {
        
        require_once (APP_PHP . $requestUri[1] . ".php");
        $r = new Restler();
        
//        $r->setSupportedFormats($responseFormats);

        $r->addAuthenticationClass("Api_AccessInternalControl");
       
        $r->addAPIClass("Api_".$requestUri[1]);
         
        $r->handle();
        
    } else {
        $array = array(
            'responseCode' => 404,
            'error' => 'Not Found'
        );
        header('Content-Type: application/json');
        echo json_encode($array);
    }
} catch (Exception $ex) {
   Rajnish_Helper::describe($ex->getMessage());
    Rajnish_Helper::describe($ex->getTraceAsString());
}
?>
