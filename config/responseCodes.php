<?php
defined('NOT_FOUND') || define('NOT_FOUND', 404);
defined('UNAUTHORIED') || define('UNAUTHORIED', 401);
defined('BAD_REQUEST') || define('BAD_REQUEST', 400);
defined('INTERNAL_SERVER_ERROR') || define('INTERNAL_SERVER_ERROR', 500);
defined('MISSING_PARAMETER') || define('MISSING_PARAMETER', 422);
defined('FORBIDDEN') || define('FORBIDDEN', 403);
defined('NOT_MODIFIED') || define('NOT_MODIFIED', 304);
defined('UNAUTHORIZED') || define('UNAUTHORIZED', 401);
defined('NOT_ACCEPTABLE') || define('NOT_ACCEPTABLE', 406);
defined('OK') || define('OK', 200);

?>
