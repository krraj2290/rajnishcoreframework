<?php

ini_set("display_errors", 1);
$action = isset(DB_Urls::$pageVars['GETPOST']['action']) ? trim(DB_Urls::$pageVars['GETPOST']['action']) : '';
try {
    switch ($action) {
        case 'signUpNewUserForRegister':
            try {
                ini_set("display_errors", 1);
                $userFirstName = isset(DB_Urls::$pageVars['GETPOST']['signUpUserFirstName']) ? trim(DB_Urls::$pageVars['GETPOST']['signUpUserFirstName']) : '';
                $userLastName = isset(DB_Urls::$pageVars['GETPOST']['signUpUserLastName']) ? trim(DB_Urls::$pageVars['GETPOST']['signUpUserLastName']) : '';
                $userPassword = isset(DB_Urls::$pageVars['GETPOST']['signUpUserPassword']) ? trim(DB_Urls::$pageVars['GETPOST']['signUpUserPassword']) : '';
                $userEmail = isset(DB_Urls::$pageVars['GETPOST']['signUpUserEmail']) ? trim(DB_Urls::$pageVars['GETPOST']['signUpUserEmail']) : '';
                $userMobile = isset(DB_Urls::$pageVars['GETPOST']['signUpUserMobile']) ? trim(DB_Urls::$pageVars['GETPOST']['signUpUserMobile']) : '';

                $userObj = new DB_Users("email||$userEmail", "quantity||1");
                if ($userObj->getResultCount() == 0) {
                    $userPassword = md5($userPassword);
                    $userObj = new DB_DO_User("add");
                    $userObj->set("firstName||$userFirstName", "lastName||$userLastName", "email||$userEmail", "password||$userPassword", "mobile||$userMobile");
                    $msg = "User register successfully. please login..";
                } else {
                    $msg = "User already register";
                }
                echo $msg;
                header('HTTP/1.1 200 Success');
            } catch (Exception $ex) {
                header('HTTP/1.1 500 Internal Server Error');
                echo "some internal problem";
                print_r($ex);
            }
            break;
        case 'loginUserProfile':
            try {
                ini_set("display_errors", 1);
                $loginUserEmail = isset(DB_Urls::$pageVars['GETPOST']['loginUserEmail']) ? trim(DB_Urls::$pageVars['GETPOST']['loginUserEmail']) : '';
                $loginUserPassword = isset(DB_Urls::$pageVars['GETPOST']['loginUserPassword']) ? trim(DB_Urls::$pageVars['GETPOST']['loginUserPassword']) : '';
                $loginUserPassword = md5($loginUserPassword);
                $userObj = new DB_Users("email||$loginUserEmail", "password||$loginUserPassword", "quantity||1");
                if ($userObj->getResultCount() > 0) {
                    $userId = $userObj->get(0)->getUserId();
                    DB_XmppUtility::createXmppUser($userId, 'user');
                    $userId = DB_Utility::encrypt($userId);
                    setcookie("_chatUserId", $userId, time() + (86400), '/');
                    
                    header('HTTP/1.1 200 Success');
                    $msg = " please login..";
                } else {
                    $msg = "Invalid User";
                }
                echo $msg;
                header('HTTP/1.1 200 Success');
            } catch (Exception $ex) {
                header('HTTP/1.1 500 Internal Server Error');
                echo "some internal problem";
                print_r($ex);
            }
            break;

        default:
            header('HTTP/1.1 400 Not Found');
            echo '({"error":"Not found","responseCode":404});';
            break;
    }
} catch (Exception $ex) {
    
}

