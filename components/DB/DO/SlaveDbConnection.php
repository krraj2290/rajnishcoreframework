<?php

/**
 * @author Rajnish Kr Rajesh
 * @desc It is basically a Slave database connection class which inherits SlaveDbConnetion class of 
 * foundation and take project specific credentials for database connection
 */
class DB_DO_SlaveDbConnection extends Rajnish_SlaveDbConnection {

    protected $server = DB_SLAVE_SERVER;
    protected $user = DB_SLAVE_USER;
    protected $pass = DB_SLAVE_PSWD;
    protected $database = DB_SLAVE_DB;
    protected $pre = "";
    protected $autoincrement_pre = DB_SLAVE_PREFIX;
    protected static $hinst = null;

    protected function __construct() {
        $this->Connect(true);
    }

    public static function GetInstance() {
        if (!self::$hinst) {
            self::$hinst = new self();
        }
        return self::$hinst;
    }

}

?>
