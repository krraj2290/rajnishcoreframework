<?php

/**
 *
 * @author: Rajnish.
 * @desc: Database Class for table BetaOutApp_client.
 * @param: clientId Integer, clientName Varchar, clientEmailId Varchar, clientUrl Varchar, clientDetails Text,
 *         clientCompany Varchar, clientSubDomain Varchar, clientAccountType Varchar, clientLastPaymentDate Datetime,
 *         clientLastPaymentType Varchar, clientLastPaymentDiscount Decimal.
 * //alter clientDomain to loginName
 */
class DB_DO_User extends DB_DO_Base {

    protected $_mainTable = 'sms_user';
    protected $_clauseColumnNames = array(
        'userId'
        , 'firstName'
        , 'lastName'
        , 'email'
        ,'imageUrl'
        , 'password'
        , 'mobile'
        , 'createdTime'
    );
    protected $_sortColumnNames = array('createdTime');
    protected $_foreignKey = 'userId';
    protected $_expandableTables = array();
    protected $_updateColumnNames = array(
        'firstName'
        , 'lastName'
        , 'email'
        ,'imageUrl'
        , 'password'
        , 'mobile'
        , 'createdTime'
    );
    protected $_requiredAddColumns = array();
    protected $_runtimeAddColumns = array('createdTime' => 'now()');

    public function specialQuery($query, $escape = true) {
        $queryResult = $this->_databaseConnection->RunMyQuery($query, $escape);
        return $queryResult;
    }

}

?>
