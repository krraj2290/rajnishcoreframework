<?php

/**
 * @author Rajnish Kr Rajesh
 * @desc It is basically a Master database connection class which inherits MasterDbConnetion class of 
 * foundation and take project specific credentials for database connection
 */
class DB_DO_MasterDbConnection extends Rajnish_MasterDbConnection {

    protected $server = DB_MASTER_SERVER;
    protected $user = DB_MASTER_USER;
    protected $pass = DB_MASTER_PSWD;
    protected $database = DB_MASTER_DB;
    protected $pre = "";
    protected $autoincrement_pre = DB_MASTER_PREFIX;
    protected static $hinst = null;

    protected function __construct() {
        $this->Connect(true);
    }

    public static function GetInstance() {
        if (!self::$hinst) {
            self::$hinst = new self();
        }
        return self::$hinst;
    }

}

?>
