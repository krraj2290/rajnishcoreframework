<?php
/**
 * @author Jitendra Singh Bhadouria
 * @desc It is basically a base class which inherits Base class of foundation and made project specific connetion to database
 */
class DB_DO_Base extends Rajnish_Base {

    protected $_databaseConnection = null;
    protected $_task = 'show';

    public function __construct($task = 'show') {
        $this->_task = strtolower($task);
        $this->_databaseConnection = DB_Connection::GetInstance();
    }

}

?>
