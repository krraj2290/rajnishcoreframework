<?php


class DB_ModelCollectionBase extends Rajnish_ModelCollectionBase {

    protected $_dbClassPrefix = '';

    public function __construct($firstArgument = false) {
        if (func_num_args() > 0) {
            $this->_dbClassPrefix = explode('/', dirname(__FILE__));
            $this->_dbClassPrefix = $this->_dbClassPrefix[count($this->_dbClassPrefix) - 1] . '_DO_';
            if (is_array($firstArgument))
                parent::__construct($firstArgument);
            else {
                $argumentString = implode('", "', func_get_args());
                eval('parent::__construct("' . $argumentString . '" );');
            }
        }
    }

}

?>
