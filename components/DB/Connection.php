<?php

/**
 * @author Rajnish Kr Rajesh
 * @desc It is a project specific connection class to create prject specific objects of Master and Slave databse connection
 * which in turn take project specific credentials for making connection to database
 */
class DB_Connection extends Rajnish_DbConnection {

    protected static $hinst = null;
    protected static $hinstm = null;
    protected static $hinsts = null;

    private function __construct() {
        $this->hinstm = DB_DO_MasterDbConnection::GetInstance();
        $this->hinsts = DB_DO_SlaveDbConnection::GetInstance();
    }

    public static function GetInstance() {
        if (!self::$hinst) {
            self::$hinst = new self();
        }

        return self::$hinst;
    }

}

?>