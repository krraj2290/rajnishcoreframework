<?php
/**
 * @author Rajnish Kr Rajesh
 * @desc It inherits ModelBase class of foundation. It finds out the project specific Db class prefix
 * It is basically a sigleton class and return the object of data corresponding to the single primary key
 */
class DB_ModelBase extends Rajnish_ModelBase {

    protected $_dbClassPrefix = '';

    public function __construct($primaryKeyValue = 0) {
        $this->_dbClassPrefix = explode('/', dirname(__FILE__));
        $this->_dbClassPrefix = $this->_dbClassPrefix[count($this->_dbClassPrefix) - 1] . '_DO_';
        parent::__construct($primaryKeyValue);
    }

}

?>
