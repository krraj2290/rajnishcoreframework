<?php

class DB_Url {

    private static $_component = 'blog';
    private static $_object = 'blog';
    private static $_urlFileUri = 'error_404.php';
    private static $_splittedDomainURI = array();
    private static $_subDomainValue = '';
    private static $_hostName;
    public static $self = '/';
    private static $_pageNumber = 1;
    private static $_pageSlug = '';
    public static $pageVars = array();

    public static function processUrl() {
        self::$_urlFileUri = SITE_PHP . 'error_404.php';
        $hostName = str_replace('www.', '', $_SERVER['HTTP_HOST']);
        $requestUri = explode('?', $_SERVER['REQUEST_URI']);
//         $requestUri = str_replace('billionclub/', '', $requestUri[0]);
        $requestUri = trim($requestUri[0], '/');

        $splittedHostName = explode('.', $hostName);
        $hostLength = count($splittedHostName);
        if ($hostLength > 2) {
            self::$_hostName = $splittedHostName[$hostLength - 2] . '.' . $splittedHostName[$hostLength - 1];
            self::$_subDomainValue = str_replace('.' . self::$_hostName, '', $hostName);
        } else {
            self::$_hostName = $hostName;
        }

        $varsUri = explode('/nv/', $requestUri);
        if (count($varsUri) > 1) {
            self::makePageVars($varsUri[1]);
            $requestUri = $varsUri[0];
        } else {
            self::makePageVars('');
        }

        self::$self = $requestUri;
        self::$_splittedDomainURI = $splittedRequestUri = explode('/', $requestUri);
        while (count($splittedRequestUri) > 0 && is_numeric($splittedRequestUri[count($splittedRequestUri) - 1])) {
            self::$_pageNumber = array_pop($splittedRequestUri);
            $requestUri = implode('/', $splittedRequestUri);
        }

        if ($requestUri == '') {
            self::$_urlFileUri = SITE_PHP . 'home.php';
        } else {

            if (substr($requestUri, -5) == '.html') {

                $slug = $splittedRequestUri[0];
                if (stripos($slug, '_p') !== false) {
                    $pageNumber = explode('_p', $slug);
                    $pageNumber = explode('.', $pageNumber[1]);
                    self::$_pageNumber = $pageNumber[0];
                }

                $replaceThis = self::$_pageNumber > 1 ? '_p' . self::$_pageNumber . '.html' : '.html';

                $slug = str_replace($replaceThis, '', $slug);

                $slugsObj = new Cms_Slugs("slug||$slug");
                if ($slugsObj->getResultCount() > 0) {
                    $object = $slugsObj->getObject(0);
                    if (( $object == 'story' || $object == 'topic' || $object == 'asset' || $object == 'entity' || $object == 'element')) {
                        self::$_pageSlug = $slug;
                        self::getFileInfo($slugsObj->get(0));
                    } else {
                        $fileName = self::slugToName($slug . '.html');
                        if (file_exists(SITE_PHP . $fileName . '.php')) {
                            self::$_urlFileUri = SITE_PHP . $fileName . '.php';
                        } else {
                            $slug = explode('/', $slug);
                            if (count($slug > 1)) {
                                if (file_exists(SITE_PHP . $slug[0] . '.php')) {
                                    self::$_pageSlug = $slug[1];
                                    self::$_urlFileUri = SITE_PHP . $slug[0] . '.php';
                                }
                            }
                        }
                    }
                } else {
                    $fileName = self::slugToName($slug . '.html');
                    if (file_exists(SITE_PHP . $fileName . '.php')) {
                        self::$_urlFileUri = SITE_PHP . $fileName . '.php';
                    } else {
                        $slug = explode('/', $slug);
                        if (count($slug > 1)) {
                            if (file_exists(SITE_PHP . $slug[0] . '.php')) {
                                self::$_pageSlug = $slug[1];
                                self::$_urlFileUri = SITE_PHP . $slug[0] . '.php';
                            }
                        }
                    }
                }
            } else {

                $slug = $splittedRequestUri[0];
                //$slugsObj = new Cms_Slugs( "slug||$slug" );
//
//					if( $slugsObj->getResultCount() > 0 ) {
//						$object = $slugsObj->getObject( 0 );
//						if( ( $object != 'story' && $object != 'topic' && $object != 'asset' ) ) {
//							self::$_pageSlug = $slug;
//							self::getFileInfo( $slugsObj->get( 0 ) );
//						}
//					}
//					else {
//						$slug = $requestUri == 'page' ? 'home' : $splittedRequestUri[ 0 ];
//						if( $slug == 'entry' ) {
//							if( isset( $splittedRequestUri[ 1 ] ) ) {
//								$slug = $splittedRequestUri[ 1 ];
//								$slugsObj = new Cms_Slugs( "slug||$slug", 'object||story' );
//								if( $slugsObj->getResultCount() > 0 ) {
//									self::$_pageSlug = $splittedRequestUri[ 1 ];
//									self::$_object = 'story';
//									self::$_urlFileUri = SITE_PHP . 'story.php';
//								}
//							}
//						} else {
                $slug = $requestUri == 'page' ? 'home' : $requestUri;
                $fileName = self::slugToName($slug);
                if (file_exists(SITE_PHP . $fileName . '.php')) {
                    self::$_urlFileUri = SITE_PHP . $fileName . '.php';
                } else {
                    $slug = explode('/', $slug);
                    if (count($slug > 1)) {
                        if (file_exists(SITE_PHP . $slug[0] . '.php')) {
                            self::$_pageSlug = $slug[1];
                            self::$_urlFileUri = SITE_PHP . $slug[0] . '.php';
                        }
                    }
                }
                //}
                //}
            }
        }
        self::$_pageNumber = self::$_pageNumber > 0 ? self::$_pageNumber : 1;
    }

    private static function slugToName($slug) {
        return str_replace('-', '_', $slug);
    }

    public static function __callStatic($functionName, $argumentsList) {
        $varName = '_' . lcfirst(str_replace('get', '', $functionName));
        if (isset(self::$$varName)) {
            return self::$$varName;
        } else {
            throw new Exception("Call to undefined function $functionName!");
        }
    }

    private static function getFileInfo($slugObj) {
        self::$_component = $slugObj->getComponent();
        self::$_object = $slugObj->getObject();
        self::$_urlFileUri = SITE_PHP . self::$_object . '.php';
    }

    private function makePageVars($varPart) {
        self::$pageVars['GETPOST'] = array();
        self::$pageVars['GET'] = array();
        self::$pageVars['POST'] = array();
        self::$pageVars['QUERY'] = array();
        self::$pageVars['COOKIE'] = $_COOKIE;
        self::$pageVars['FILES'] = $_FILES;
        //self::$pageVars[ 'SERVER' ] = $_SERVER;
        if (trim($varPart) != '') {
            $allVars = explode('/', $varPart);

            if (( count($allVars) % 2 ) !== 0) {
                self::$pageVars['GETPOST']['ORPHAN'] = trim(array_pop($allVars));
            }

            for ($varCount = 0; $varCount < count($allVars); $varCount += 2) {
                self::$pageVars['GET'][trim($allVars[$varCount])] = stripslashes(stripslashes(urldecode(trim($allVars[$varCount + 1]))));
                self::$pageVars['QUERY'][trim($allVars[$varCount])] = stripslashes(stripslashes(urldecode(trim($allVars[$varCount + 1]))));
                self::$pageVars['GETPOST'][trim($allVars[$varCount])] = stripslashes(stripslashes(urldecode(trim($allVars[$varCount + 1]))));
            }
        }

        foreach ($_GET as $key => $value) {
            self::$pageVars['GETPOST'][$key] = ( is_array($value) ) ? $value : stripslashes(stripslashes(trim($value)));
            self::$pageVars['GET'][$key] = ( is_array($value) ) ? $value : stripslashes(stripslashes(trim($value)));
        }

        foreach ($_POST as $key => $value) {
            self::$pageVars['GETPOST'][$key] = ( is_array($value) ) ? $value : stripslashes(stripslashes(trim($value)));
            self::$pageVars['POST'][$key] = ( is_array($value) ) ? $value : stripslashes(stripslashes(trim($value)));
        }
        Goquii_Url::$pageVars = self::$pageVars;
    }

}
?>