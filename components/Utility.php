<?php

class Utility {

    // csv file delimiter 
    protected $message = '';
    protected $startTime = '';
    protected $endTime = '';

    public function __construct($debug = false) {

        if ($debug) {
            $this->startTime = self::getmicrotime();
        }
    }

    public function encodeURIComponent($str) {
        $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');
        return strtr(rawurlencode($str), $revert);
    }

    public function calculateTime($check = true) {
        $type = '';
        $loadTime = 0;
        $this->endTime = round(( self::getmicrotime() - $this->startTime), 2);
        if ($check) {
            if ($this->endTime > 60) {
                $type = 'minutes';
                $loadTime = $this->endTime / 60;
            } else {
                $type = 'seconds';
                $loadTime = $this->endTime;
            }
        } else {
            $loadTime = $this->endTime > 60 ? ( $this->endTime / 60 ) : $this->endTime;
        }
        return array('time' => round($loadTime, 2), 'type' => $type);
    }

    public function getTime() {
        return self::getmicrotime() - $this->startTime;
    }

    public function getmicrotime() {
        return microtime(true);
    }

    
    /**
     * @author Dharmendra Rai <developer.dharam@gmail.com> 
     * @desc  method utf8ize  will encode all array and string in utf-8 
     * */
    public static function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = self::utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public static function date() {
        return array('date' => time);
    }

    public static function onSiteArgumentValidator($variables) {
        if (!is_array($variables))
            return false;

        if (count($variables) == 0)
            return false;

        foreach ($variables as $key => $value) {
            if (empty($value)) {
                return false;
            }
        }
        return true;
    }

    
    public static function validateIntlPhone($str) {
// test if input is of the form +cc aa nnnn nnnn
        return preg_match("/^(\+|00)[1-9]{1,3}(\.|\s|-)?([0-9]{1,5}(\.|\s|-)?){1,3}$/", $str);
    }

    public static function arrayToCsv(array $fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $outputString = "";
        foreach ($fields as $tempFields) {
            $output = array();
            foreach ($tempFields as $field) {
                // ADDITIONS BEGIN HERE
                if (gettype($field) == 'integer' || gettype($field) == 'double') {
                    $field = strval($field); // Change $field to string if it's a numeric type
                }
                // ADDITIONS END HERE
                if ($field === null && $nullToMysqlNull) {
                    $output[] = 'NULL';
                    continue;
                }

                // Enclose fields containing $delimiter, $enclosure or whitespace
                if ($encloseAll || preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)) {
                    $field = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
                }
                $output[] = $field;
            }
            $outputString .= implode($delimiter, $output) . "\r\n";
        }
        return $outputString;
    }

    public static function changeUri() {
        $arrNotLogin = self::getLocaleList();
        $_SERVER["REQUEST_URIold"] = $_SERVER["REQUEST_URI"];
//        $arrNotLogin = array('account', 'blog', 'about-us', 'contact-us', 'sign-up', 'login', 'home', 'investors', 'onboarding', 'team-member-register', 'forgot-password');
        $requestUrl = explode("/", $_SERVER["REQUEST_URI"]);
        $requestUrl1 = $requestUrl[1];
//        $requestUrl0 = $requestUrl[1];
        $requestUrl1 = empty($requestUrl1) ? 'home' : $requestUrl1;
        if (in_array($requestUrl1, $arrNotLogin)) {
//            print_r(array_keys($arrNotLogin));
            unset($requestUrl[1]);
            $_SERVER["REQUEST_URI"] = implode('/', $requestUrl);
        }
    }

    public static function getLocaleList() {
        return array("aa_DJ", "aa_ER", "aa_ET", "af_ZA", "sq_AL", "sq_MK", "am_ET", "ar_DZ", "ar_BH", "ar_EG", "ar_IN", "ar_IQ", "ar_JO", "ar_KW", "ar_LB", "ar_LY", "ar_MA", "ar_OM", "ar_QA", "ar_SA", "ar_SD", "ar_SY", "ar_TN", "ar_AE", "ar_YE", "an_ES", "hy_AM", "as_IN", "ast_ES", "az_AZ", "az_TR", "eu_FR", "eu_ES", "be_BY", "bem_ZM", "bn_BD", "bn_IN", "ber_DZ", "ber_MA", "byn_ER", "bs_BA", "br_FR", "bg_BG", "my_MM", "ca_AD", "ca_FR", "ca_IT", "ca_ES", "zh_CN", "zh_HK", "zh_SG", "zh_TW", "cv_RU", "kw_GB", "crh_UA", "hr_HR", "cs_CZ", "da_DK", "dv_MV", "nl_AW", "nl_BE", "nl_NL", "dz_BT", "en_AG", "en_AU", "en_BW", "en_CA", "en_DK", "en_HK", "en_IN", "en_IE", "en_NZ", "en_NG", "en_PH", "en_SG", "en_ZA", "en_GB", "en_US", "en_ZM", "en_ZW", "eo", "et_EE", "fo_FO", "fil_PH", "fi_FI", "fr_BE", "fr_CA", "fr_FR", "fr_LU", "fr_CH", "fur_IT", "ff_SN", "gl_ES", "lg_UG", "gez_ER", "gez_ET", "ka_GE", "de_AT", "de_BE", "de_DE", "de_LI", "de_LU", "de_CH", "el_CY", "el_GR", "gu_IN", "ht_HT", "ha_NG", "iw_IL", "he_IL", "hi_IN", "hu_HU", "is_IS", "ig_NG", "id_ID", "ia", "iu_CA", "ik_CA", "ga_IE", "it_IT", "it_CH", "ja_JP", "kl_GL", "kn_IN", "ks_IN", "csb_PL", "kk_KZ", "km_KH", "rw_RW", "ky_KG", "kok_IN", "ko_KR", "ku_TR", "lo_LA", "lv_LV", "li_BE", "li_NL", "lt_LT", "nds_DE", "nds_NL", "mk_MK", "mai_IN", "mg_MG", "ms_MY", "ml_IN", "mt_MT", "gv_GB", "mi_NZ", "mr_IN", "mn_MN", "ne_NP", "se_NO", "nso_ZA", "nb_NO", "nn_NO", "oc_FR", "or_IN", "om_ET", "om_KE", "os_RU", "pap_AN", "ps_AF", "fa_IR", "pl_PL", "pt_BR", "pt_PT", "pa_IN", "pa_PK", "ro_RO", "ru_RU", "ru_UA", "sa_IN", "sc_IT", "gd_GB", "sr_ME", "sr_RS", "sid_ET", "sd_IN", "si_LK", "sk_SK", "sl_SI", "so_DJ", "so_ET", "so_KE", "so_SO", "nr_ZA", "st_ZA", "es_AR", "es_BO", "es_CL", "es_CO", "es_CR", "es_DO", "es_EC", "es_SV", "es_GT", "es_HN", "es_MX", "es_NI", "es_PA", "es_PY", "es_PE", "es_ES", "es_US", "es_UY", "es_VE", "sw_KE", "sw_TZ", "ss_ZA", "sv_FI", "sv_SE", "tl_PH", "tg_TJ", "ta_IN", "tt_RU", "te_IN", "th_TH", "bo_CN", "bo_IN", "tig_ER", "ti_ER", "ti_ET", "ts_ZA", "tn_ZA", "tr_CY", "tr_TR", "tk_TM", "ug_CN", "uk_UA", "hsb_DE", "ur_PK", "uz_UZ", "ve_ZA", "vi_VN", "wa_BE", "cy_GB", "fy_DE", "fy_NL", "wo_SN", "xh_ZA", "yi_US", "yo_NG", "zu_ZA");
    }

    public static function getLocaleList1() {
        return array(
            'aa_DJ' => 'Afar (Djibouti)',
            'aa_ER' => 'Afar (Eritrea)',
            'aa_ET' => 'Afar (Ethiopia)',
            'af_ZA' => 'Afrikaans (South Africa)',
            'sq_AL' => 'Albanian (Albania)',
            'sq_MK' => 'Albanian (Macedonia)',
            'am_ET' => 'Amharic (Ethiopia)',
            'ar_DZ' => 'Arabic (Algeria)',
            'ar_BH' => 'Arabic (Bahrain)',
            'ar_EG' => 'Arabic (Egypt)',
            'ar_IN' => 'Arabic (India)',
            'ar_IQ' => 'Arabic (Iraq)',
            'ar_JO' => 'Arabic (Jordan)',
            'ar_KW' => 'Arabic (Kuwait)',
            'ar_LB' => 'Arabic (Lebanon)',
            'ar_LY' => 'Arabic (Libya)',
            'ar_MA' => 'Arabic (Morocco)',
            'ar_OM' => 'Arabic (Oman)',
            'ar_QA' => 'Arabic (Qatar)',
            'ar_SA' => 'Arabic (Saudi Arabia)',
            'ar_SD' => 'Arabic (Sudan)',
            'ar_SY' => 'Arabic (Syria)',
            'ar_TN' => 'Arabic (Tunisia)',
            'ar_AE' => 'Arabic (United Arab Emirates)',
            'ar_YE' => 'Arabic (Yemen)',
            'an_ES' => 'Aragonese (Spain)',
            'hy_AM' => 'Armenian (Armenia)',
            'as_IN' => 'Assamese (India)',
            'ast_ES' => 'Asturian (Spain)',
            'az_AZ' => 'Azerbaijani (Azerbaijan)',
            'az_TR' => 'Azerbaijani (Turkey)',
            'eu_FR' => 'Basque (France)',
            'eu_ES' => 'Basque (Spain)',
            'be_BY' => 'Belarusian (Belarus)',
            'bem_ZM' => 'Bemba (Zambia)',
            'bn_BD' => 'Bengali (Bangladesh)',
            'bn_IN' => 'Bengali (India)',
            'ber_DZ' => 'Berber (Algeria)',
            'ber_MA' => 'Berber (Morocco)',
            'byn_ER' => 'Blin (Eritrea)',
            'bs_BA' => 'Bosnian (Bosnia and Herzegovina)',
            'br_FR' => 'Breton (France)',
            'bg_BG' => 'Bulgarian (Bulgaria)',
            'my_MM' => 'Burmese (Myanmar [Burma])',
            'ca_AD' => 'Catalan (Andorra)',
            'ca_FR' => 'Catalan (France)',
            'ca_IT' => 'Catalan (Italy)',
            'ca_ES' => 'Catalan (Spain)',
            'zh_CN' => 'Chinese (China)',
            'zh_HK' => 'Chinese (Hong Kong SAR China)',
            'zh_SG' => 'Chinese (Singapore)',
            'zh_TW' => 'Chinese (Taiwan)',
            'cv_RU' => 'Chuvash (Russia)',
            'kw_GB' => 'Cornish (United Kingdom)',
            'crh_UA' => 'Crimean Turkish (Ukraine)',
            'hr_HR' => 'Croatian (Croatia)',
            'cs_CZ' => 'Czech (Czech Republic)',
            'da_DK' => 'Danish (Denmark)',
            'dv_MV' => 'Divehi (Maldives)',
            'nl_AW' => 'Dutch (Aruba)',
            'nl_BE' => 'Dutch (Belgium)',
            'nl_NL' => 'Dutch (Netherlands)',
            'dz_BT' => 'Dzongkha (Bhutan)',
            'en_AG' => 'English (Antigua and Barbuda)',
            'en_AU' => 'English (Australia)',
            'en_BW' => 'English (Botswana)',
            'en_CA' => 'English (Canada)',
            'en_DK' => 'English (Denmark)',
            'en_HK' => 'English (Hong Kong SAR China)',
            'en_IN' => 'English (India)',
            'en_IE' => 'English (Ireland)',
            'en_NZ' => 'English (New Zealand)',
            'en_NG' => 'English (Nigeria)',
            'en_PH' => 'English (Philippines)',
            'en_SG' => 'English (Singapore)',
            'en_ZA' => 'English (South Africa)',
            'en_GB' => 'English (United Kingdom)',
            'en_US' => 'English (United States)',
            'en_ZM' => 'English (Zambia)',
            'en_ZW' => 'English (Zimbabwe)',
            'eo' => 'Esperanto',
            'et_EE' => 'Estonian (Estonia)',
            'fo_FO' => 'Faroese (Faroe Islands)',
            'fil_PH' => 'Filipino (Philippines)',
            'fi_FI' => 'Finnish (Finland)',
            'fr_BE' => 'French (Belgium)',
            'fr_CA' => 'French (Canada)',
            'fr_FR' => 'French (France)',
            'fr_LU' => 'French (Luxembourg)',
            'fr_CH' => 'French (Switzerland)',
            'fur_IT' => 'Friulian (Italy)',
            'ff_SN' => 'Fulah (Senegal)',
            'gl_ES' => 'Galician (Spain)',
            'lg_UG' => 'Ganda (Uganda)',
            'gez_ER' => 'Geez (Eritrea)',
            'gez_ET' => 'Geez (Ethiopia)',
            'ka_GE' => 'Georgian (Georgia)',
            'de_AT' => 'German (Austria)',
            'de_BE' => 'German (Belgium)',
            'de_DE' => 'German (Germany)',
            'de_LI' => 'German (Liechtenstein)',
            'de_LU' => 'German (Luxembourg)',
            'de_CH' => 'German (Switzerland)',
            'el_CY' => 'Greek (Cyprus)',
            'el_GR' => 'Greek (Greece)',
            'gu_IN' => 'Gujarati (India)',
            'ht_HT' => 'Haitian (Haiti)',
            'ha_NG' => 'Hausa (Nigeria)',
            'iw_IL' => 'Hebrew (Israel)',
            'he_IL' => 'Hebrew (Israel)',
            'hi_IN' => 'Hindi (India)',
            'hu_HU' => 'Hungarian (Hungary)',
            'is_IS' => 'Icelandic (Iceland)',
            'ig_NG' => 'Igbo (Nigeria)',
            'id_ID' => 'Indonesian (Indonesia)',
            'ia' => 'Interlingua',
            'iu_CA' => 'Inuktitut (Canada)',
            'ik_CA' => 'Inupiaq (Canada)',
            'ga_IE' => 'Irish (Ireland)',
            'it_IT' => 'Italian (Italy)',
            'it_CH' => 'Italian (Switzerland)',
            'ja_JP' => 'Japanese (Japan)',
            'kl_GL' => 'Kalaallisut (Greenland)',
            'kn_IN' => 'Kannada (India)',
            'ks_IN' => 'Kashmiri (India)',
            'csb_PL' => 'Kashubian (Poland)',
            'kk_KZ' => 'Kazakh (Kazakhstan)',
            'km_KH' => 'Khmer (Cambodia)',
            'rw_RW' => 'Kinyarwanda (Rwanda)',
            'ky_KG' => 'Kirghiz (Kyrgyzstan)',
            'kok_IN' => 'Konkani (India)',
            'ko_KR' => 'Korean (South Korea)',
            'ku_TR' => 'Kurdish (Turkey)',
            'lo_LA' => 'Lao (Laos)',
            'lv_LV' => 'Latvian (Latvia)',
            'li_BE' => 'Limburgish (Belgium)',
            'li_NL' => 'Limburgish (Netherlands)',
            'lt_LT' => 'Lithuanian (Lithuania)',
            'nds_DE' => 'Low German (Germany)',
            'nds_NL' => 'Low German (Netherlands)',
            'mk_MK' => 'Macedonian (Macedonia)',
            'mai_IN' => 'Maithili (India)',
            'mg_MG' => 'Malagasy (Madagascar)',
            'ms_MY' => 'Malay (Malaysia)',
            'ml_IN' => 'Malayalam (India)',
            'mt_MT' => 'Maltese (Malta)',
            'gv_GB' => 'Manx (United Kingdom)',
            'mi_NZ' => 'Maori (New Zealand)',
            'mr_IN' => 'Marathi (India)',
            'mn_MN' => 'Mongolian (Mongolia)',
            'ne_NP' => 'Nepali (Nepal)',
            'se_NO' => 'Northern Sami (Norway)',
            'nso_ZA' => 'Northern Sotho (South Africa)',
            'nb_NO' => 'Norwegian Bokm�l (Norway)',
            'nn_NO' => 'Norwegian Nynorsk (Norway)',
            'oc_FR' => 'Occitan (France)',
            'or_IN' => 'Oriya (India)',
            'om_ET' => 'Oromo (Ethiopia)',
            'om_KE' => 'Oromo (Kenya)',
            'os_RU' => 'Ossetic (Russia)',
            'pap_AN' => 'Papiamento (Netherlands Antilles)',
            'ps_AF' => 'Pashto (Afghanistan)',
            'fa_IR' => 'Persian (Iran)',
            'pl_PL' => 'Polish (Poland)',
            'pt_BR' => 'Portuguese (Brazil)',
            'pt_PT' => 'Portuguese (Portugal)',
            'pa_IN' => 'Punjabi (India)',
            'pa_PK' => 'Punjabi (Pakistan)',
            'ro_RO' => 'Romanian (Romania)',
            'ru_RU' => 'Russian (Russia)',
            'ru_UA' => 'Russian (Ukraine)',
            'sa_IN' => 'Sanskrit (India)',
            'sc_IT' => 'Sardinian (Italy)',
            'gd_GB' => 'Scottish Gaelic (United Kingdom)',
            'sr_ME' => 'Serbian (Montenegro)',
            'sr_RS' => 'Serbian (Serbia)',
            'sid_ET' => 'Sidamo (Ethiopia)',
            'sd_IN' => 'Sindhi (India)',
            'si_LK' => 'Sinhala (Sri Lanka)',
            'sk_SK' => 'Slovak (Slovakia)',
            'sl_SI' => 'Slovenian (Slovenia)',
            'so_DJ' => 'Somali (Djibouti)',
            'so_ET' => 'Somali (Ethiopia)',
            'so_KE' => 'Somali (Kenya)',
            'so_SO' => 'Somali (Somalia)',
            'nr_ZA' => 'South Ndebele (South Africa)',
            'st_ZA' => 'Southern Sotho (South Africa)',
            'es_AR' => 'Spanish (Argentina)',
            'es_BO' => 'Spanish (Bolivia)',
            'es_CL' => 'Spanish (Chile)',
            'es_CO' => 'Spanish (Colombia)',
            'es_CR' => 'Spanish (Costa Rica)',
            'es_DO' => 'Spanish (Dominican Republic)',
            'es_EC' => 'Spanish (Ecuador)',
            'es_SV' => 'Spanish (El Salvador)',
            'es_GT' => 'Spanish (Guatemala)',
            'es_HN' => 'Spanish (Honduras)',
            'es_MX' => 'Spanish (Mexico)',
            'es_NI' => 'Spanish (Nicaragua)',
            'es_PA' => 'Spanish (Panama)',
            'es_PY' => 'Spanish (Paraguay)',
            'es_PE' => 'Spanish (Peru)',
            'es_ES' => 'Spanish (Spain)',
            'es_US' => 'Spanish (United States)',
            'es_UY' => 'Spanish (Uruguay)',
            'es_VE' => 'Spanish (Venezuela)',
            'sw_KE' => 'Swahili (Kenya)',
            'sw_TZ' => 'Swahili (Tanzania)',
            'ss_ZA' => 'Swati (South Africa)',
            'sv_FI' => 'Swedish (Finland)',
            'sv_SE' => 'Swedish (Sweden)',
            'tl_PH' => 'Tagalog (Philippines)',
            'tg_TJ' => 'Tajik (Tajikistan)',
            'ta_IN' => 'Tamil (India)',
            'tt_RU' => 'Tatar (Russia)',
            'te_IN' => 'Telugu (India)',
            'th_TH' => 'Thai (Thailand)',
            'bo_CN' => 'Tibetan (China)',
            'bo_IN' => 'Tibetan (India)',
            'tig_ER' => 'Tigre (Eritrea)',
            'ti_ER' => 'Tigrinya (Eritrea)',
            'ti_ET' => 'Tigrinya (Ethiopia)',
            'ts_ZA' => 'Tsonga (South Africa)',
            'tn_ZA' => 'Tswana (South Africa)',
            'tr_CY' => 'Turkish (Cyprus)',
            'tr_TR' => 'Turkish (Turkey)',
            'tk_TM' => 'Turkmen (Turkmenistan)',
            'ug_CN' => 'Uighur (China)',
            'uk_UA' => 'Ukrainian (Ukraine)',
            'hsb_DE' => 'Upper Sorbian (Germany)',
            'ur_PK' => 'Urdu (Pakistan)',
            'uz_UZ' => 'Uzbek (Uzbekistan)',
            've_ZA' => 'Venda (South Africa)',
            'vi_VN' => 'Vietnamese (Vietnam)',
            'wa_BE' => 'Walloon (Belgium)',
            'cy_GB' => 'Welsh (United Kingdom)',
            'fy_DE' => 'Western Frisian (Germany)',
            'fy_NL' => 'Western Frisian (Netherlands)',
            'wo_SN' => 'Wolof (Senegal)',
            'xh_ZA' => 'Xhosa (South Africa)',
            'yi_US' => 'Yiddish (United States)',
            'yo_NG' => 'Yoruba (Nigeria)',
            'zu_ZA' => 'Zulu (South Africa)'
        );
    }


    public static function return_BTC_MS($sheduleHour) {
        try {
            $currentHour = gmdate("G");
            print "\$currentHour = " . $currentHour . "\n";
            print "\$sheduleHour = " . $sheduleHour . "\n";

            if ($sheduleHour < $currentHour || $sheduleHour > 23) {
                $ms = 0;
            } else {
                $diffHour = $sheduleHour - $currentHour;
                $ms = $diffHour * 3600000; //1* 60 * 60 * 1000
            }
            print "\$ms = " . $ms . "\n";
            return $ms;
        } catch (Exception $ex) {
//            Utility::_exception(10010, $ex);
        }
    }

    public static function millisecsBetween($dateOne, $dateTwo, $abs = true) {
        $func = $abs ? 'abs' : 'intval';
        return $func(strtotime($dateOne) - strtotime($dateTwo)) * 1000;
    }

    public function post_curl($query, $url) {
//        echo $url;
//        echo Utility::json_encode($query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, Utility::json_encode($query));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
//        c_x($response,"reesponse");
        if (empty($response)) {
            $response = Utility::json_encode(array('event_created' => 'failed'));
        }
        return $response;
    }

    public static function _curl_request($url, array $data = array(), $method = 'GET') {

        try {
            if (empty($url)) {
                return false;
            }
            $ch = curl_init();
            if ($method == 'POST') {
                $headers = array(
                    'Content-Type: application/json'
                );
                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, Utility::json_encode($data));
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $curlInfo = curl_getinfo($ch);
            curl_close($ch);

            $curlInfoString = Utility::json_encode($curlInfo) . ' <=====> _curl_request:payload:: ' . Utility::json_encode($data);
            $log = Log::getInstance();
            $log->forcedLog('', '', Log4Php_LoggerLevel::getLevelError(), "_curl_request_log - " . $curlInfoString . " <=====>curl-resp::" . $result . "\n\n");

            return $result;
        } catch (Exception $ex) {
//            print_r($ex);
        }
    }

    public static function getDPNSettings($subdomain) {
        try {
            if (empty($subdomain)) {
                throw new Exception("Empty subdomain supplied.");
            }
            $settings = array();
            $objSetting = new DB_ProjectDefaultSettings("dpnSubDomain||$subdomain", "quantity||1");
            if ($objSetting->getResultCount() > 0) {
                $settingArr = Utility_Project::_get($objSetting->getProjectId());
                $settings = array(
                    'projectId' => $settingArr['projectId'],
                    'projectName' => $settingArr['projectName'],
                    'apiKey' => $settingArr['apiKey'],
                    'dpnGcmSenderId' => $settingArr['dpnGcmSenderId']
                );
            }
            return $settings;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function _terminate_api($error_code, $projectId, $raw, $option = false) {

        $args = array('projectId' => $projectId, 'die' => true);
//        die("cool");
        $objError = new Error_Code_Onsite($args);
        $objError->_raise($error_code, false, $raw, $option);
    }

    /*
     * send email to client when project user subscribe for mobile lead
     * 
     */

    public static function send_cro_notification($info, $to = array()) {
        try {
//            $htmlTemplate = DB_MailTemplate::getInvitationTemplate($resultArr['email'], ucwords($resultArr['name']), $link, $resultArr['image'], ucwords($resultArr['name']) . " has invite you to manage User Support Desk", $teamMemberName, ucfirst($resultArr['accountName']));
            $subject = 'Website New Booking Lead: Instant call-back requested';
            $htmlTemplate = '<!doctype html><html>'
                    . '<head><title></title></head>'
                    . '<body>'
                    . '<table align="center" style="margin:0 auto;width:700px;font-family:Arial;background-color:#ffffff;table-layout:fixed;font-size:16px;max-width: 700px" cellspacing="0" cellpadding="0">'
                    . '<tr>'
                    . '<tr>'
                    . '<td>Please call back the following lead who has visited the website and assist in completing the booking:</td>';
            if ($info['name'] != '') {
                $htmlTemplate.= '</tr>'
                        . '<td style="padding-left:20px">Name: ' . ucwords($info['name']) . '</td>'
                        . '</tr>';
            }
            $htmlTemplate.= '<tr>'
                    . '<td style="padding-left:20px">Phone No: ' . ucwords($info['phone']) . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td></td>'
                    . '</tr>'
                    . '</table>'
                    . '</body>'
                    . '</html>';
            //'cro@damacmaison.com'


            foreach ($to as $email) {
                DB_Utility::sendSimpleMail($email, $subject, $htmlTemplate, 'team@amplifyemails.com', false, false, "DAMAC Maison Website");
            }
        } catch (Exception $ex) {
            
        }
    }

    public static function isBot() {
// test if input is of the form +cc aa nnnn nnnn
        $reCaptcha = new ReCaptcha(GOOGLE_RECAPTCHA_SECRET_KEY);
        $resp = null;
//print_r($reCaptcha);
// Was there a reCAPTCHA response?
        if (isset($_POST["g-recaptcha-response"])) {
            $resp = $reCaptcha->verifyResponse(
                    $_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]
            );
//            print_r($resp);
        }if ($resp != null && $resp->success) {
            return false;
        } else {
            return true;
        }
    }

    public static function isPresentInEnterprise($projectId) {
        if (!empty($projectId)) {
            $PROCESS_ES_240_FOR_DEFAULT = unserialize(PROCESS_ES_240_FOR_DEFAULT);

//            return (in_array($projectId, $PROCESS_ES_240_FOR_DEFAULT)) ? true : false;

            /*
             *  after testing Es 2.4.0
             *  uncomment below  code and comment above code
             *  
             */

            /*
             *  for staging
             */

            if (isset($projectId) && ($projectId > 100000)) {
                return true;
            }

            return (!in_array($projectId, $PROCESS_ES_240_FOR_DEFAULT)) ? true : false;
        }
    }

    /**
     * Checks whether an attribute is in object oriented format or not.(Eg. order / order.custom_properties.sahil)<br/>
     * <b>1. Valid: </b>order.custom_properties.name<br/>
     * <b>2. InValid: </b>order.custom_properties.
     * @author Sahil Gulati <sahil@getamplify.com>
     * @param String Dynamic property name
     * @return boolean
     */
    public static function isValidDynamicProperty($dynamicProperty) {
        if (is_string($dynamicProperty)) {
            if (preg_match('#^[\w\-]+(\.[\w\-]+){0,}$#', $dynamicProperty)) {
                return true;
            }
        }
        return false;
    }

    public function insertDelayDataToMongo($projectId, $queueName, $data, $delay) {

        $mongoObj = new BOMongo_DelayData();
        $insertedMongoId = $mongoObj->_insert($projectId, $data, $delay);

        if (!empty($insertedMongoId)) {
            return array("_id" => $insertedMongoId);
        } elseif ($response['error']) {
            return $response['error'];
        } else {
            return false;
        }
    }

    public function generateOTP($countryCode, $phoneNumber) {

        if (!empty($phoneNumber)) {

            $otpObj = new OTP($countryCode, $phoneNumber);
            $result = $otpObj->generateOTP();
//            $result = Utility::json_encode(array('status'=>'success'));
            return $result;
        } else {
            return false;
        }
    }

    public function verifyOTP($countryCode, $phoneNumber, $otp) {

        if (!empty($phoneNumber) && !empty($otp)) {

            $otpObj = new OTP($countryCode, $phoneNumber, $otp);
            $result = $otpObj->verifyOTP();
//            $result = Utility::json_encode(array('status'=>'success'));
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @author krishna
     * @desc this function replace "space" by "_" and remove all special characters from string.
     */
    public static function cleanString($string) {
        $string = strtolower($string);
        $str = str_replace(" ", "-", $string);
        return preg_replace('/[^A-Za-z0-9\_\-\']/', '', $str);
    }

    public static function checkSpecialChar($str) {
        if (preg_match('/[^A-Za-z0-9\_\-\']/', $str)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getJsApiURL() {
        return 'https://' . JS_CLOUDFRONT_DOMAIN . '/' . trim(API_JS_PATH, '/');
    }

    public static function getCdnUrl($imageUrl) {
        $imageUrl = str_replace('s3.amazonaws.com/www.betaoutcdn.com', BETAOUTCDN_CLOUDFRONT_DOMAIN, $imageUrl);
        return $imageUrl;
    }

    public static function json_encode($msg, $opt = '') {
        return json_encode($msg, $opt | JSON_PARTIAL_OUTPUT_ON_ERROR);
    }

    public static function getpidinfo($pid = 0, $ps_opt = " -p ") {
        if (!$pid) {
            $pid = getmypid();
        }
        $pidinfo = shell_exec("ps " . $ps_opt . $pid . " -u");
        return $pidinfo;
    }

    /**
     * @desc it is used for filter recursive multidimention array. it only removed null or blank array key value not false value. 
     */
    public static function filterRecursiveMultiDArray($array) {
        $array = array_map(function($item) {
            return is_array($item) ? self::filterRecursiveMultiDArray($item) : $item;
        }, $array);
        return array_filter($array, function($item) {
            return $item !== "" && $item !== null && (!is_array($item) || count($item) > 0);
        });
    }

    public static function checkNaN($val) {
        if (is_nan($val) || is_infinite($val)) {
            return 0;
        } else {
            return $val;
        }
    }

    public static function _add_to_user_property_nf_queueing($projectId, $message) {
        try {
            if (!empty($projectId)) {
                $enterprise_client = unserialize(ENTERPRISE_CLIENT_ARRAY);
                if (in_array($projectId, $enterprise_client)) {
                    $rmqObj = new RabbitMQ_Elasticsearch54_UserPropertyNF($projectId);
                    $rmqObj->_produce($message);
                } else {
                    $queueName = 'default';
                    $rmqObj = new RabbitMQ_Elasticsearch54_UserPropertyNF($queueName);
                    $rmqObj->_produce($message);
                }
            }
        } catch (Exception $e) {
//            print_r($e);
        }
    }

    public static function _add_to_user_property_shard_es_nf($message, $shardId = false, $projectId = 0) {
        try {
            if (!empty($message) && !empty($shardId) && !empty($projectId)) {
                $enterprise_client = unserialize(ENTERPRISE_CLIENT_ARRAY);
                if (in_array($projectId, $enterprise_client)) {
                    $queue_name = $projectId . "_" . $shardId;
                } else {
                    $queue_name = "default_" . $shardId;
                }

                $rmqObj = new RabbitMQ_Elasticsearch54_UserPropertyShardEsNF($queue_name);
                $rmqObj->_produce($message);
            } else {
                $queue_name = "error";
                $rmqObj = new RabbitMQ_Elasticsearch54_UserPropertyShardEsNF($queue_name);
                $rmqObj->_produce($message);
            }
        } catch (Exception $e) {
            
        }
    }

    public static function _add_to_user_import_property_shard_es_nf($message, $shardId = false, $projectId = 0) {
        /*
         * for user import , bulk user api
         */
        try {
            if (!empty($message) && !empty($shardId) && !empty($projectId)) {

                $shardId = ($shardId % 5) + 1;

                $enterprise_client = unserialize(ENTERPRISE_CLIENT_ARRAY);
                if (in_array($projectId, $enterprise_client)) {
                    $queue_name = $projectId . "_" . $shardId;
                } else {
                    $queue_name = "default_" . $shardId;
                }

                $rmqObj = new RabbitMQ_Elasticsearch54_UserBulkImportEsShard($queue_name);
                $rmqObj->_produce($message);
            } else {
                $queue_name = "error";
                $rmqObj = new RabbitMQ_Elasticsearch54_UserBulkImportEsShard($queue_name);
                $rmqObj->_produce($message);
            }
        } catch (Exception $e) {
            
        }
    }

    public static function getBasicSystemInfo($ip = false, $useragent = false, $system = false) {
        try {
            $basicInfo = array();
            if (empty($ip) || empty($useragent)) {
                $info = RabbitMQ_Tools::get_client_basic_info($ip, $useragent);
            }
            if ($system) {
                $basicInfo['system'] = $system;
            }
            if ($useragent) {
                $basicInfo['userAgent'] = $useragent;
            } else {
                $basicInfo['userAgent'] = $info['useragent'];
            }
            if ($ip) {
                $basicInfo['ip'] = $ip;
            } else {
                $basicInfo['ip'] = $info['ip'];
            }
            return $basicInfo;
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    public static function _divide_user_custom_property_queue($userId, $projectId, $propertyArray, $case) {

        if (!empty($userId) && !empty($projectId) && !empty($propertyArray)) {
            $shardId = Utility::_getShardId($userId, 5);
            $enterprise_client = unserialize(ENTERPRISE_CLIENT_ARRAY);


            if (in_array($projectId, $enterprise_client)) {
                $queue_name = $projectId . "_" . $shardId;
            } else {
                $queue_name = "default_" . $shardId;
            }

            $message['projectId'] = $projectId;
            $message['userId'] = $userId;
            $message['propertyArray'] = $propertyArray;
            $message['case'] = $case;
            $rmqObj = new RabbitMQ_Elasticsearch54_UserCustomPropertyShardEsNF($queue_name);
            $rmqObj->_produce($message);
        }
    }

    public static function createMongoIdFromTimestamp($timestamp) {
        $inc = 0;
        $ts = pack('N', $timestamp);
        $m = substr(md5(gethostname()), 0, 3);
        $pid = pack('n', getmypid());
        $trail = substr(pack('N', $inc++), 1, 3);

        $bin = sprintf('%s%s%s%s', $ts, $m, $pid, $trail);

        $id = '';
        for ($i = 0; $i < 12; $i++) {
            $id .= sprintf('%02X', ord($bin[$i]));
        }
        return new \MongoDB\BSON\ObjectID($id);
    }

    public static function is_valid_timestamp($timestamp) {
        if (strlen($timestamp) > 10 || !is_numeric($timestamp)) {
            return false;
        } else {
            return true;
        }
    }

    public static function create_image_via_html($customHtml, $height = 200, $width = 400) {
        $params = array("html" => $customHtml, 'height' => $height, 'width' => $width);
        if (!is_numeric($height) || $height < 10) {
            unset($params['height']);
        }
        if (!is_numeric($width) || $width < 10) {
            unset($params['width']);
        }

        $ci = curl_init();
        curl_setopt($ci, CURLOPT_URL, "https://boscreen.betaout.com/html");
//        curl_setopt($ci, CURLOPT_PORT, 9200);
        curl_setopt($ci, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ci, CURLOPT_TIMEOUT, 10);
        curl_setopt($ci, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ci, CURLOPT_FORBID_REUSE, 0);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ci);
        $curlInfo = curl_getinfo($ci);
        curl_close($ci);
        return $response;
    }

    public static function _add_to_EmailPropUpdate($message) {
        try {
            $obj = new RabbitMQ_EmailPropUpdate();
            $obj->_produce($message);
        } catch (Exception $e) {
            throw $ex;
        }
    }

}
