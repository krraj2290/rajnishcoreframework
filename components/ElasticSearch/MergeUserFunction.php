<?php

/**
 * @author Rajnish Kr. Rajesh
 * 
 * Date 09 Oct 2017 
 */
class ElasticSearch_MergeUserFunction {

    protected $_registerUserArray = array();
    protected $_register_all_User_array = array();
    protected $_merge_with_user = array();
    protected $_merged_in_user = array();
    protected $_un_registerUserArray = array();
    protected $_un_register_all_User_array = array();
    protected $_projectId;
    protected $_userCount = 1;
    protected $_unregister_send_message_to_delete = true;
    protected $_project_property_cache;
    protected $_user_update_array = array();
    protected $_user_custom_update_array = array();
    protected $_first_array = array();
    protected $_second_array = array();
    protected $_message = array();
    protected $process_message = true;
    static $dpn_subscribe_property_id = array();
    protected $_dpn_unregister_user = array();
    protected $_user_merge_logger = array();
    protected $_unreg_user_have_purchased_property = array();
    protected $_acquisition_selected = false;

    protected function _singleCustomRegisterUser_singleCustom_un_registerUser_merge($projectId) {
 
        $registerUserDoc = $this->_get_user_custom_doc_test($projectId, $this->_registerUserArray[0], true);
       
        if (!$this->process_message) {
            return false;
        }

        $un_registerUserDoc = $this->_get_user_custom_doc_test($projectId, $this->_un_registerUserArray[0]);
        if (!$this->process_message) {
            return false;
        }
        
        if ($un_registerUserDoc['projectId'] == $this->_projectId) {
//            $this->check_dpn_subscription_user($un_registerUserDoc, $this->_projectId);
            $second = array("user_id" => $this->_un_registerUserArray[0], "doc" => $un_registerUserDoc);
            $this->_merge_custom_user($registerUserDoc, $second);
            echo "<br/><br/><br/><br/>================================user property========================================<br/><br/><br/><br/>";
             print_r($this->_user_update_array);
            echo "<br/><br/><br/><br/><br/>================================custom property========================================\<br/><br/><br/><br/>";
            print_r($this->_user_custom_update_array);
//            die;
//            $this->_send_property_queue($this->_registerUserArray[0], $this->_user_update_array);
        }
    }

    protected function _singleCustomRegisterUser_multiCustom_un_registerUser_merge($projectId) {

        /*
         *  first merge un_register user to final un_register user 
         *  and then merge final un_register user  with $_un_register_user
         */
        if (count($this->_un_registerUserArray) > 1) {
            /*
             *  get first unregister user from $this->_un_registerUserArray 
             *  and consider first unregister user as a register user to convert all unregister user into single unregister user.
             */
            foreach ($this->_un_registerUserArray as $index => $first_user_id) {
                $_un_register_user = $this->_get_user_custom_doc($projectId, $first_user_id);
                if (!$this->process_message) {
                    return false;
                }
                unset($this->_un_registerUserArray[$index]);
                if ($_un_register_user['projectId'] == $this->_projectId) {
                    break;
                } else {
                    $_un_register_user = false;
                }
            }
//            $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);

            /*
             *  merge all unregister user into single unregister user
             */

            foreach ($this->_un_registerUserArray as $user_id) {
                $userDoc2 = $this->_get_user_custom_doc($projectId, $user_id);
//                $this->check_dpn_subscription_user($userDoc2, $this->_projectId);
                $second = array("user_id" => $user_id, "doc" => $userDoc2);
                $this->_merge_custom_user($_un_register_user, $second);
                $_un_register_user = $this->_reset();
            }

            /*
             *  now finally merge final unregister user with register user
             */

            if (!empty($_un_register_user)) {
                $registerUserDoc = $this->_get_user_custom_doc($projectId, $this->_registerUserArray[0], true);
                if (!$this->process_message) {
                    return false;
                }

                $second = array("user_id" => $first_user_id, "doc" => $_un_register_user);
                $this->_merge_custom_user($registerUserDoc, $second);
//                $this->_send_property_queue($this->_registerUserArray[0], $this->_user_update_array);
            }
        }
    }

    protected function _multiCustomRegisterUser_singleCustom_un_registerUser_merge($projectId) {
        /*
         *  merge $this->_un_registerUserArray user with all register user
         */

        if (count($this->_registerUserArray) > 1) {
            $second_user_id = $this->_un_registerUserArray[0];
            $_un_register_user = $this->_get_user_custom_doc($projectId, $second_user_id);
            if (!$this->process_message) {
                return false;
            }


            if ($_un_register_user['projectId'] == $this->_projectId) {
//                $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);
                $second = array("user_id" => $second_user_id, "doc" => $_un_register_user);
                foreach ($this->_registerUserArray as $user_id) {
                    $register_user = $this->_get_user_custom_doc($projectId, $user_id, true);
                    $this->_merge_custom_user($register_user, $second);
//                    $this->_send_property_queue($user_id, $this->_user_update_array);
                }
            }
        }
    }

    protected function _multiCustomRegisterUser_multiCustom_un_registerUser_merge($projectId) {
        /*
         *  first merge all un_register user 
         *  and then merge final un_register user  with all  register_user
         */


        if (count($this->_un_registerUserArray) > 1) {
            foreach ($this->_un_registerUserArray as $index => $first_user_id) {
                $_un_register_user = $this->_get_user_custom_doc_test($projectId, $first_user_id);
                if (!$this->process_message) {
                    return false;
                }
                unset($this->_un_registerUserArray[$index]);
                if ($_un_register_user['projectId'] == $this->_projectId) {
                    break;
                } else {
                    $_un_register_user = false;
                }
            }
            if (empty($_un_register_user)) {
                return false;
            }

//            $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);

            foreach ($this->_un_registerUserArray as $user_id) {
                $userDoc2 = $this->_get_user_custom_doc_test($projectId, $user_id);
                if ($userDoc2['projectId'] != $this->_projectId) {
                    continue;
                }
//                $this->check_dpn_subscription_user($userDoc2, $this->_projectId);
                $second = array("user_id" => $user_id, "doc" => $userDoc2);
                $this->_merge_custom_user($_un_register_user, $second);
                $_un_register_user = $this->_reset();
            }
        }
    }

    protected function _get_user_custom_doc($projectId, $userId, $registerUser = false) {
        $start = microtime(true);
        if (!$this->process_message) {
            return false;
        }
        $result = false;


        $conditon = array(array("match" => array("userId" => $userId)));
        if (!empty($userId)) {
            $user_query = array("query" => array("bool" => array("must" => $conditon)));
            if (!empty($projectId) && !empty($user_query)) {
                echo "\n**************************** query ****************************\n";
                echo json_encode($user_query);
                echo "\n***************************************************************\n";
                $user_result = $this->_getCustomUserBy_scroll($user_query, $projectId, '', 'urza');
                echo "\n******************* Total Users to be fetched: ";
                $count = $user_result['hits']['total'];
                echo $count;
            }

            $customResult = array();
            $j = 0;
            while (1) {
                $_scroll_id = $user_result['_scroll_id'];
                $user_result = $this->_getCustomUserBy_scroll($query, $projectId, $_scroll_id, 'urza');

                $total = count($user_result['hits']['hits']);

                echo "\n******************* Processing scroll of size: ";
                echo $total;

                if ($total > 0) {
                    for ($i = 0; $i < $total; $i++) {
                        $j++;
                        if (!empty($user_result["hits"]["hits"])) {
                            $result = $user_result["hits"]["hits"][$i]['_source'];
                            $key = $user_result["hits"]["hits"][$i]['_source']['name'];
                            $customResult[$key] = $user_result["hits"]["hits"][$i]['_source'];
                            if (($registerUser && ($result['projectId'] != $this->_projectId)) && ($result['projectId'] == ES_INACTIVE_PROJECT_ID)) {
                                echo "\n =========== Register user  userId : " . $userId . "=== found in  projectId : " . $result['projectId'] . "===\n";
                                echo "\n =========== Need to move register user to project  " . $this->_projectId . " from project =" . $result['projectId'] . " \n";

                                $shardId = Identifier_Utility::getShardId($userId, $this->_projectId);
                                $forUpdateArr['projectId'] = $this->_projectId;
                                $forUpdateArr['user_projectId_changed_from'] = "merge_user";
                                $elastic = '';
                                $elastic.= Utility::json_encode(array('update' => array('_id' => $userId, "routing" => $shardId, "_retry_on_conflict" => 5))) . "\n";
                                $elastic.= Utility::json_encode(array('doc' => $forUpdateArr, "doc_as_upsert" => true)) . "\n";

//                                $this->user_bulk_request($this->_projectId, $shardId, $elastic);
                            } else if ($result['projectId'] == ES_INACTIVE_PROJECT_ID) {
                                echo "\n =========== UnRegister user  userId : " . $userId . "=== found in  projectId : " . $result['projectId'] . "===\n";
                            }
                        } else {
                            echo "\n ===========user not found for userId : " . $userId . "=== and projectId : " . $this->_projectId . "===\n";
                            echo "\n message is queue into RabbitMQ_UserMergeUnprocess \n";

                            $queueName = $this->_message['previsousQueue'];
                            echo PHP_EOL . "user sent to queue : " . $queueName . PHP_EOL;

                            if ($queueName == "user_merge_unprocess") {
                                echo "\n ==insert into BOMongo_UserMergeUnprocess  ====";
//                                $mongoObject = new BOMongo_UserMergeUnprocess();
//                                $mongoObject->_insert($this->_message);
                            }
//                            $obj = new RabbitMQ_UserMergeUnprocess($queueName);
//                            $obj->_produce($this->_message);

                            $this->process_message = false;
                            $this->_user_merge_logger["userDocmentNotFoundId"] = $userId;
                            $this->_user_merge_logger["userDocmentNotFoundMessage"] = $this->_message;
                        }
                    }
                } else {
                    break;
                }
            }
        }
        echo "\n ==_get_user_doc = $userId ==tim in ms ====" . ((microtime(true) - $start) * 1000);


        /*
         *  below code get user document for tracking user merge process 
         *  Rajnish Kr. Rajesh 
         *  @date 09 Oct 2017
         */

        $userDocument = $customResult;
        if (!empty($userDocument)) {
            $userKey = "user" . $this->_userCount;
            $this->_user_merge_logger[$userKey] = Utility::json_encode($userDocument);
            $this->_userCount++;
        }
        return $customResult;
    }

    protected function _merge_custom_user($firstUser, $secondUser) {
        if (!$this->process_message) {
            return false;
        }
        if (!empty($firstUser) && !empty($secondUser['doc'])) {
            $this->_first_array = $firstUser;
            $this->_second_array = $secondUser['doc'];
            /*
             *  merge $this->_second_array to $this->_first_array
             *  get all propertyId from $this->_second_array and set into $this->_first_array if not exist and if updated type.
             */
            $this->_other_custom_plain_property();
            $this->_merge_with_user[] = $secondUser['user_id'];
            $this->_merged_in_user[] = isset($firstUser['_userId']) ? $firstUser['_userId'] : $firstUser['userId'];
        }
    }

    protected function _other_custom_plain_property() {
        if (!empty($this->_second_array)) {
            foreach ($this->_second_array as $propertyName => $propertyArray) {
                $propertyType = ElasticSearch_PropertySuffix::_get_property_type($propertyName);
                if ($propertyType == "number") {
                    $propertyValue = 'value_number';
                }
                if ($propertyType == "text") {
                    $propertyValue = 'value_text';
                }
                if ($propertyType == "xs:date") {
                    $propertyValue = 'value_date';
                }

                if (empty($propertyArray[$propertyValue])) {
                    $this->_second_array = $this->_unset_filed($this->_second_array, 'name');
                    continue;
                }

                /*
                 * check unregister user have purchased property or not 
                 * for merge unregister user order to register user.
                 */


                if (in_array($propertyName, $this->_ignore_property)) {
                    $this->_second_array = $this->_unset_filed($this->_second_array, 'name');
                    continue;
                }

                if (!empty($propertyName)) {
                    $case = '';
                    if (!in_array($propertyName, $this->_first_array)) {
//                        if (($propertyType == "number") || !empty($this->_second_array[$propertyIndex]['name'])) {
                        $addValue = $this->_first_array[$propertyName] = $this->_second_array[$propertyName];
                        $case = "update";
//                        }
                    } else {
                        if (($propertyType == "text") && !empty($this->_second_array[$propertyName])) {
                            if (in_array($propertyName, $this->_ignore_text_property)) {
                                $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
                                continue;
                            }

                            $result = $this->_combine_custom_array_value($this->_first_array[$propertyName], $this->_second_array[$propertyName], $propertyType);
                            $this->_first_array[$propertyName] = $result["final_array"];
                            if (empty($result["update_arr"])) {
                                continue;
                            }
                            $addValue = $result["update_arr"];
                            $case = "add";
                        } else if (($propertyType == "xs:date") && !empty($this->_second_array[$propertyName])) {
                            $case = "update";
                            $addValue = $this->_first_array[$propertyName];
                        } else if (($propertyType == "number") && !empty($this->_second_array[$propertyName])) {
                            $addValue = (int) $this->_first_array[$propertyName];
                            $case = "update";
                        }
                    }
                    if (!empty($case)) {
                        $this->_cutom_property_set($case, $propertyType, $propertyName, $addValue);
                    }
                }
                $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
            }
        }
    }

    protected function _cutom_property_set($case, $type, $propertyName, $propertyValue) {
        if (!empty($case) && (($case == 'add') || ($case == 'update') || ($case == 'increment') || ($case == 'ignore')) && !empty($propertyName))
            $this->_user_custom_update_array['custom'][$case][$propertyName] = array("value" => $propertyValue, "type" => $type);
    }

    function _getCustomUserBy_scroll($query, $projectId, $scroll_id = '', $getFromIndex = 'urza') {
        $size = 100;

        $_ENTERPRISE_CLIENT_ARRAY = unserialize(ENTERPRISE_CLIENT_ARRAY);
        if (empty($scroll_id)) {
            if (in_array($projectId, $_ENTERPRISE_CLIENT_ARRAY)) {
                $baseUri = 'http://' . constant('P_' . $projectId . '_PRIVATE_URL') . '/' . $getFromIndex . '/user_custom_property/' . '_search?search_type=scan&scroll=3m&size=' . $size;
                $port = constant('P_' . $projectId . '_PORT');
            } else {
                $baseUri = 'http://' . P_FINAL_240_SEARCH_PRIVATE_URL . '/' . $getFromIndex . '/user_custom_property/' . '_search?search_type=scan&scroll=3m&size=' . $size;
                $port = ES_PORT;
            }
        } else {
            $port = ES_PORT;
            if (in_array($projectId, $_ENTERPRISE_CLIENT_ARRAY)) {
                $baseUri = 'http://' . constant('P_' . $projectId . '_PRIVATE_URL') . "/_search/scroll?scroll=3m&scroll_id=$scroll_id";
                $port = constant('P_' . $projectId . '_PORT');
            } else {
                $baseUri = 'http://' . P_FINAL_240_SEARCH_PRIVATE_URL . '/' . "_search/scroll?scroll=3m&scroll_id=$scroll_id";
                $port = ES_PORT;
            }
        }
//    echo "\n \n =====baseUri is $baseUri\n \n ";
        if (!empty($scroll_id)) {
            $userArray = $this->_get_request_scroll($baseUri);
        } else {
            $userArray = $this->_get_request_scroll($baseUri, $query);
        }
        return $userArray;
    }

    function _get_request_scroll($baseUri, $query = '') {

        $ci = curl_init();
        curl_setopt($ci, CURLOPT_URL, $baseUri);
        curl_setopt($ci, CURLOPT_PORT, 9200);
        if (!empty($query)) {
            curl_setopt($ci, CURLOPT_POSTFIELDS, Utility::json_encode($query));
        }
        curl_setopt($ci, CURLOPT_TIMEOUT, 200);
        curl_setopt($ci, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ci, CURLOPT_FORBID_REUSE, 0);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ci);
        curl_close($ci);
        $res = json_decode($response, true);
        //print_r($res);
        return $res;
    }

    protected function _combine_custom_array_value($first_array, $second_array, $type) {
        $update_arr = array();

        if (empty($first_array)) {
            $first_array_new = $update_arr = $second_array;
        } else {
            if (!empty($second_array)) {
                $second_array = array_filter($second_array);
                if ($type == "text") {
                    $update_arr[] = $second_array;
                    $first_array[$type] = $first_array[$type] + $second_array[$type];
                    $first_array_new = $first_array;
                } else {
                    $update_arr[] = $second_array;
                    $first_array[$type] = $second_array[$type];
                    $first_array_new = $first_array;
                }
            }
        }
        return array("update_arr" => array_filter($update_arr), "final_array" => array_filter($first_array_new));
    }

    protected function _combine_array_value($first_array, $second_array) {
        $update_arr = array();

        if (empty($first_array)) {
            $first_array_new = $update_arr = $second_array;
        } else {
            if (!empty($second_array)) {
                $second_array = array_filter($second_array);

                foreach ($second_array as $val) {
                    if (is_array($first_array) && !in_array($val, $first_array)) {
                        $update_arr[] = $val;
                        $first_array[] = $val;
                        $first_array_new = $first_array;
                    } else if (!is_array($first_array)) {
                        $update_arr[] = $val;
                        $first_array_new[] = $first_array;
                        $first_array_new[] = $val;
                    }
                }
            }
        }
        return array("update_arr" => array_filter($update_arr), "final_array" => array_filter($first_array_new));
    }

    protected function _plain_property_set($case, $type, $propertyName, $propertyValue) {
        if (!empty($case) && (($case == 'add') || ($case == 'update') || ($case == 'increment') || ($case == 'ignore')) && !empty($propertyName))
            $this->_user_update_array['plain'][$case][$propertyName] = array("value" => $propertyValue, "type" => $type);
    }

    protected function _unset_filed($user_array, $propertyFiled) {
        unset($user_array[$propertyFiled]);
        return $user_array;
    }

    protected function _select_date($first_date, $second_date, $selection = "later") {
        $first_strtotime = strtotime($first_date);
        $second_strtotime = strtotime($second_date);
        return ($selection == "earlier") ? (($first_strtotime > $second_strtotime) ? $second_date : $first_date) : (($first_strtotime > $second_strtotime) ? $first_date : $second_date );
    }

    protected function _reset() {
        $this->_acquisition_selected = false;
        $this->_user_update_array = array();
        $this->_user_merge_logger = array();
        return $this->_first_array;
    }

    protected function _getTimestamp($first, $second) {
        /*
         *  give priority to user 
         *  Note : lastest user have first priority else second user;
         */
        try {
            $_id = new MongoDB\BSON\ObjectID($first);
            $first_timestamp = $_id->getTimestamp();

            $_id = new MongoDB\BSON\ObjectID($second);
            $second_timestamp = $_id->getTimestamp();

            return ($first_timestamp > $second_timestamp) ? 'first' : 'second';
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    protected function _get_user_custom_doc_test($projectId, $userId, $registerUser = false) {
        $start = microtime(true);
        if (!$this->process_message) {
            return false;
        }
        $result = false;


        $conditon = array(array("match" => array("userId" => $userId)));
        if (!empty($userId)) {
            $user_query = array("query" => array("bool" => array("must" => $conditon)));
            if (!empty($projectId) && !empty($user_query)) {
                echo "\n**************************** query ****************************\n";
                echo json_encode($user_query);
                echo "\n***************************************************************\n";
                if ($registerUser) {
                    $json = '{
   "took": 7,
   "timed_out": false,
   "_shards": {
      "total": 20,
      "successful": 20,
      "failed": 0
   },
   "hits": {
      "total": 6,
      "max_score": 13.367785,
      "hits": [
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_delivery_date_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "userId": "590b29338d8fb54760a9b7c7",
               "name": "cart_delivery_date",
               "value_date": {
                  "day": "12",
                  "month": "07",
                  "year": "2017",
                  "day_month": "12-07",
                  "year_month_day": "2017-07-12",
                  "timestamp": "2017-07-12T00:00:00",
                  "weekday": "wednesday"
               }
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_acquisition_ucid_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "name": "acquisition_ucid",
               "userId": "590b29338d8fb54760a9b7c7",
               "value_text": [
                  452681
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_segmentation_last_process_date_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "userId": "590b29338d8fb54760a9b7c7",
               "name": "segmentation_last_process_date",
               "value_date": {
                  "day": "11",
                  "month": "09",
                  "year": "2017",
                  "day_month": "11-09",
                  "year_month_day": "2017-09-11",
                  "timestamp": "2017-09-11T00:06:35",
                  "weekday": "monday"
               }
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_recipient_name_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "userId": "590b29338d8fb54760a9b7c7",
               "name": "cart_recipient_name",
               "value_text": [
                  "Madhavi Sharma"
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_recipient_city_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "userId": "590b29338d8fb54760a9b7c7",
               "name": "cart_recipient_city",
               "value_text": [
                  "MUMBAI"
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_occasion_590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_parent": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "userId": "590b29338d8fb54760a9b7c7",
               "name": "cart_occasion",
               "value_text": [
                  "Anniversary"
               ]
            }
         }
      ]
   }
}';
                }else{
                    $json = '{
   "took": 111,
   "timed_out": false,
   "_shards": {
      "total": 20,
      "successful": 20,
      "failed": 0
   },
   "hits": {
      "total": 6,
      "max_score": 13.55252,
      "hits": [
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_delivery_date_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "userId": "59679ab68d8fb54760d690a7",
               "name": "cart_delivery_date",
               "value_date": {
                  "day": "14",
                  "month": "07",
                  "year": "2017",
                  "day_month": "14-07",
                  "year_month_day": "2017-07-14",
                  "timestamp": "2017-07-14T00:00:00",
                  "weekday": "friday"
               }
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_acquisition_ucid_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "name": "acquisition_ucid",
               "userId": "59679ab68d8fb54760d690a7",
               "value_text": [
                  452684
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_segmentation_last_process_date_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "userId": "59679ab68d8fb54760d690a7",
               "name": "segmentation_last_process_date",
               "value_date": {
                  "day": "30",
                  "month": "08",
                  "year": "2017",
                  "day_month": "30-08",
                  "year_month_day": "2017-08-30",
                  "timestamp": "2017-08-30T17:28:26",
                  "weekday": "wednesday"
               }
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_recipient_name_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "userId": "59679ab68d8fb54760d690a7",
               "name": "cart_recipient_name",
               "value_text": [
                  "Aafreen Marjan"
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_recipient_city_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "userId": "59679ab68d8fb54760d690a7",
               "name": "cart_recipient_city",
               "value_text": [
                  "KOLKATA"
               ]
            }
         },
         {
            "_index": "urza_5_6",
            "_type": "user_custom_property",
            "_id": "31801_cart_occasion_59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_parent": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "userId": "59679ab68d8fb54760d690a7",
               "name": "cart_occasion",
               "value_text": [
                  "Birthday"
               ]
            }
         }
      ]
   }
}';
                }
                $user_result = json_decode($json,true);
                echo "\n******************* Total Users to be fetched: ";
                $count = $user_result['hits']['total'];
                echo $count;
            }

            $customResult = array();
            $j = 0;
            while (1) {
//                $_scroll_id = $user_result['_scroll_id'];
//                $user_result = $this->_getCustomUserBy_scroll($query, $projectId, $_scroll_id, 'urza');

                $total = count($user_result['hits']['hits']);

                echo "\n******************* Processing scroll of size: ";
                echo $total;

                if ($total > 0) {
                    for ($i = 0; $i < $total; $i++) {
                        $j++;
                        if (!empty($user_result["hits"]["hits"])) {
                            $result = $user_result["hits"]["hits"][$i]['_source'];
                            $key = $user_result["hits"]["hits"][$i]['_source']['name'];
                            $customResult['projectId'] = $user_result["hits"]["hits"][$i]['_source']['projectId'];
                            $customResult[$key] = $user_result["hits"]["hits"][$i]['_source'];
                            if (($registerUser && ($result['projectId'] != $this->_projectId)) ) {
//                            if (($registerUser && ($result['projectId'] != $this->_projectId)) && ($result['projectId'] == ES_INACTIVE_PROJECT_ID)) {
                                echo "\n =========== Register user  userId : " . $userId . "=== found in  projectId : " . $result['projectId'] . "===\n";
                                echo "\n =========== Need to move register user to project  " . $this->_projectId . " from project =" . $result['projectId'] . " \n";

                                $shardId = Identifier_Utility::getShardId($userId, $this->_projectId);
                                $forUpdateArr['projectId'] = $this->_projectId;
                                $forUpdateArr['user_projectId_changed_from'] = "merge_user";
                                $elastic = '';
                                $elastic.= Utility::json_encode(array('update' => array('_id' => $userId, "routing" => $shardId, "_retry_on_conflict" => 5))) . "\n";
                                $elastic.= Utility::json_encode(array('doc' => $forUpdateArr, "doc_as_upsert" => true)) . "\n";

//                                $this->user_bulk_request($this->_projectId, $shardId, $elastic);
                            } 
                        } else {
                            echo "\n ===========user not found for userId : " . $userId . "=== and projectId : " . $this->_projectId . "===\n";
                            echo "\n message is queue into RabbitMQ_UserMergeUnprocess \n";

                            $queueName = $this->_message['previsousQueue'];
                            echo PHP_EOL . "user sent to queue : " . $queueName . PHP_EOL;

                            if ($queueName == "user_merge_unprocess") {
                                echo "\n ==insert into BOMongo_UserMergeUnprocess  ====";
//                                $mongoObject = new BOMongo_UserMergeUnprocess();
//                                $mongoObject->_insert($this->_message);
                            }
//                            $obj = new RabbitMQ_UserMergeUnprocess($queueName);
//                            $obj->_produce($this->_message);

                            $this->process_message = false;
                            $this->_user_merge_logger["userDocmentNotFoundId"] = $userId;
                            $this->_user_merge_logger["userDocmentNotFoundMessage"] = $this->_message;
                        }
                    }
                     break;
                } else {
                    break;
                }
            }
        }
        echo "\n ==_get_user_doc = $userId ==tim in ms ====" . ((microtime(true) - $start) * 1000);


        /*
         *  below code get user document for tracking user merge process 
         *  Rajnish Kr. Rajesh 
         *  @date 09 Oct 2017
         */

        $userDocument = $customResult;
//        if (!empty($userDocument)) {
//            $userKey = "user" . $this->_userCount;
//            $this->_user_merge_logger[$userKey] = Utility::json_encode($userDocument);
//            $this->_userCount++;
//        }
        return $customResult;
    }

}
