<?php

/**
 * @author Rajnish Kr. Rajesh
 * 
 * Date 4 Oct 2017 
 */
class ElasticSearch_MergeUser extends ElasticSearch_MergeUserFunction {
    /*
      Update unregistered properties with registered user.
      Merge Identifier user properties unregistered user to register user if register user doesn't have unregistered user property.
     */

    protected $_registerUserArray = array();
    protected $_register_all_User_array = array();
    protected $_merge_with_user = array();
    protected $_merged_in_user = array();
    protected $_un_registerUserArray = array();
    protected $_un_register_all_User_array = array();
    protected $_projectId;
    protected $_userCount = 1;
    protected $_unregister_send_message_to_delete = true;
    protected $_project_property_cache;
    protected $_user_update_array = array();
    protected $_fixed_plain_property_array = array(array("pn" => "first_purchase_date", "for" => "earlier"), array("pn" => "firstseen_date", "for" => "earlier"), array("pn" => "acquisition_source", "for" => "earlier"), array("pn" => "acquisition_medium", "for" => "earlier"), array("pn" => "acquisition_campaign", "for" => "earlier"));
    protected $_first_array = array();
    protected $_second_array = array();
    protected $_message = array();
    protected $process_message = true;
    static $dpn_subscribe_property_id = array();
    protected $_dpn_unregister_user = array();
    protected $_user_merge_logger = array();
    protected $_unreg_user_have_purchased_property = array();
    protected $_user_nested_property = array("event", "campaign", "product", "brand", "category", "campaigntype", "channel", "apn_property", "gcm_property");
//    protected $_user_nested_property = array("event", "campaign", "product", "brand", "category", "campaigntype", "channel");
    protected $_acquisition_selected = false;
    protected $_ignore_property = array("lifecycleid", "segmentid"
        , "avg_revenue_per_month", "last_120_days_gmv", "last_120_days_purchased", "rfm_total_score", "total_frequecy_monetory", "avg_order_value", "frequency_score", "highest_order_value", "last_30_days_gmv", "last_30_days_purchased"
        , "last_365_days_gmv", "last_365_days_purchased", "last_60_days_gmv", "last_60_days_purchased", "last_7_days_gmv", "last_7_days_purchased",
        "last_90_days_gmv", "last_90_days_purchased", "last_purchased_value", "lowest_order_value", "monetary_score", "orders_withoutpromo_percentage", "orders_withpromo_percentage", "recency_score", "total_revenue", "time_to_1st_purchased"
    );
    protected $_ignore_text_property = array("firstname", "fullname", "lastname", "usertag", "timezone", "can_receive_email");  // ignore only if both user have exists property.

    public function _process($projectId, $registerArray, $unRegisterArray, $previsousQueue = false) {

        echo "\n ================================================ " . __CLASS__ . " ===========================================\n";
//        print_r(func_get_args());


        $this->_userCount = 1;
        $this->_unregister_send_message_to_delete = true;

        $unRegisterUserArray = false;
        foreach ($unRegisterArray as $unRegister_array) {
            $unRegisterUserArray[] = isset($unRegister_array['userId']) ? $unRegister_array['userId'] : $unRegister_array;
        }
        $registerUserArray = false;
        foreach ($registerArray as $register_array) {
            $registerUserArray[] = isset($register_array['userId']) ? $register_array['userId'] : $register_array;
        }

        $this->_register_all_User_array = $this->_registerUserArray = $registerUserArray;
        $this->_un_register_all_User_array = $this->_un_registerUserArray = $unRegisterUserArray;
        $this->_projectId = $projectId;
        if (!is_array($this->_registerUserArray) || !is_array($this->_un_registerUserArray) || empty($this->_projectId)) {
            echo PHP_EOL . " registerUserArray / unRegisterUserArray / projectId is empty  " . PHP_EOL;
            return false;
        }

        $this->decidePrevisousQueue($previsousQueue);
//        $this->set_user_merge_logger($projectId, $registerArray, $unRegisterArray, func_get_args());

        $registerUserCount = count($this->_registerUserArray);
        $unRegisterUserCount = count($this->_un_registerUserArray);

        /*
         *  get projectProperty cache
         */
//        $this->_get_project_property();

        if (($registerUserCount == 1) && ($unRegisterUserCount == 1)) {
            echo PHP_EOL . " case _singleRegisterUser_single_un_registerUser_merge " . PHP_EOL;
            $this->_singleRegisterUser_single_un_registerUser_merge();

            echo PHP_EOL . " case _singleCustomRegisterUser_singleCustom_un_registerUser_merge " . PHP_EOL;
            $this->_singleCustomRegisterUser_singleCustom_un_registerUser_merge($projectId);
        } else if (($registerUserCount == 1) && ($unRegisterUserCount > 1)) {
            echo PHP_EOL . " case _singleRegisterUser_multi_un_registerUser_merge " . PHP_EOL;
            $this->_singleRegisterUser_multi_un_registerUser_merge();

            echo PHP_EOL . " case _singleCustomRegisterUser_multiCustom_un_registerUser_merge " . PHP_EOL;
            $this->_singleCustomRegisterUser_multiCustom_un_registerUser_merge($projectId);
        } else if (($registerUserCount > 1) && ($unRegisterUserCount == 1)) {
            echo PHP_EOL . " case _multiRegisterUser_single_un_registerUser_merge " . PHP_EOL;
            $this->_multiRegisterUser_single_un_registerUser_merge();

            echo PHP_EOL . " case _multiCustomRegisterUser_singleCustom_un_registerUser_merge " . PHP_EOL;
            $this->_multiCustomRegisterUser_singleCustom_un_registerUser_merge($projectId);
        } else {
            echo PHP_EOL . " case _multiRegisterUser_multi_un_registerUser_merge " . PHP_EOL;
            $this->_multiRegisterUser_multi_un_registerUser_merge();

            echo PHP_EOL . " case _multiCustomRegisterUser_multiCustom_un_registerUser_merge " . PHP_EOL;
            $this->_multiCustomRegisterUser_multiCustom_un_registerUser_merge($projectId);
        }

        /*
         *  @dev Bharat Bhushan and Hitesh
         *  @date 6 Jan 2017
         *  Nested merge process 
         *  Merge Nested property in latest register user
         */
        $this->_send_order_cart_property($registerUserCount);


        /*
         *  send DPN user 
         */
//        $this->send_dpn_user($registerArray);


        /*
         *  @dev Bharat Bhushan 
         *  @date 27 Dec 2016
         *  @desc insert user merge tracking process into Elasticsearch logger server
         */
//        $esObj = new ElasticSearch_EsBaseMergeUser();
//        $esObj->index($this->_user_merge_logger);

        $this->_reset();
        $this->_unreg_user_have_purchased_property = $this->_un_register_all_User_array = $this->_register_all_User_array = array();
    }

    protected function _singleRegisterUser_single_un_registerUser_merge() {

        $registerUserDoc = $this->_get_user_doc($this->_registerUserArray[0], true);
        if (!$this->process_message) {
            return false;
        }

        $un_registerUserDoc = $this->_get_user_doc($this->_un_registerUserArray[0]);
        if (!$this->process_message) {
            return false;
        }
        if ($un_registerUserDoc['projectId'] == $this->_projectId) {
//            $this->check_dpn_subscription_user($un_registerUserDoc, $this->_projectId);
            $second = array("user_id" => $this->_un_registerUserArray[0], "doc" => $un_registerUserDoc);
            $this->_merge_user($registerUserDoc, $second);
//            $this->_send_property_queue($this->_registerUserArray[0], $this->_user_update_array);
        }
    }

    protected function _singleRegisterUser_multi_un_registerUser_merge() {

        /*
         *  first merge un_register user to final un_register user 
         *  and then merge final un_register user  with $_un_register_user
         */
        if (count($this->_un_registerUserArray) > 1) {
            /*
             *  get first unregister user from $this->_un_registerUserArray 
             *  and consider first unregister user as a register user to convert all unregister user into single unregister user.
             */
            foreach ($this->_un_registerUserArray as $index => $first_user_id) {
                $_un_register_user = $this->_get_user_doc($first_user_id);
                if (!$this->process_message) {
                    return false;
                }
                unset($this->_un_registerUserArray[$index]);
                if ($_un_register_user['projectId'] == $this->_projectId) {
                    break;
                } else {
                    $_un_register_user = false;
                }
            }
            $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);

            /*
             *  merge all unregister user into single unregister user
             */

            foreach ($this->_un_registerUserArray as $user_id) {
                $userDoc2 = $this->_get_user_doc($user_id);
                $this->check_dpn_subscription_user($userDoc2, $this->_projectId);
                $second = array("user_id" => $user_id, "doc" => $userDoc2);
                $this->_merge_user($_un_register_user, $second);
                $_un_register_user = $this->_reset();
            }

            /*
             *  now finally merge final unregister user with register user
             */

            if (!empty($_un_register_user)) {
                $registerUserDoc = $this->_get_user_doc($this->_registerUserArray[0], true);
                if (!$this->process_message) {
                    return false;
                }

                $second = array("user_id" => $first_user_id, "doc" => $_un_register_user);
                $this->_merge_user($registerUserDoc, $second);
                $this->_send_property_queue($this->_registerUserArray[0], $this->_user_update_array);
            }
        }
    }

    protected function _multiRegisterUser_single_un_registerUser_merge() {
        /*
         *  merge $this->_un_registerUserArray user with all register user
         */

        if (count($this->_registerUserArray) > 1) {
            $second_user_id = $this->_un_registerUserArray[0];
            $_un_register_user = $this->_get_user_doc($second_user_id);
            if (!$this->process_message) {
                return false;
            }


            if ($_un_register_user['projectId'] == $this->_projectId) {
                $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);
                $second = array("user_id" => $second_user_id, "doc" => $_un_register_user);
                foreach ($this->_registerUserArray as $user_id) {
                    $register_user = $this->_get_user_doc($user_id, true);
                    $this->_merge_user($register_user, $second);
                    $this->_send_property_queue($user_id, $this->_user_update_array);
                }
            }
        }
    }

    protected function _multiRegisterUser_multi_un_registerUser_merge() {
        /*
         *  first merge all un_register user 
         *  and then merge final un_register user  with all  register_user
         */


        if (count($this->_un_registerUserArray) > 1) {
            foreach ($this->_un_registerUserArray as $index => $first_user_id) {
                $_un_register_user = $this->_get_user_doc($first_user_id);
                if (!$this->process_message) {
                    return false;
                }
                unset($this->_un_registerUserArray[$index]);
                if ($_un_register_user['projectId'] == $this->_projectId) {
                    break;
                } else {
                    $_un_register_user = false;
                }
            }
            if (empty($_un_register_user)) {
                return false;
            }

            $this->check_dpn_subscription_user($_un_register_user, $this->_projectId);

            foreach ($this->_un_registerUserArray as $user_id) {
                $userDoc2 = $this->_get_user_doc($user_id);
                if ($userDoc2['projectId'] != $this->_projectId) {
                    continue;
                }
                $this->check_dpn_subscription_user($userDoc2, $this->_projectId);
                $second = array("user_id" => $user_id, "doc" => $userDoc2);
                $this->_merge_user($_un_register_user, $second);
                $_un_register_user = $this->_reset();
            }
        }


        if (count($this->_registerUserArray) > 1) {
            foreach ($this->_registerUserArray as $user_id) {
                $register_user = $this->_get_user_doc($user_id, true);
                $second = array("user_id" => $first_user_id, "doc" => $_un_register_user);
                $this->_merge_user($register_user, $second);
                $this->_send_property_queue($user_id, $this->_user_update_array);
            }
        }
    }

    protected function _merge_user($firstUser, $secondUser) {
        if (!$this->process_message) {
            return false;
        }
        if (!empty($firstUser) && !empty($secondUser['doc'])) {
            $this->_first_array = $firstUser;
            $this->_second_array = $secondUser['doc'];
            /*
             *  merge $this->_second_array to $this->_first_array
             *  get all propertyId from $this->_second_array and set into $this->_first_array if not exist and if updated type.
             */
            $this->_identifier();
            $this->_process_fixed_property();
            $this->_other_plain_property();
//            $this->_nested_property();

            $this->_merge_with_user[] = $secondUser['user_id'];
            $this->_merged_in_user[] = isset($firstUser['_userId']) ? $firstUser['_userId'] : $firstUser['userId'];
        }
    }

    protected function _identifier() {
        /*
         *  according to arjun sir push the identiifer value of $this->_second_array into $this->_first_array if not exist into $this->_first_array array.
         *  if both user exist same key then consider only $this->_first_array.
         */

        $identifierArr = array("customer_id", "email", "phone", "device_id");
        foreach ($identifierArr as $propertyName) {

            if (!isset($this->_first_array[$propertyName]) && (isset($this->_second_array[$propertyName]))) {
                $this->_first_array[$propertyName] = $this->_second_array[$propertyName];
                $propertyValue = is_array($this->_second_array[$propertyName]) ? $this->_second_array[$propertyName][0] : $this->_second_array[$propertyName];

                if (!empty($propertyValue)) {
                    $this->_plain_property_set("update", 'text', $propertyName, $propertyValue);
                }
            }
            $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
        }
    }

    protected function _process_fixed_property() {
        /*
         *  we take  first time insert value
         *  so we should compare both array value.
         */

        foreach ($this->_fixed_plain_property_array as $pn_array) {
            $pn = $pn_array['pn'];
            $for = $pn_array['for'];
            $propertyType = ElasticSearch_PropertySuffix::_get_property_type($propertyName);
            echo "\n" . $propertyType . "\n";
            if (!empty($pn)) {
                if (isset($this->_second_array[$pn])) {
                    if (isset($this->_first_array[$pn])) {
                        if ($propertyType == "xs:date") {
                            $set_value = $this->_select_date($this->_first_array[$pn], $this->_second_array[$pn], $for);
                            if ($pn != 'first_purchase_date') {
                                $this->_acquisition_selected = ($this->_first_array[$pn] == $set_value) ? "first_array_selected" : "second_array_selected";  // its for  acquisition_source for second iteration of  $this->_fixed_plain_property_array 
                                if ($this->_acquisition_selected == "first_array_selected") {
                                    $this->_second_array = $this->_unset_filed($this->_second_array, $pn);
                                    continue; // because user have also this property.
                                }
                            } else {
                                if ($this->_first_array[$pn] == $set_value) {
                                    $this->_second_array = $this->_unset_filed($this->_second_array, $pn);
                                    continue; // because user have also this property.
                                }
                            }
                            $case = "update";
                        } else if (($pn == "acquisition_source") || ($pn == "acquisition_medium") || ($pn == "acquisition_campaign")) {
                            /*
                             *   acquisition_source, acquisition_medium and acquisition_campaign is depands on first_seen_date of user.
                             *   in case of "acquisition_source", "acquisition_medium" and "acquisition_campaign" we only take acquisition_* value  whose first_seen_date is earlier.
                             */
                            if (!empty($this->_acquisition_selected)) {
                                if (($this->_acquisition_selected == 'second_array_selected') && ($this->_second_array[$pn] != $this->_first_array[$pn])) {
                                    $set_value = $this->_second_array[$pn];
                                } else {
                                    $this->_second_array = $this->_unset_filed($this->_second_array, $pn);
                                    continue;
                                }
                            }
                            $case = "update";
                        }
                    } else {
                        $this->_first_array[$pn] = $this->_second_array[$pn];
                        $set_value = $this->_second_array[$pn];
                        $case = "update";
                    }
                    $this->_plain_property_set($case, $propertyType, $pn, $set_value);
                    $this->_second_array = $this->_unset_filed($this->_second_array, $pn);
                }
            }
        }
        $this->_acquisition_selected = false;
    }

    protected function _other_plain_property() {
        if (!empty($this->_second_array)) {
            foreach ($this->_second_array as $propertyName => $propertyValue) {
                if (empty($propertyValue)) {
                    $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
                    continue;
                }

                /*
                 * check unregister user have purchased property or not 
                 * for merge unregister user order to register user.
                 */
                $propertyType = ElasticSearch_PropertySuffix::_get_property_type($propertyName);
                if ($propertyName == 'purchased_i') {
                    $this->_unreg_user_have_purchased_property[] = $this->_second_array['_userId'];
                }

                if (in_array($propertyName, $this->_ignore_property)) {
                    $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
                    continue;
                }

                if (!empty($propertyName)) {
                    $case = '';
                    if (!isset($this->_first_array[$propertyName])) {
                        if (($propertyType == "number") || !empty($this->_second_array[$propertyName])) {
                            $addValue = $this->_first_array[$propertyName] = $this->_second_array[$propertyName];
                            $case = "update";
                        }
                    } else {
                        if (($propertyType == "text") && !empty($this->_second_array[$propertyName])) {
                            if (in_array($propertyName, $this->_ignore_text_property)) {
                                $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
                                continue;
                            }

                            $result = $this->_combine_array_value($this->_first_array[$propertyName], $this->_second_array[$propertyName]);
                            $this->_first_array[$propertyName] = $result["final_array"];
                            if (empty($result["update_arr"])) {
                                continue;
                            }
                            $addValue = $result["update_arr"];
                            $case = "add";
                        } else if ($propertyType == "xs:date") {
                            $addValue = $this->_select_date($this->_first_array[$propertyName], $this->_second_array[$propertyName]);
                            if ($this->_first_array[$propertyName] == $addValue) {
                                continue; // because user _first_array have also this property.
                            }
                            $case = "update";
                            $this->_first_array[$propertyName] = $addValue;
                        } else if (($propertyType == "number") && !empty($this->_second_array[$propertyName])) {
                            $this->_first_array[$propertyName] = ((int) $this->_first_array[$propertyName] + (int) $this->_second_array[$propertyName]);
                            $addValue = (int) $this->_second_array[$propertyName];
                            $case = "increment";
                        }
                    }
                    if (!empty($case)) {
                        $this->_plain_property_set($case, $propertyType, $propertyName, $addValue);
                    }
                }
                $this->_second_array = $this->_unset_filed($this->_second_array, $propertyName);
            }
        }
    }

    protected function _send_property_queue($userId, $property_array) {
        if (!$this->process_message) {
            return false;
        }

        if (isset($property_array['plain'])) {
            $shardId = Identifier_Utility::getShardId($userId, $this->_projectId);
            $plain_property = $this->_user_update_array['plain'];

            $final_arr = array();
            $flag = true;

            foreach ($plain_property as $case => $array) {
                if ($flag && ($case == "add")) {
                    $flag = false;
                    $array["merge_with_user"] = array("value" => $this->_merge_with_user, "type" => "text");
                }
                $final_arr[] = $this->_set_sharding_array($userId, $shardId, $this->_projectId, "plain", $case, $array);
            }
            if ($flag) {
                $array = array("merge_with_user" => array("value" => $this->_merge_with_user, "type" => "text"));
                $final_arr[] = $this->_set_sharding_array($userId, $shardId, $this->_projectId, "plain", "add", $array);
            }
            echo "\n \n \n =================== final array of  plain property =========    \n \n";
            echo Utility::json_encode(array($final_arr));

            $this->_user_merge_logger['finalArrayOfPlainProperty'] = Utility::json_encode(array($final_arr));
            if (!empty($final_arr)) {
                Utility::_add_to_shard_queueing_system(array($final_arr), $shardId, $this->_projectId);
            }
        } else {
            echo "\n ========================== user have not any property for merge but user should be merge ========================== \n ";
        }


        echo "\n ========================== _merge_with_user ========================== \n ";
        print_r($this->_merge_with_user);
        echo "\n ========================== _merged_in_user ========================== \n ";
        print_r($this->_merged_in_user);


        echo "\n \n ";

        $merge_with_user_logger = array();

        if (!empty($this->_merge_with_user)) {
            foreach ($this->_merge_with_user as $userId) {
                $shardId = Identifier_Utility::getShardId($userId, $this->_projectId);
                $current_date = gmdate("Y-m-d H:i:s");
                $array = array("projectId" => array("value" => ES_INACTIVE_PROJECT_ID, "type" => "number"),
                    "user_merge_date" => array("value" => str_replace(" ", "T", $current_date), "type" => "xs:date")
                );
                $_merge_with_user[] = $this->_set_sharding_array($userId, $shardId, $this->_projectId, "plain", "update", $array);
                echo "\n _merge_with_user===" . Utility::json_encode(array($_merge_with_user));
                echo PHP_EOL;
                Utility::_add_to_shard_queueing_system(array($_merge_with_user), $shardId, $this->_projectId);

                $merge_with_user_logger[] = $_merge_with_user;

                $_merge_with_user = false;
            }
            $this->_user_merge_logger['merge_with_user'] = Utility::json_encode($merge_with_user_logger);
            $this->_merge_with_user = array();
        }

        if ($this->_unregister_send_message_to_delete) {
            echo "\n ========================== unregister user array send to IdentifierScrubber_Remove  ========================== \n ";
            $unRegArray = $this->_message['unRegisterUserArray'];
            if (!empty($unRegArray) && is_array($unRegArray)) {
                $this->_user_merge_logger['unRegUserDeleteFromMongo'] = Utility::json_encode($unRegArray);

                $obj = new IdentifierScrubber_Remove($this->_projectId, true, true);
                $obj->_delete($unRegArray);
            }
            $this->_unregister_send_message_to_delete = false;
        }
    }

    protected function _get_user_doc($userId, $registerUser = false) {
        $start = microtime(true);
        if (!$this->process_message) {
            return false;
        }
        $result = false;


        $conditon = array(array("match" => array("_id" => $userId)));
        if (!empty($userId)) {
            $user_query = array("query" => array("bool" => array("must" => $conditon)));
//            echo "\n json queryto find user = " . Utility::json_encode($user_query);
            if ($registerUser) {
                $json = '{
   "took": 14,
   "timed_out": false,
   "_shards": {
      "total": 20,
      "successful": 20,
      "failed": 0
   },
   "hits": {
      "total": 1,
      "max_score": 13.367785,
      "hits": [
         {
            "_index": "urza_5_6",
            "_type": "user",
            "_id": "590b29338d8fb54760a9b7c7",
            "_score": 13.367785,
            "_routing": "590b29338d8fb54760a9b7c7",
            "_source": {
               "projectId": 31801,
               "_userId": "590b29338d8fb54760a9b7c7",
               "case": "insert",
               "_shardId": 8,
               "@timestamp": "2017-09-11T13:25:27",
               "customer_id": [
                  "396797"
               ],
               "email": [
                  "as5492@gmail.com"
               ],
               "phone": [
                  "9870541786"
               ],
               "firstseen_date": {
                  "day": "04",
                  "month": "05",
                  "year": "2017",
                  "day_month": "04-05",
                  "year_month_day": "2017-05-04",
                  "timestamp": "2017-05-04T13:04:56",
                  "weekday": "thursday"
               },
               "last_session_start_date": {
                  "day": "05",
                  "month": "09",
                  "year": "2017",
                  "day_month": "05-09",
                  "year_month_day": "2017-09-05",
                  "timestamp": "2017-09-05T08:34:08",
                  "weekday": "tuesday"
               },
               "last_session_process_date": {
                  "day": "05",
                  "month": "09",
                  "year": "2017",
                  "day_month": "05-09",
                  "year_month_day": "2017-09-05",
                  "timestamp": "2017-09-05T08:34:08",
                  "weekday": "tuesday"
               },
               "lifecycleid": 59339,
               "between_13_14_gmt_i": 2,
               "session_on_thursday_i": 3,
               "avg_order_value_i": 1860.5,
               "highest_order_value_i": 2023,
               "usertag": [
                  "customer"
               ],
               "last_purchase_date": {
                  "day": "12",
                  "month": "07",
                  "year": "2017",
                  "day_month": "12-07",
                  "year_month_day": "2017-07-12",
                  "timestamp": "2017-07-12T07:29:31",
                  "weekday": "wednesday"
               },
               "first_purchase_date": {
                  "day": "04",
                  "month": "05",
                  "year": "2017",
                  "day_month": "04-05",
                  "year_month_day": "2017-05-04",
                  "timestamp": "2017-05-04T13:12:21",
                  "weekday": "thursday"
               },
               "time_to_1st_purchased_i": 0,
               "total_revenue_i": 3721,
               "purchased_i": 2,
               "orders_withoutpromo_count_i": 2,
               "orders_withpromo_count_i": 0,
               "best_hour_i": 13,
               "best_day": "thursday",
               "segmentid": [
                  "59511",
                  "66570",
                  "66647",
                  "67848",
                  "67850"
               ],
               "acquisition_medium": [
                  "direct"
               ],
               "acquisition_source": [
                  "direct"
               ],
               "acquisition_campaign": [
                  "direct"
               ],
               "number_of_session_i": 2,
               "country": [
                  "India"
               ],
               "countrycode": [
                  "IN"
               ],
               "city": [
                  "Mumbai"
               ],
               "continent": [
                  "Asia"
               ],
               "region": [
                  "Maharashtra"
               ],
               "timezone": [
                  "Asia/Kolkata"
               ],
               "firstname": [
                  "Ankita sharma"
               ],
               "between_12_13_gmt_i": 1,
               "session_on_saturday_i": 2,
               "between_4_5_gmt_i": 1,
               "between_3_4_gmt_i": 1,
               "session_on_monday_i": 1,
               "gender": [
                  "female"
               ],
               "between_7_8_gmt_i": 1,
               "session_on_wednesday_i": 2,
               "between_5_6_gmt_i": 1,
               "between_10_11_gmt_i": 1,
               "between_6_7_gmt_i": 1,
               "session_on_tuesday_i": 1,
               "userId": "590b29338d8fb54760a9b7c7"
            }
         }
      ]
   }
}';
            } else {
                $json = '{
   "took": 93,
   "timed_out": false,
   "_shards": {
      "total": 20,
      "successful": 20,
      "failed": 0
   },
   "hits": {
      "total": 1,
      "max_score": 13.55252,
      "hits": [
         {
            "_index": "urza_5_6",
            "_type": "user",
            "_id": "59679ab68d8fb54760d690a7",
            "_score": 13.55252,
            "_routing": "59679ab68d8fb54760d690a7",
            "_source": {
               "projectId": 31801,
               "_userId": "59679ab68d8fb54760d690a7",
               "case": "insert",
               "_shardId": 19,
               "@timestamp": "2017-09-05T08:53:00",
               "customer_id": [
                  "443007"
               ],
               "email": [
                  "jilani69@gmail.com"
               ],
               "firstseen_date": {
                  "day": "13",
                  "month": "07",
                  "year": "2017",
                  "day_month": "13-07",
                  "year_month_day": "2017-07-13",
                  "timestamp": "2017-07-13T16:07:14",
                  "weekday": "thursday"
               },
               "last_session_start_date": {
                  "day": "05",
                  "month": "09",
                  "year": "2017",
                  "day_month": "05-09",
                  "year_month_day": "2017-09-05",
                  "timestamp": "2017-09-05T08:53:00",
                  "weekday": "tuesday"
               },
               "last_session_process_date": {
                  "day": "05",
                  "month": "09",
                  "year": "2017",
                  "day_month": "05-09",
                  "year_month_day": "2017-09-05",
                  "timestamp": "2017-09-05T08:53:00",
                  "weekday": "tuesday"
               },
               "lifecycleid": 59335,
               "usertag": [
                  "customer"
               ],
               "segmentid": [
                  "59511",
                  "66647",
                  "67848",
                  "67850"
               ],
               "between_16_17_gmt_i": 1,
               "session_on_thursday_i": 3,
               "best_hour_i": 6,
               "best_day": "thursday",
               "number_of_session_i": 3,
               "acquisition_medium": [
                  "organic"
               ],
               "acquisition_source": [
                  "google"
               ],
               "acquisition_campaign": [
                  "google"
               ],
               "country": [
                  "India"
               ],
               "countrycode": [
                  "IN"
               ],
               "city": [
                  "Bekobar"
               ],
               "continent": [
                  "Asia"
               ],
               "region": [
                  "Jharkhand"
               ],
               "timezone": [
                  "Asia/Kolkata"
               ],
               "between_18_19_gmt_i": 1,
               "between_22_23_gmt_i": 1,
               "between_5_6_gmt_i": 1,
               "session_on_friday_i": 1,
               "phone": [
                  "9199521996"
               ],
               "firstname": [
                  "Jilani Ansari"
               ],
               "avg_order_value_i": 996.55,
               "highest_order_value_i": 996.55,
               "last_purchase_date": {
                  "day": "14",
                  "month": "07",
                  "year": "2017",
                  "day_month": "14-07",
                  "year_month_day": "2017-07-14",
                  "timestamp": "2017-07-14T06:02:17",
                  "weekday": "friday"
               },
               "first_purchase_date": {
                  "day": "14",
                  "month": "07",
                  "year": "2017",
                  "day_month": "14-07",
                  "year_month_day": "2017-07-14",
                  "timestamp": "2017-07-14T06:02:17",
                  "weekday": "friday"
               },
               "time_to_1st_purchased_i": 0,
               "total_revenue_i": 996.55,
               "purchased_i": 1,
               "orders_withpromo_count_i": 1,
               "orders_withoutpromo_count_i": 0,
               "between_10_11_gmt_i": 1,
               "session_on_saturday_i": 3,
               "between_11_12_gmt_i": 2,
               "between_15_16_gmt_i": 1,
               "between_9_10_gmt_i": 1,
               "session_on_sunday_i": 2,
               "between_6_7_gmt_i": 2,
               "session_on_monday_i": 1,
               "session_on_tuesday_i": 1,
               "userId": "59679ab68d8fb54760d690a7",
               "last_365_days_purchased_i": 1,
               "first_purchase_utm_info": {
                  "campaignType": "organic",
                  "utm_campaign": "google",
                  "universalCampaignId": "126847",
                  "utm_medium": "organic",
                  "utm_source": "google"
               },
               "last_180_days_gmv_i": 996.55,
               "last_365_days_discounted_order_i": 1,
               "best_hour_to_purchase_i": "06",
               "last_180_days_purchased_i": 1,
               "lowest_order_value_i": 996.55,
               "last_90_days_discounted_order_i": 1,
               "avg_discount_percentage_last180_days_purchased_i": 100,
               "segmentation_last_process_date": {
                  "day_month": "22-09",
                  "year_month_day": "2017-09-22",
                  "month": "09",
                  "year": "2017",
                  "weekday": "friday",
                  "day": "22",
                  "timestamp": "2017-09-22T21:29:42"
               },
               "avg_revenue_per_month_i": 450,
               "best_channel_to_purchase": [
                  "organic"
               ],
               "total_discount_percentage_i": 5.2631578947368,
               "best_source_to_purchase": [
                  "google"
               ],
               "last_90_days_gmv_i": 996.55,
               "best_day_to_purchase": [
                  "friday"
               ],
               "time_since_last_purchase_i": "69",
               "avg_discount_percentage_last90_days_purchased_i": 100,
               "last_365_days_gmv_i": 996.55,
               "time_between_purchases_i": 0,
               "last_90_days_purchased_i": 1,
               "first_purchase_value_i": 996.55,
               "total_discount_i": 52.45,
               "avg_discount_percentage_last365_days_purchased_i": 100,
               "last_180_days_discounted_order_i": 1,
               "session_conversion_i": 0.33,
               "user_percentile_last_90_days_gmv": [
                  "0-10"
               ],
               "user_percentile_last_180_days_gmv": [
                  "0-10"
               ],
               "user_percentile_last_365_days_gmv": [
                  "0-10"
               ]
            }
         }
      ]
   }
}';
            }
            $user_result = json_decode($json, true);
            if (!empty($user_result["hits"]["hits"])) {
                $result = $user_result["hits"]["hits"][0]['_source'];
            }
        }
        return $result;
    }

    public function decidePrevisousQueue($previsousQueue) {
        $nexQueueName = 'user_merge_unprocess_slower';
        if ($previsousQueue) {
            if ($previsousQueue == 'user_merge_unprocess_slower') {
                $nexQueueName = 'user_merge_unprocess_slowest';
            } else if ($previsousQueue == 'user_merge_unprocess_slowest') {
                $nexQueueName = 'user_merge_unprocess';
            }
        }
        $this->_message['previsousQueue'] = $nexQueueName;
    }

    public function _send_order_cart_property($registerUserCount) {
        try {
            if ($registerUserCount == 1) {
                $registerUserId = $this->_register_all_User_array[0];
            } else {
                $registerUserId = $this->_findLatestRegisterUser();
            }
            echo "<br/><br/>\n \n\n\n\n\n\n\n\n\n\n\n==========_unreg_user_have_purchased_property==================\n<br/>";
            print_r($this->_unreg_user_have_purchased_property);

            /*
             *   send user to update cart and order of unregister user to register user
             */

            foreach ($this->_un_register_all_User_array as $unregUserId) {
                if (in_array($unregUserId, $this->_unreg_user_have_purchased_property)) {
                    $message = array("projectId" => $this->_projectId, "regId" => $registerUserId, "unregId" => $unregUserId, 'case' => 'ordercart');
                } else {
                    $message = array("projectId" => $this->_projectId, "regId" => false, "unregId" => $unregUserId, 'case' => 'cart');
                }
                echo "<br/><br/><br/><br/>";
                print_r($message);
//                $queuObj = new RabbitMQ_UserMergeOrderCart();
//                $queuObj->_produce($message);
            }

            /*
             *  update user event in Mongo  [user_event_relation]
             *  and user campaign relation in Mongo [user_uc_relation]
             */
//            $message = array("projectId" => $this->_projectId, "regId" => $registerUserId, "unregId" => $this->_un_register_all_User_array, 'case' => 'userevent');
//            $queuObj = new RabbitMQ_UserMergeOrderCart();
//            $queuObj->_produce($message);

            /*
             *  update user campaign last 24hour cache [key => "last24_30007_56cefde07b685163c58efe36"]
             *  insert into Mongo [campaign_last_24hour]
             */
//            $this->_set_user_campaign($registerUserId);

            /*
             *  calculate all register user all property via server24
             */
//            foreach ($this->_register_all_User_array as $regUserId) {
//                $message = array("projectId" => $this->_projectId, "regId" => $regUserId, "unregId" => false, 'case' => 'server24');
//                $queuObj = new RabbitMQ_UserMergeOrderCart();
//                $queuObj->_produce($message);
//            }
        } catch (Exception $ex) {
            
        }
    }
    protected function _findLatestRegisterUser() {
        $registerUserArray = $this->_register_all_User_array;
        $latestUserId = $registerUserArray[0];

        unset($registerUserArray[0]);
        foreach ($registerUserArray as $userId) {
            $returnOutput = $this->_getTimestamp($latestUserId, $userId);
            if ($returnOutput == 'second') {
                $latestUserId = $userId;
            }
        }
        return $latestUserId;
    }

}
