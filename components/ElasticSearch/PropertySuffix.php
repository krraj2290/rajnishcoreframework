<?php

/**
 *
 * @author Rajnish
 * @date 12 June 2017
 */
class ElasticSearch_PropertySuffix {

    public static $_number = '_i';
    public static $_date = '_date';
    public static $_text = '';

    public static function _final_property_name($propertyName, $propertyType) {
        if (!empty($propertyName) && !empty($propertyType)) {
            $propertyName = strtolower($propertyName);
            if ($propertyType == 'number') {
                if (preg_match('/_i$/', $propertyName)) {
                    return $propertyName;
                } else {
                    if ($propertyName == 'lifecycleid') {
                        return $propertyName;
                    } else {
                        return $propertyName . "" . self::$_number;
                    }
                }
            } else if (($propertyType == 'sd:date') || ($propertyType == 'xs:date')) {
                if (preg_match('/_date$/', $propertyName)) {
                    return $propertyName;
                } else if (preg_match('/_sdate$/', $propertyName)) {
                    return preg_replace('/_sdate$/', '_date', $propertyName);
                } else {
                    return $propertyName . "" . self::$_date;
                }
            } else {
                return $propertyName;
            }
        }
        return '';
    }

    static function _sdateFormate($date) {

        if (is_numeric($date)) {
            $unixTimestamp = $date;
        } else {
            $unixTimestamp = strtotime($date);
        }
        $returnArray = array();
        $returnArray['day'] = gmdate("d", $unixTimestamp);
        $returnArray['month'] = gmdate("m", $unixTimestamp);
        $returnArray['year'] = gmdate("Y", $unixTimestamp);
        $returnArray['day_month'] = gmdate("d-m", $unixTimestamp);
        $returnArray['year_month_day'] = gmdate("Y-m-d", $unixTimestamp);
        $returnArray['timestamp'] = str_replace(" ", "T", gmdate("Y-m-d H:i:s", $unixTimestamp));
        $returnArray['weekday'] = strtolower(gmdate("l", $unixTimestamp));

        return $returnArray;
    }

    static function _convert_into_date($document) {
        foreach ($document as $fieldName => $fieldValue) {
            $document[$fieldName] = self::_convert_according_to_property_type($fieldName, $fieldValue);
        }
        return $document;
    }

    public static function _get_property_type($propertyName) {
        if (!empty($propertyName)) {
            $propertyName = strtolower($propertyName);
            if (preg_match('/_i$/', $propertyName)) {
                return 'number';
            } else if (preg_match('/_date$/', $propertyName) || preg_match('/_sdate$/', $propertyName)) {
                return 'xs:date';
            } else {
                return "text";
            }
        }
        return "text";
    }

    static function _convert_property_into_nested($document) {
        $nested = array();
        foreach ($document as $fieldName => $fieldValue) {
            $fieldName = strtolower($fieldName);
            if (preg_match('/_date$/', $fieldName) || (preg_match('/_sdate$/', $fieldName))) {
                $nested[] = array("name" => "$fieldName", "value_date" => self::_sdateFormate($fieldValue));
            } else if (preg_match('/_i$/', $fieldName)) {
                $nested[] = array("name" => "$fieldName", "value_number" => (float) $fieldValue);
            } else {
                if (is_array($fieldValue)) {
                    $text = $fieldValue;
                } else {
                    $text = array($fieldValue);
                }

                $nested[] = array("name" => "$fieldName", "value_text" => $text);
            }
        }
        return $nested;
    }

    public static function _get_property_event_type($propertyName) {
        if (!empty($propertyName)) {
            $propertyName = strtolower($propertyName);
            if (preg_match('/_sent$/', $propertyName)) {
                return 'sent';
            } else if (preg_match('/_deliver$/', $propertyName) || preg_match('/_delivered$/', $propertyName)) {
                return 'delivered';
            } else {
                return $propertyName;
            }
        }
        return $propertyName;
    }

    static function _convert_according_to_property_type($fieldName, $fieldValue) {

        $fieldName = strtolower($fieldName);
        if (preg_match('/_date$/', $fieldName)) {
            return self::_sdateFormate($fieldValue);
        } else if (preg_match('/_sdate$/', $fieldName)) {
            return self::_sdateFormate($fieldValue);
        } else if (preg_match('/_i$/', $fieldName)) {
            return floatval($fieldValue);
        } else {
            if (is_array($fieldValue)) {
                return $fieldValue;
            } else {
                return array($fieldValue);
            }
        }
    }

}
