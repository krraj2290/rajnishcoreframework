<?php

/**
 * @author Rajnish Kr Rajesh <krraj2290@gmail.com>
 * @desc
 */
class ElasticSearch_EsBase {

    public $_connection = false;
    protected $_projectId;
    protected $_ES_INDEX;
    protected $_ENTERPRISE_CLIENT_ARRAY;
    protected $_ES_SEARCH_PRIVATE_URL;
    protected $_ES_PRIVATE_URL;
    protected $_connection_tag = 'default';
    protected $_connection_type = 'master';
    protected static $_connection_obj = false;

    public function __construct($projectId, $type, $connection = 'slave', $testIndex = false) {
        $this->_projectId = $projectId;
//        $this->_ENTERPRISE_CLIENT_ARRAY = unserialize(ENTERPRISE_CLIENT_ARRAY);
        $this->_ES_INDEX = 'urza';
        $this->_ES_SEARCH_PRIVATE_URL = 'localhost';
        $this->_ES_PRIVATE_URL = 'localhost';
        $this->_ES_document_type = $type;
        $this->_connection_type = $connection;
//        $this->check_enterprise();
        if (!empty($testIndex)) {
            $this->_ES_INDEX = $testIndex;
        }
        $this->_get_connection_obj();
    }

    function index($attribute, $attributeIndex = false, $option = array()) {
        $statsdAseObj = new BOStatsd_Esi();
        $statsd = $statsdAseObj->_startTiming();
//        $attribute = array_filter($attribute);
        $res = $this->_connection->index($attribute, $attributeIndex, $option);
        $statsdAseObj->_endTiming($statsd);
        return $res;
    }

    function source($option = array()) {
//        $statsdAseObj = new BOStatsd_Ess();
//        $statsd = $statsdAseObj->_startTiming();
        $res = $this->_connection->source($option);
//        $statsdAseObj->_endTiming($statsd);
        return $res;
    }

    function search($query, $option = array()) {

//        describe( "class variables::", $this );

        $res = $this->_connection->search($query, $option);
        return $res;
    }

    public function scrollData($scroll_id) {
        //please don't put this in a try catch block  
        $res = false;
        if ($scroll_id) {
            $baseUri = 'http://' . $this->_ES_SEARCH_PRIVATE_URL . '/' . "_search/scroll?scroll=2m&scroll_id=$scroll_id";
            $res = $this->_make_request($baseUri, false);
        }
        return $res;
    }

    function master() {

        self::$_connection_obj[$this->_connection_type][$this->_connection_tag][$this->_ES_document_type] = \ElasticSearch\Client::connection(array(
                    'servers' => $this->_ES_PRIVATE_URL,
                    'protocol' => 'http',
                    'type' => $this->_ES_document_type,
                    'index' => $this->_ES_INDEX,
        ));
    }

    public function _destroy_connection() {
//        self::$_connection_obj = false;
    }

    public function _delete($id) {
        try {
            $this->_connection->delete($id);
        } catch (Exception $ex) {
            $this->_log($ex->getMessage() . PHP_EOL . $ex->getTraceAsString());
        }
    }

    public function deleteByQuery() {
        if (!empty($this->_projectId)) {
            $query = "projectId:" . $this->_projectId;
            $this->_connection->deleteByQuery("$query");
        }
    }

    function _get_connection_obj() {
        if (!isset(self::$_connection_obj[$this->_connection_type][$this->_connection_tag][$this->_ES_document_type])) {
            $connection = $this->_connection_type;
            $this->$connection();
        } else {
            
        }
        $this->_connection = self::$_connection_obj[$this->_connection_type][$this->_connection_tag][$this->_ES_document_type];
    }

    function slave() {

        self::$_connection_obj[$this->_connection_type][$this->_connection_tag][$this->_ES_document_type] = \ElasticSearch\Client::connection(array(
                    'servers' => $this->_ES_SEARCH_PRIVATE_URL,
                    'protocol' => 'http',
                    'type' => $this->_ES_document_type,
                    'index' => $this->_ES_INDEX,
        ));
        return $this->_connection;
    }

    private function check_enterprise() {
        try {

            if (in_array($this->_projectId, $this->_ENTERPRISE_CLIENT_ARRAY)) {
                $this->_connection_tag = $this->_projectId;
                $this->_ES_INDEX = constant('P_' . $this->_projectId . '_INDEX');
                $this->_ES_SEARCH_PRIVATE_URL = constant('P_' . $this->_projectId . '_SEARCH_PRIVATE_URL');
                $this->_ES_PRIVATE_URL = constant('P_' . $this->_projectId . '_PRIVATE_URL');
            }
        } catch (Exception $ex) {
            $this->_log($ex->getMessage() . PHP_EOL . $ex->getTraceAsString());
        }
    }

    public function _bulk($json_doc, $type = 'user', $search_port = '9200') {
        $statsdObj = new BOStatsd_Statsd('betaout.bulk_user', 'es1');
        $statsd = $statsdObj->_startTiming();
        $baseUri = 'http://' . $this->_ES_PRIVATE_URL . '/' . $this->_ES_INDEX . '/' . $type . '/' . '_bulk';

//        $res = $this->_make_binary_shell($baseUri, $json_doc);
        $res = $this->_make_request($baseUri, $json_doc, $search_port);
        $statsdObj->_endTiming($statsd);
        return $res;
    }

    public function get_user_order($baseUri, $query) {
        if (!empty($baseUri) && !empty($query)) {
            $res = $this->_get_request($baseUri, $query);
            return $res;
        }
    }

    static $_curl;

    function _make_request($baseUri, $json_doc, $search_port = '9200') {
        if (!self::$_curl) {
            self::$_curl = $ci = curl_init();
        } else {
            $ci = self::$_curl;
        }

        curl_setopt($ci, CURLOPT_URL, $baseUri);
        curl_setopt($ci, CURLOPT_PORT, $search_port);
        curl_setopt($ci, CURLOPT_TIMEOUT, 200);
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ci, CURLOPT_FORBID_REUSE, 0);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ci, CURLOPT_POSTFIELDS, $json_doc);
//        curl_setopt($ci, CURLOPT_VERBOSE, 1);
//        curl_setopt($ci, CURLOPT_STDERR, $fp);

        $response = curl_exec($ci);

        $errno = curl_errno($ci);
        if ($errno > 0) {
            switch ($errno) {
                case CURLE_UNSUPPORTED_PROTOCOL:
                    $error = "Unsupported protocol [$protocol]";
                    break;
                case CURLE_FAILED_INIT:
                    $error = "Internal cUrl error?";
                    break;
                case CURLE_URL_MALFORMAT:
                    $error = "Malformed URL [$requestURL] -d " . Utility::json_encode($payload);
                    break;
                case CURLE_COULDNT_RESOLVE_PROXY:
                    $error = "Couldnt resolve proxy";
                    break;
                case CURLE_COULDNT_RESOLVE_HOST:
                    $error = "Couldnt resolve host";
                    break;
                case CURLE_COULDNT_CONNECT:
                    $error = "Couldnt connect to host [{$this->host}], ElasticSearch down?";

                    break;
                case CURLE_OPERATION_TIMEDOUT:
                    $error = "Operation timed out on [$requestURL]";
                    break;
                default:
                    $error = "Unknown error";
                    if ($errno == 0)
                        $error .= ". Non-cUrl error";
                    break;
            }
            throw new Exception($error);
        }


        $curl_error = curl_error($ci);

        $memory_limit = ini_get("memory_limit");
//        var_dump($memory_limit);

        ini_set("memory_limit", -1);
        $memory_limit = ini_get("memory_limit");

//        var_dump($memory_limit);
        return $res = json_decode($response, true);
    }

    public function _get_request($baseUri, $query) {
        $ci = curl_init();
        curl_setopt($ci, CURLOPT_URL, $baseUri);
        curl_setopt($ci, CURLOPT_PORT, 9200);
        curl_setopt($ci, CURLOPT_POSTFIELDS, Utility::json_encode($query));
        curl_setopt($ci, CURLOPT_TIMEOUT, 200);
        curl_setopt($ci, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ci, CURLOPT_FORBID_REUSE, 0);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ci);
        curl_close($ci);
        $res = json_decode($response, true);
        return $res;
    }

    function _make_binary_shell($baseUri, $json_doc) {
        $pid = getmypid();
        $filename = $pid . time();
        $filepath = "/tmp/$filename" . "request.json";
        $fp = fopen($filepath, 'w+');
        fwrite($fp, $json_doc);
        fclose($fp);
//        echo "curl -s _XPOST $baseUri --data-binary @$filepath;echo";
//        shell_exec("cat $filepath");
        $res = shell_exec("curl -s _XPOST $baseUri --data-binary @$filepath;echo");
        unlink($filepath);
        return $res;
    }

    public function testConnection() {

        $ch = curl_init();
        $url = 'http://' . $this->_ES_PRIVATE_URL . '/_cluster/health?pretty=true';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $res;
        if ($response) {
            $res = true;
        } else {
            $res = false;
        }
        curl_close($ch);
        return $res;
    }

}
