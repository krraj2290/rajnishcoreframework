<?php

/**
 * Print anything and can take multiple arguments
 *
 */
function describe() {
    $arguments = func_get_args();
    if (count($arguments) == 0) {
        describe('Exit Call Handled . . .');
        exit;
    }
    foreach ($arguments as $index => $thisVariable) {
        if ($index == count($arguments) - 1 && count($arguments) != 1 && is_bool($thisVariable)) {
            break;
        }
        if (is_string($thisVariable)) {
            echo "\r\n<pre>" . $thisVariable . '</pre>';
        } else if (is_bool($thisVariable)) {
            echo "\r\n<pre>";
            var_dump($thisVariable);
            echo '</pre>';
        } else {
            echo "\r\n<pre>";
            print_r($thisVariable);
            echo '</pre>';
        }
        flush();
    }
    if ($arguments[count($arguments) - 1] === true && count($arguments) != 1) {
        describe('Exit Call Handled . . .');
        exit;
    }
}

/**
 * Pagination function
 * @param $pagePath(optional), $currentPageNumber(optional), $totalPages(optional)
 * @return array
 */
function getPaging($pagePath = '/nv/page/', $currentPageNumber = 1, $totalPages = 1) {
    $temp_paging = array();
    if ($totalPages > 1) {
        $p_start = 1;
        if ($currentPageNumber > 5) {
            $p_start = $currentPageNumber - 5;
        }
        $p_end = $p_start + 9;
        if ($p_end > $totalPages) {
            $p_end = $totalPages;
        }
        $i = 0;
        if ($currentPageNumber > 1) {
            $temp_paging_link = $pagePath . ( $currentPageNumber - 1 ) . "/";
            if (2 == $currentPageNumber and $pagePath == 'page/') {
                $temp_paging_link = '/';
            }
            $temp_paging[$i]['numbers'] = 'prev';
            $temp_paging[$i]['link'] = $temp_paging_link;
            $temp_paging[$i]['active'] = '';
            $temp_paging[$i]['type'] = 'prev';
            $i++;
        }
        for ($x = $p_start; $x <= $p_end; $x++) {
            $temp_paging_link = $pagePath . $x . "/";
            if (1 == $x and $pagePath == 'page/') {
                $temp_paging_link = '/';
            }
            $temp_paging[$i]['type'] = 'page';
            if ($currentPageNumber != $x) {
                $temp_paging[$i]['numbers'] = $x;
                $temp_paging[$i]['link'] = $temp_paging_link;
                $temp_paging[$i]['active'] = '';
            } else {
                $temp_paging[$i]['numbers'] = $x;
                $temp_paging[$i]['link'] = "";
                $temp_paging[$i]['active'] = 'active';
            }
            $i++;
        }
        if ($currentPageNumber < $p_end) {
            $temp_paging_link = $pagePath . ( $currentPageNumber + 1 ) . "/";
            $temp_paging[$i]['numbers'] = 'next';
            $temp_paging[$i]['link'] = $temp_paging_link;
            $temp_paging[$i]['active'] = '';
            $temp_paging[$i]['type'] = 'next';
        }
    }
    return $temp_paging;
}

function systemTimezoneDate($dateTime, $new_timezone, $default_timezone = '') {
    $default_timezone = $default_timezone != '' ? $default_timezone : date_default_timezone_get();
    if ($default_timezone == $new_timezone) {
        return $dateTime;
    }
    return getTimezoneDate($dateTime, $new_timezone, $default_timezone);
}

function c_x($param, $tag = false, $print = false) {
//    if ($print) {
    echo "\n";
    echo "<!-------";
    if ($tag) {

        echo $tag;
    }
    echo "\n";
    print_r($param);
    echo "---->";
//    }
}

function getTimezoneDate($dateTime, $oldTZ = false, $newTZ = false) {
    if ($oldTZ === false) {
        $oldTZ = date_default_timezone_get();
    }
    if ($newTZ === false) {
        return false;
    }
    if ($newTZ == $oldTZ) {
        return $dateTime;
    }
    $oldTZ = new DateTimeZone($oldTZ);
    $date = new DateTime($dateTime, $oldTZ);
    $newTZ = new DateTimeZone($newTZ);
    $date->setTimezone($newTZ);
    return $date->format(strlen($dateTime) != 10 ? 'Y-m-d H:i:s' : 'Y-m-d' );
}

function validateIntlPhone($str) {
// test if input is of the form +cc aa nnnn nnnn
    return preg_match("/^(\+|00)[1-9]{1,3}(\.|\s|-)?([0-9]{1,5}(\.|\s|-)?){1,3}$/", $str);
}

function validate_phone($str) {
    //return preg_match("/^[0-9]{10,12}$/", $str);
    return IdentifierNew_Utility::_noChar($str);
}

//  function validate_phone_number($mobile) {
//        if (preg_match(PHONE_PREG_MATCH, $mobile)) {
//            return TRUE;
//        } else {
//            return FALSE;
//        }
//    }

function _timers($name) {
    global $_timer;
    if (constant('BO_TIMER') && is_object($_timer))
        $_timer->setMarker($name);
}

function pretty($data, $message) {
    global $_debug;
    if ($_debug) {
        echo "<pre><---" . $message . "\n\r";
        print_r($data);
        echo "---></pre>";
    }
}

